;(function ($, window, document, undefined) {
  'use strict';

  window.main = window.main || {};

  main.sessionav = {
    settings: {
      autoinit: true,
      trigger: '.sessionav .trigger',
      menu: '.sessionav .menu',
      speed: 100,
      delay: 300
    },

    init: function () {
      this.handler();
    },

    handler: function () {
      var that = this;

      // Trigger
      $(document).on({
        click: function (e) {
          var $menu = $(that.settings.menu);

          if ($menu.is(':visible')) {
            that.heightAdjust();

            $menu
              .clearQueue()
              .fadeOut(that.settings.speed);
          } else {
            $menu.fadeIn(that.settings.speed);
          }

          e.preventDefault();
        },

        mouseenter: function () {
          $(that.settings.menu).clearQueue();
        },

        mouseleave: function () {
          var $menu = $(that.settings.menu);

          if ($menu.is(':visible')) {
            $menu
              .delay(that.settings.delay)
              .fadeOut(that.settings.speed);
          }
        }
      }, this.settings.trigger);

      // Menu
      $(document).on({
        mouseenter: function() {
          $(this).clearQueue();
        },

        mouseleave: function() {
          $(this)
            .delay(that.settings.delay)
            .fadeOut(that.settings.speed);
        }
      }, this.settings.menu);
    },

    heightAdjust: function () {
      $(this.settings.menu).css('max-height', $(window).height() - 100);
    }
  };
}(jQuery, this, this.document));
