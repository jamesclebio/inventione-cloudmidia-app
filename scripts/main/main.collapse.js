;(function ($, window, document, undefined) {
  'use strict';

  window.main = window.main || {};

  main.collapse = {
    settings: {
      autoinit: true,
      main: '.main-collapse',
      heading: '.main-collapse-heading',
      body: '.main-collapse-body',
      staticClass: 'static',
      onClass: 'on',
      speed: 100
    },

    init: function () {
      this.handler();
    },

    builder: function () {},

    handler: function () {
      var that = this;

      $(document).on({
        click: function (e) {
          that.toggle($(this));
          e.preventDefault();
        }
      }, this.settings.heading);
    },

    toggle: function ($this) {
      var $main = $this.closest(this.settings.main),
          $body = $this.next(this.settings.body);

      if (!$main.hasClass(this.settings.staticClass)) {
        if ($main.hasClass(this.settings.onClass)) {
          this.hide(this, $main, $body);
        } else {
          this.show(this, $main, $body);
        }
      }
    },

    show: function (that, $main, $body) {
      $main
        .siblings('.' + this.settings.onClass)
        .find(this.settings.body)
        .slideUp(this.settings.speed, function () {
          $(this)
            .closest(that.settings.main)
            .removeClass(that.settings.onClass);
        });

      $body.slideDown(this.settings.speed, function () {
        $main.addClass(that.settings.onClass);

        // inputRange plugin
        if ($body.find(main.inputRange.settings.main).length) {
          main.inputRange.update();
        }
      });
    },

    hide: function (that, $main, $body) {
      $body.slideUp(this.settings.speed, function () {
        $main.removeClass(that.settings.onClass);
      });
    }
  };
}(jQuery, this, this.document));
