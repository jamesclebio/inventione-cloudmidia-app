;(function ($, window, document, undefined) {
  'use strict';

  window.main = window.main || {};

  main.percentBar = {
    settings: {
      autoinit: false,
      main: '.percentbar',
      chart: '.percentbar-chart div',
      value: '.percentbar-value',
      animationDuration: 500
    },

    init: function () {},

    builder: function () {
      var $main = $(this.settings.main),
          $chart,
          $value;

      if ($main.length && arguments.length) {
        $chart = $main.find(this.settings.chart);
        $value = $main.find(this.settings.value);

        for (var item in arguments) {
          $($chart[item]).width(arguments[item] + '%');
          this.number($($value[item]), arguments[item]);
        }
      }
    },

    handler: function () {},

    number: function ($value, newValue) {
      $value
        .prop('number', parseInt($value.text()))
        .animateNumber({
          number: newValue
        }, this.settings.animationDuration);
    }
  };
}(jQuery, this, this.document));
