;(function ($, window, document, undefined) {
  'use strict';

  window.main = window.main || {};

  main.nav = {
    settings: {
      autoinit: true,
      navicon: '.default-header .navicon',
      nav: '.default-header .nav',
      links: '.default-header .links'
    },

    init: function () {
      this.handler();
    },

    handler: function () {
      var that = this;

      // Nav/Links
      $(document).on({
        click: function (e) {
          that.toggle();
          main.scrollTop.run();
          e.preventDefault();
        }
      }, this.settings.nav + ' a,' + this.settings.links + ' a');

      // Navicon
      $(document).on({
        click: function (e) {
          that.toggle();
          e.preventDefault();
        }
      }, this.settings.navicon);

      // Resize
      $(window).on({
        resize: function () {
          if ($(this).width() > 992) {
            $(that.settings.nav)
              .add(that.settings.links)
              .removeAttr('style');
          }
        }
      });
    },

    toggle: function () {
      if ($(this.settings.navicon).is(':visible')) {
        $(this.settings.nav)
          .add(this.settings.links)
          .slideToggle(100);
      }
    }
  };
} (jQuery, this, this.document));
