;(function ($, window, document, undefined) {
  'use strict';

  window.main = window.main || {};

  main.scrollTo = {
    settings: {
      autoinit: true,
      main: '[data-scroll-to]'
    },

    init: function () {
      this.handler();
    },

    handler: function () {
      var that = this;

      $(document).on({
        click: function (e) {
          that.run($($(this).data('scrollTo')));
          e.preventDefault();
        }
      }, this.settings.main);
    },

    run: function ($target) {
      if ($target.length) {
        $('html, body').animate({
          scrollTop: $target.offset().top
        }, 50);
      }
    }
  };
}(jQuery, this, this.document));
