;(function ($, window, document, undefined) {
  'use strict';

  window.main = window.main || {};

  main.duotab = {
    settings: {
      autoinit: false,
      rotate: '.rotate',
      onClass: 'on'
    },

    init: function (element) {
      var $main = $(element);

      this.builder($main);
      this.handler($main);
    },

    builder: function ($main) {
      $main
        .find(this.settings.rotate)
        .textrotator({
          animation: 'flipUp' // dissolve (default), fade, flip, flipUp, flipCube, flipCubeUp, spin
        });

      this.flowToggle($main.find('.duotab-item'));
    },

    handler: function ($main) {
      var that = this,
          $itemTrigger = $main.find('.duotab-item > .trigger');

      $itemTrigger.on({
        click: function(e) {
          var $item = $(this).closest('.duotab-item');

          that.itemToggle($item);
          that.flowToggle($item);
          e.preventDefault();
        }
      });
    },

    itemToggle: function ($item) {
      $item
        .addClass(this.settings.onClass)
        .siblings()
        .removeClass(this.settings.onClass);
    },

    flowToggle: function ($item) {
      var $flowPublisher = $('#flowPublisher'),
          $flowAdvertiser = $('#flowAdvertiser');

      if ($item.is(':first-child')) {
        $flowPublisher.show();
        $flowAdvertiser.hide();
      } else {
        $flowPublisher.hide();
        $flowAdvertiser.show();
      }
    }
  };
}(jQuery, this, this.document));
