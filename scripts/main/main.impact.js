;(function ($, window, document, undefined) {
  'use strict';

  window.main = window.main || {};

  main.impact = {
    settings: {
      autoinit: false,
      main: '.block-impact',
      chart: '.chart',
      value: '.value',
      views: '.views span',
      target: '.target span',
      animationDuration: 1200
    },

    init: function () {},

    builder: function (chartValue, viewsValue, targetValue) {
      var $main = $(this.settings.main),
          $chart,
          $views,
          $target;

      if ($main.length) {
        $chart = $main.find(this.settings.chart);
        $views = $main.find(this.settings.views);
        $target = $main.find(this.settings.target);

        this.chart($chart, chartValue);
        this.number($chart.next().find('span'), chartValue);
        this.number($views, viewsValue);
        this.number($target, targetValue);
      }
    },

    handler: function () {},

    chart: function ($chart, chartValue) {
      $chart.circleProgress({
        size: 140,
        thickness: 15,
        lineCap: 'round',
        fill: {
          gradient: [
            '#fdd250',
            '#10cfbd'
          ]
        },
        animation: {
          duration: this.settings.animationDuration
        },
        value: (chartValue * 0.01) || 0,
        animationStartValue: ($chart.data('circle-progress')) ? $chart.circleProgress('value') : 0
      });
    },

    number: function ($context, contextValue) {
      var currentValue = parseInt($context.text());

      contextValue = parseInt(contextValue) || 0;

      $context
        .text(contextValue)
        .prop('number', currentValue)
        .animateNumber({
          number: contextValue
        }, this.settings.animationDuration);
    }
  };
}(jQuery, this, this.document));
