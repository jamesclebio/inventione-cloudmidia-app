;(function ($, window, document, undefined) {
  'use strict';

  window.main = window.main || {};

  main.collapse = {
    settings: {
      autoinit: true,
      main: '.main-collapse',
      heading: '.main-collapse-heading',
      body: '.main-collapse-body',
      staticClass: 'static',
      onClass: 'on',
      speed: 100
    },

    init: function () {
      this.handler();
    },

    builder: function () {},

    handler: function () {
      var that = this;

      $(document).on({
        click: function (e) {
          that.toggle($(this));
          e.preventDefault();
        }
      }, this.settings.heading);
    },

    toggle: function ($this) {
      var $main = $this.closest(this.settings.main),
          $body = $this.next(this.settings.body);

      if (!$main.hasClass(this.settings.staticClass)) {
        if ($main.hasClass(this.settings.onClass)) {
          this.hide(this, $main, $body);
        } else {
          this.show(this, $main, $body);
        }
      }
    },

    show: function (that, $main, $body) {
      $main
        .siblings('.' + this.settings.onClass)
        .find(this.settings.body)
        .slideUp(this.settings.speed, function () {
          $(this)
            .closest(that.settings.main)
            .removeClass(that.settings.onClass);
        });

      $body.slideDown(this.settings.speed, function () {
        $main.addClass(that.settings.onClass);

        // inputRange plugin
        if ($body.find(main.inputRange.settings.main).length) {
          main.inputRange.update();
        }
      });
    },

    hide: function (that, $main, $body) {
      $body.slideUp(this.settings.speed, function () {
        $main.removeClass(that.settings.onClass);
      });
    }
  };
}(jQuery, this, this.document));

;(function ($, window, document, undefined) {
  'use strict';

  window.main = window.main || {};

  main.duotab = {
    settings: {
      autoinit: false,
      rotate: '.rotate',
      onClass: 'on'
    },

    init: function (element) {
      var $main = $(element);

      this.builder($main);
      this.handler($main);
    },

    builder: function ($main) {
      $main
        .find(this.settings.rotate)
        .textrotator({
          animation: 'flipUp' // dissolve (default), fade, flip, flipUp, flipCube, flipCubeUp, spin
        });

      this.flowToggle($main.find('.duotab-item'));
    },

    handler: function ($main) {
      var that = this,
          $itemTrigger = $main.find('.duotab-item > .trigger');

      $itemTrigger.on({
        click: function(e) {
          var $item = $(this).closest('.duotab-item');

          that.itemToggle($item);
          that.flowToggle($item);
          e.preventDefault();
        }
      });
    },

    itemToggle: function ($item) {
      $item
        .addClass(this.settings.onClass)
        .siblings()
        .removeClass(this.settings.onClass);
    },

    flowToggle: function ($item) {
      var $flowPublisher = $('#flowPublisher'),
          $flowAdvertiser = $('#flowAdvertiser');

      if ($item.is(':first-child')) {
        $flowPublisher.show();
        $flowAdvertiser.hide();
      } else {
        $flowPublisher.hide();
        $flowAdvertiser.show();
      }
    }
  };
}(jQuery, this, this.document));

;(function ($, window, document, undefined) {
  'use strict';

  window.main = window.main || {};

  main.impact = {
    settings: {
      autoinit: false,
      main: '.block-impact',
      chart: '.chart',
      value: '.value',
      views: '.views span',
      target: '.target span',
      animationDuration: 1200
    },

    init: function () {},

    builder: function (chartValue, viewsValue, targetValue) {
      var $main = $(this.settings.main),
          $chart,
          $views,
          $target;

      if ($main.length) {
        $chart = $main.find(this.settings.chart);
        $views = $main.find(this.settings.views);
        $target = $main.find(this.settings.target);

        this.chart($chart, chartValue);
        this.number($chart.next().find('span'), chartValue);
        this.number($views, viewsValue);
        this.number($target, targetValue);
      }
    },

    handler: function () {},

    chart: function ($chart, chartValue) {
      $chart.circleProgress({
        size: 140,
        thickness: 15,
        lineCap: 'round',
        fill: {
          gradient: [
            '#fdd250',
            '#10cfbd'
          ]
        },
        animation: {
          duration: this.settings.animationDuration
        },
        value: (chartValue * 0.01) || 0,
        animationStartValue: ($chart.data('circle-progress')) ? $chart.circleProgress('value') : 0
      });
    },

    number: function ($context, contextValue) {
      var currentValue = parseInt($context.text());

      contextValue = parseInt(contextValue) || 0;

      $context
        .text(contextValue)
        .prop('number', currentValue)
        .animateNumber({
          number: contextValue
        }, this.settings.animationDuration);
    }
  };
}(jQuery, this, this.document));

;(function ($, window, document, undefined) {
  'use strict';

  window.main = window.main || {};

  main.inputRange = {
    settings: {
      autoinit: false,
      main: '[inv-input-range]'
    },

    init: function () {},

    builder: function (element) {
      var $main = $(element),
          $input = $main.find('[type="range"]'),
          $output = $main.find('output');

      $input
        .rangeslider('destroy')
        .rangeslider({
          polyfill: false
        });

      this.output($input, $output);
      this.handler($input, $output);
    },

    handler: function ($input, $output) {
      var that = this;

      $input.on({
        input: function () {
          that.output($input, $output);
        },

        change: function () {
          that.output($input, $output);
        }
      });
    },

    output: function ($input, $output) {
      $output.val($input.val());
    },

    update: function () {
      $(this.settings.main + ' input[type="range"]').rangeslider('update', true).change();
    }
  };
}(jQuery, this, this.document));

;(function ($, window, document, undefined) {
  'use strict';

  window.main = window.main || {};

  main.loaderPage = {
    settings: {
      autoinit: false,
      main: '.loader-page',
      template: '<div class="loader-page"></div>',
      speed: 100
    },

    init: function () {},

    builder: function () {
      var $main = $(this.settings.main);

      return ($main.length) ? $main : $(this.settings.template).appendTo($('.default-content, .lite-content'));
    },

    handler: function () {},

    on: function () {
      this.builder().fadeIn(this.settings.speed);
    },

    off: function () {
      this.builder().fadeOut(this.settings.speed);
    }
  };
}(jQuery, this, this.document));

;(function ($, window, document, undefined) {
  'use strict';

  window.main = window.main || {};

  main.nav = {
    settings: {
      autoinit: true,
      navicon: '.default-header .navicon',
      nav: '.default-header .nav',
      links: '.default-header .links'
    },

    init: function () {
      this.handler();
    },

    handler: function () {
      var that = this;

      // Nav/Links
      $(document).on({
        click: function (e) {
          that.toggle();
          main.scrollTop.run();
          e.preventDefault();
        }
      }, this.settings.nav + ' a,' + this.settings.links + ' a');

      // Navicon
      $(document).on({
        click: function (e) {
          that.toggle();
          e.preventDefault();
        }
      }, this.settings.navicon);

      // Resize
      $(window).on({
        resize: function () {
          if ($(this).width() > 992) {
            $(that.settings.nav)
              .add(that.settings.links)
              .removeAttr('style');
          }
        }
      });
    },

    toggle: function () {
      if ($(this.settings.navicon).is(':visible')) {
        $(this.settings.nav)
          .add(this.settings.links)
          .slideToggle(100);
      }
    }
  };
} (jQuery, this, this.document));

;(function ($, window, document, undefined) {
  'use strict';

  window.main = window.main || {};

  main.percentBar = {
    settings: {
      autoinit: false,
      main: '.percentbar',
      chart: '.percentbar-chart div',
      value: '.percentbar-value',
      animationDuration: 500
    },

    init: function () {},

    builder: function () {
      var $main = $(this.settings.main),
          $chart,
          $value;

      if ($main.length && arguments.length) {
        $chart = $main.find(this.settings.chart);
        $value = $main.find(this.settings.value);

        for (var item in arguments) {
          $($chart[item]).width(arguments[item] + '%');
          this.number($($value[item]), arguments[item]);
        }
      }
    },

    handler: function () {},

    number: function ($value, newValue) {
      $value
        .prop('number', parseInt($value.text()))
        .animateNumber({
          number: newValue
        }, this.settings.animationDuration);
    }
  };
}(jQuery, this, this.document));

;(function ($, window, document, undefined) {
  'use strict';

  window.main = window.main || {};

  main.scrollTo = {
    settings: {
      autoinit: true,
      main: '[data-scroll-to]'
    },

    init: function () {
      this.handler();
    },

    handler: function () {
      var that = this;

      $(document).on({
        click: function (e) {
          that.run($($(this).data('scrollTo')));
          e.preventDefault();
        }
      }, this.settings.main);
    },

    run: function ($target) {
      if ($target.length) {
        $('html, body').animate({
          scrollTop: $target.offset().top
        }, 50);
      }
    }
  };
}(jQuery, this, this.document));

;(function ($, window, document, undefined) {
  'use strict';

  window.main = window.main || {};

  main.scrollTop = {
    settings: {
      autoinit: true,
      main: '[data-scroll-top]'
    },

    init: function () {
      this.handler();
    },

    handler: function () {
      var that = this;

      $(document).on({
        click: function (e) {
          that.run();
          e.preventDefault();
        }
      }, this.settings.main);
    },

    run: function () {
      $('html, body').animate({
        scrollTop: 0
      }, 0);
    }
  };
}(jQuery, this, this.document));

;(function ($, window, document, undefined) {
  'use strict';

  window.main = window.main || {};

  main.sessionav = {
    settings: {
      autoinit: true,
      trigger: '.sessionav .trigger',
      menu: '.sessionav .menu',
      speed: 100,
      delay: 300
    },

    init: function () {
      this.handler();
    },

    handler: function () {
      var that = this;

      // Trigger
      $(document).on({
        click: function (e) {
          var $menu = $(that.settings.menu);

          if ($menu.is(':visible')) {
            that.heightAdjust();

            $menu
              .clearQueue()
              .fadeOut(that.settings.speed);
          } else {
            $menu.fadeIn(that.settings.speed);
          }

          e.preventDefault();
        },

        mouseenter: function () {
          $(that.settings.menu).clearQueue();
        },

        mouseleave: function () {
          var $menu = $(that.settings.menu);

          if ($menu.is(':visible')) {
            $menu
              .delay(that.settings.delay)
              .fadeOut(that.settings.speed);
          }
        }
      }, this.settings.trigger);

      // Menu
      $(document).on({
        mouseenter: function() {
          $(this).clearQueue();
        },

        mouseleave: function() {
          $(this)
            .delay(that.settings.delay)
            .fadeOut(that.settings.speed);
        }
      }, this.settings.menu);
    },

    heightAdjust: function () {
      $(this.settings.menu).css('max-height', $(window).height() - 100);
    }
  };
}(jQuery, this, this.document));

;(function($, window, document, undefined) {
  'use strict';

  window.main = window.main || {};

  main.init = function () {
    var resources = [];

    if (arguments.length) {
      resources = arguments;
    } else {
      for (var i in this) {
        resources.push(i);
      }
    }

    this.initTrigger(this, resources);
  };

  main.initTrigger = function (object, resources) {
    if (!resources) {
      resources = [];

      for (var i in object) {
        resources.push(i);
      }
    }

    for (var j in resources) {
      if (object[resources[j]].hasOwnProperty('settings') && object[resources[j]].settings.autoinit) {
        object[resources[j]].init();
      }
    }
  };

  main.init();
}(jQuery, this, this.document));
