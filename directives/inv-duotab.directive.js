;(function () {
  'use strict';

  angular
    .module('cloudmidiaApp')
    .directive('invDuotab', invDuotab);

  function invDuotab() {
    return {
      restrict: 'A',
      link: function (scope, element, attrs) {
        main.duotab.init(element);
      }
    }
  }
}());
