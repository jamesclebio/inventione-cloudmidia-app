/**
 * Created by Admin on 10/19/2016.
 */
;(function () {
  'use strict';

  angular
    .module('cloudmidiaApp')
    .directive('invLocalInputRange', invLocalInputRange);

  function invLocalInputRange() {
    return {
      restrict: 'A',
      link: function (scope, element, attrs) {
        scope.$watch(attrs.rangeValue,function(newvalue, oldvalue){
          main.inputRange.builder(element);
        });
      }
    }
  }
}());
