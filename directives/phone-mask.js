angular.module('ng').filter('phone', function () {
  return function (tel) {
    if (!tel) { return ''; }

    var value = tel.toString().trim().replace(/^\+/, '');

    if (value.match(/[^0-9]/)) {
      return tel;
    }

    var country, city, number;

    switch (value.length) {
      case 9: // ######### -> #####-####
        country = 1;
        city = value.slice(0, 4);
        number = value.slice(4);
        break;

      case 10: // PP####### -> (PP) ####-####
        country = 1;
        city = value.slice(0, 2);
        number = value.slice(2);
        break;

      case 11: // PP######### -> (PP) #####-####
        country = 1;
        city = value.slice(0, 2);
        number = value.slice(2);
        break;

      default:
        return tel;
    }

    if (country === 1) {
      country = '';
    }

    number = number.slice(0, 4) + '-' + number.slice(4);
    if(value.length > 9)
      return (country + '(' + city + ')' + number).trim();
    else
      return number.trim();
  };
});
