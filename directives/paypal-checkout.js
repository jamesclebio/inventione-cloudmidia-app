/**
 * Created by Admin on 10/19/2016.
 */
'use strict'
var module = angular.module('cloudmidiaApp')

module.directive("paypalCheckout", function($http, $q, $timeout) {
  return {
    restrict: 'A',
    link: function(scope, ele, attrs) {
      var environment = 'live';       // CHANGE AS NEEDED
      var merchantId  = 'VSMMZSK6YJP7Q'; // YOUR MERCHANT ID HERE (or import with scope)
      var req = {
        method: 'POST',
        url: attrs.url          // YOUR SERVER HERE (or import with scope)
      }
      scope.showButton = false;

      function loadPaypalButton() {
        paypal.checkout.setup(merchantId, {
          environment: environment,
          buttons: 't1'
        });
      }

      scope.initLoad = function(){
        main.loaderPage.on();
      }

      if (window.paypalCheckoutReady != null) {
        scope.showButton = true
      } else {
        var s = document.createElement('script');
        s.src = '//www.paypalobjects.com/api/checkout.js';
        document.body.appendChild(s);
        window.paypalCheckoutReady = function() {
          return loadPaypalButton();
        }
      }

    }
  }
})

