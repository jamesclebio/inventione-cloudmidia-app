;(function () {
  'use strict';

  angular
    .module('cloudmidiaApp')
    .directive('invInputRange', invInputRange);

  function invInputRange() {
    return {
      restrict: 'A',
      scope:{
        isDisabled: '='
      },
      link: function (scope, element, attrs) {
        scope.$watch(attrs.rangeValue,function(newvalue, oldvalue){
          main.inputRange.builder(element);
        });
        scope.$watch('isDisabled', function(newvalue, oldvalue){
          main.inputRange.builder(element);
        });
      }
    }
  }
}());
