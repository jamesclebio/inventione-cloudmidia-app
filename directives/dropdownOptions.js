/**
 * Created by Admin on 5/10/2016.
 */
angular.module('cloudmidiaApp')
  .directive('dropdownOptions', function(){
    return {
      restrict: 'A',
      transclude: true,
      link: function(scope, element, attrs) {
        scope.selectColumn = function(column) {
          scope.selectedColumn = column;
          scope.showList = false;
          scope.overlay = false;
        }
      }
    }
  });
