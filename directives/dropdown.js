/**
 * Created by Admin on 5/10/2016.
 */
angular.module('cloudmidiaApp')
  .directive("dropdown", function() {
    return {
      restrict: 'A',
      link: function(scope) {
        scope.showDropdown = function() {
          if (scope.showList) {
            scope.showList = false;
            scope.overlay = false;
          }
          else {
            scope.showList = true;
            scope.overlay = true;
          }
        }
      }
    }
  });
