//'use strict';
//
//describe('UploadControllerTest', function () {
//  var $controllerConstructor, scope, q, modalInstance, cloudMidiaApi;
//  var accountId, mediaFilter, Upload, appSettings, ctrl, calcThumbSize;
//
//  beforeEach(module('cloudmidiaApp'));
//
//  beforeEach(module(function ($provide) {
//    calcThumbSize = {
//      calc: function (videoWidth, videoHeight) {
//        var maxWidth = 140;
//        var maxHeight = 105;
//        var ratio = 0;
//        var width = videoWidth;
//        var height = videoHeight;
//        if(videoWidth <= maxWidth && videoHeight <= maxHeight){
//          return {'width': width, 'height': height};
//        }else{
//          if (width > height && width>maxWidth) {
//            ratio = maxWidth / width;
//            height = height * ratio;
//            width = width * ratio;
//          } else {
//            ratio = maxHeight / height;
//            width = width * ratio;
//            height = height * ratio;
//          }
//          return {'width': width, 'height': height};
//        }
//      }
//    };
//    $provide.value('CalcThumbSize', calcThumbSize);
//  }));
//
//  beforeEach(inject(function ($controller, $q, $rootScope) {
//    scope = $rootScope.$new();
//    $controllerConstructor = $controller;
//    q = $q;
//    ctrl = $controllerConstructor('ModalFileUploadController', {
//      AppSettings: appSettings,
//      $q: q, $scope: scope, $modalInstance: modalInstance, accountId: accountId, mediaFilter: mediaFilter,
//      Upload: Upload
//    });
//  }));
//
//  it('testing calcThumbSize function with values 1020 X 800', function () {
//    var width = 1020;
//    var height = 800;
//    var result = calcThumbSize.calc(width, height);
//    expect(result.width).toBe(140);
//  });
//
//  it('testing calcThumbSize function with values 800 X 1020', function () {
//    var width = 800 ;
//    var height = 1020 ;
//    var result = calcThumbSize.calc(width, height);
//    expect(result.height).toBe(105);
//  });
//
//  it('testing calcThumbSize function with height values less than 105', function () {
//    var width = 150 ;
//    var height = 70 ;
//    var result = calcThumbSize.calc(width, height);
//    expect(result.width).toBe(140);
//  });
//
//  it('testing calcThumbSize function with width values less than 140', function () {
//    var width = 139 ;
//    var height = 110 ;
//    var result = calcThumbSize.calc(width, height);
//    expect(result.height).toBe(105);
//  });
//
//  it('testing calcThumbSize function with width values less than 140 and 105', function () {
//    var width = 138 ;
//    var height = 104 ;
//    var result = calcThumbSize.calc(width, height);
//    expect(result.height).toBe(104);
//  });
//
//});
