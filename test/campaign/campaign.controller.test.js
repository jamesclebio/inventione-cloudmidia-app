describe('Campaign Core',function(){
  var $scope;
  var $location;
  var authService;
  var $log;
  var $modal;
  var CampaignService;
  var ProviderService;
  var $rootScope;
  var $q;
  var $controller;
  var enterpriser={id:1};
  var listOfCampaings = {
    items:[{
      'advertiser':{},
      'advertiserId':1023,
      'advertiserName':"RERE",
      'boost':2,
      'devices':[{
        "id": 1,
        "name": "Device1",
        "locals":1,
        "status":true,
        "displays":10,
        "date":'1/1/1900 12:00:00 AM'
      },
        {
          "id": 2,
          "name": "Device1",
          "locals":1,
          "status":true,
          "displays":10,
          "date":'1/1/1900 12:00:00 AM'
        }],
      'duration':30,
      'endDate':"2016-06-12T14:16:21.083Z",
      'expired':false,
      'id':4285,
      'interrupted':true,
      'lastUpdated':"2016-05-24T15:11:46.97Z",
      'leftDays':1638274113,
      'media':Object,
      'name':"carcará",
      'quantDevices':1,
      'quantityOfViewers':0,
      'reachOfCampaign':0,
      'registerDate':"2016-05-13T14:16:23.157Z",
      'startDate':"2016-05-13T14:15:51.973Z",
      'status':false,
      'type':0
    }],
    totalItems:12,
    page:1,
    pageSize:10
  };

  beforeEach(module('cloudmidiaApp'));
  beforeEach(module(function($provide){
    $provide.value('AppSettings',{urlApi:'http://localhost:61110/api/'});

  }));

  beforeEach(inject(function(_$controller_,_$location_,_authService_,_$rootScope_,_CampaignService_,_ProviderService_,_$q_,_$modal_,_$log_){
    $scope = _$rootScope_.$new();
    $log = _$log_;
    $q = _$q_;
    $modal = _$modal_;
    CampaignService = _CampaignService_;
    ProviderService = _ProviderService_;
    $rootScope = _$rootScope_;
    $location = _$location_;
    authService = _authService_;
    $controller = _$controller_;

  }));

  it('should calc impact', function(){
    $controller('CampaignController',{$scope:$scope});
    expect($scope.impact(10,22)).toBe(45);
    expect($scope.impact(10,0)).toBe(0);
    expect($scope.impact(0,10)).toBe(0);
  });

  it('should search something',function(){
    $controller('CampaignController',{$scope:$scope});
    $scope.enterpriser = enterpriser;
    spyOn(CampaignService,'getCampaigns').and.callFake(function(){
      var deferred = $q.defer();
      deferred.resolve(listOfCampaings);
      return deferred.promise;
    });
    $scope.search(13,'kdkd');
    $rootScope.$apply();
    expect(CampaignService.getCampaigns).toHaveBeenCalled();
    expect($scope.filterName).toBe('kdkd');
  });
  it('Page should be changed', function(){
    $controller('CampaignController', {$scope:$scope});
    $scope.enterpriser = enterpriser;
    spyOn(CampaignService,'getCampaigns').and.callFake(function(){
      var deferred = $q.defer();
      deferred.resolve(listOfCampaings);
      return deferred.promise;
    });
    $scope.pageChanged(2);
    expect($scope.currentPage).toBe(2);
    $rootScope.$apply();
    expect(CampaignService.getCampaigns).toHaveBeenCalled();
  })
});

