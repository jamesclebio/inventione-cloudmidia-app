// describe('Device service core',function(){
//   var mocDevicesService;
//   var mocHttpBackend;
//   var mocGoogle;
//   var device = {data:{
//     "id": 1,
//     "name": "Device1",
//     "locals":1,
//     "status":true,
//     "displays":10,
//     "date":'1/1/1900 12:00:00 AM'
//   }};
//   var listOfDevices = {
//     item:[{
//       "id": 1,
//       "name": "Device1",
//       "locals":1,
//       "status":true,
//       "displays":10,
//       "date":'1/1/1900 12:00:00 AM'
//     },
//       {
//         "id": 2,
//         "name": "Device1",
//         "locals":1,
//         "status":true,
//         "displays":10,
//         "date":'1/1/1900 12:00:00 AM'
//       }]
//   };

//   beforeEach(module('ngMockE2E'));

//   beforeEach(module('cloudmidiaApp'));

//   beforeEach(module(function($provide){
//     $provide.value('AppSettings',{urlApi:'http://localhost:61110/api/'});
//     $provide.value('google',{
//       maps:function(){
//         return 'dsdsd';
//       }
//     });
//   }));

//   beforeEach(inject(function($injector){
//     mocHttpBackend = $injector.get('$httpBackend');
//     mocGoogle = $injector.get('google');
//     mocDevicesService = $injector.get('DevicesService');
//   }));

//   it('should get a device', function(){
//     var result;

//     mocHttpBackend.expectGET(/api\/accounts\/\d+\/devices\/\d+$/)
//       .respond(200,device);

//     mocDevicesService.getDevice(1,1).then(
//       function success(data){
//         result = data;
//         dump(result);
//       }
//     );
//     mocHttpBackend.flush();
//     expect(result.data).toEqual(device.data);
//   });

//   it('should get a list of devices', function(){
//     var result;
//     mocHttpBackend.expectGET(/api\/accounts\/\d+\/devices/).respond(200,listOfDevices);

//     mocDevicesService.getDevices(1,null,null,null,null).then(
//       function success(data){
//         result = data;
//       }
//     );
//     mocHttpBackend.flush();
//     expect(result.item).toEqual(listOfDevices.item);
//   })
// });
