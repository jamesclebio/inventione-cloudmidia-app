angular.module('cloudmidiaAppMock', ['cloudmidiaApp', 'ngMockE2E'])
  .run(function ($httpBackend) {

    var contents = [
      {
        "title": "Mídias",
        "subTitle": "Vinheta Cloudmidia",
        "image": "assets/images/grade_programacao/img1.jpg"
      },
      {
        "title": "Texto e foto",
        "subTitle": "Seja bem-vindo!",
        "image": "assets/images/grade_programacao/img2.jpg"
      },
      {
        "title": "Previsão do Tempo",
        "subTitle": "Nossa Senhora das Dores",
        "image": "assets/images/grade_programacao/img3.jpg"
      },
      {
        "title": "Trânsito",
        "subTitle": "Trânsito em Aracaju",
        "image": "assets/images/grade_programacao/img4.jpg"
      },
      {
        "title": "Mídias",
        "subTitle": "Vinheta Cloudmidia",
        "image": "assets/images/grade_programacao/img1.jpg"
      },
      {
        "title": "Texto e foto",
        "subTitle": "Seja bem-vindo!",
        "image": "assets/images/grade_programacao/img2.jpg"
      },
      {
        "title": "Previsão do Tempo",
        "subTitle": "Nossa Senhora das Dores",
        "image": "assets/images/grade_programacao/img3.jpg"
      },
      {
        "title": "Trânsito",
        "subTitle": "Trânsito em Aracaju",
        "image": "assets/images/grade_programacao/img4.jpg"
      },
      {
        "title": "Mídias",
        "subTitle": "Vinheta Cloudmidia",
        "image": "assets/images/grade_programacao/img1.jpg"
      },
      {
        "title": "Texto e foto",
        "subTitle": "Seja bem-vindo!",
        "image": "assets/images/grade_programacao/img2.jpg"
      },
      {
        "title": "Previsão do Tempo",
        "subTitle": "Nossa Senhora das Dores",
        "image": "assets/images/grade_programacao/img3.jpg"
      },
      {
        "title": "Trânsito",
        "subTitle": "Trânsito em Aracaju",
        "image": "assets/images/grade_programacao/img4.jpg"
      },
      {
        "title": "Mídias",
        "subTitle": "Vinheta Cloudmidia",
        "image": "assets/images/grade_programacao/img1.jpg"
      },
      {
        "title": "Texto e foto",
        "subTitle": "Seja bem-vindo!",
        "image": "assets/images/grade_programacao/img2.jpg"
      },
      {
        "title": "Previsão do Tempo",
        "subTitle": "Nossa Senhora das Dores",
        "image": "assets/images/grade_programacao/img3.jpg"
      },
      {
        "title": "Trânsito",
        "subTitle": "Trânsito em Aracaju",
        "image": "assets/images/grade_programacao/img4.jpg"
      }
    ];
    var devices = [
      {
        "id": 1,
        "version": "1.1",
        "name": "TV do pátio",
        "exhibitionGroup": "Grupo 1",
        "lastConnection": "2015-09-02T13:07:49",
        "lastUpdate": "2015-09-02T13:07:49"
      },
      {
        "id": 2,
        "version": "1.2",
        "name": "TV do corredor",
        "exhibitionGroup": "Grupo 2",
        "lastConnection": "2015-09-02T13:07:49",
        "lastUpdate": "2015-09-02T13:07:49"
      },
      {
        "id": 3,
        "version": "1.1",
        "name": "TV do quarto",
        "exhibitionGroup": "Grupo 3",
        "lastConnection": "2015-09-02T13:07:49",
        "lastUpdate": "2015-09-02T13:07:49"
      }
    ];
    var devicesFirstPage = [
      {
        "id": 1,
        "version": "1.1",
        "name": "TV do pátio",
        "exhibitionGroup": "Grupo 1",
        "lastConnection": "2015-09-02T13:07:49",
        "lastUpdate": "2015-09-02T13:07:49"
      },
      {
        "id": 2,
        "version": "1.2",
        "name": "TV do corredor",
        "exhibitionGroup": "Grupo 2",
        "lastConnection": "2015-09-02T13:07:49",
        "lastUpdate": "2015-09-02T13:07:49"
      },
      {
        "id": 3,
        "version": "1.1",
        "name": "TV do quarto",
        "exhibitionGroup": "Grupo 3",
        "lastConnection": "2015-09-02T13:07:49",
        "lastUpdate": "2015-09-02T13:07:49"
      }
    ];
    var devicesSecondPage = [
      {
        "id": 1,
        "version": "1.1",
        "name": "Sony",
        "exhibitionGroup": "Grupo 1",
        "lastConnection": "2015-09-02T13:07:49",
        "lastUpdate": "2015-09-02T13:07:49"
      },
      {
        "id": 2,
        "version": "1.2",
        "name": "Shopping Jardins",
        "exhibitionGroup": "Grupo 2",
        "lastConnection": "2015-09-02T13:07:49",
        "lastUpdate": "2015-09-02T13:07:49"
      }
    ];
    var deviceGroups = [
      {
        "id": 1,
        "name": "Device1"
      },
      {
        "id": 2,
        "name": "Device2"
      },
      {
        "id": 3,
        "name": "Device3"
      }
    ];
    var updateDevice = {
      "id": 1,
      "name": "Device1",
      "groups": [1, 2]
    };
    var campaigns = [{
      "id": 1,
      "order": 1,
      "name": "Norcon",
      "period": "22/01/04 - 23/01/04",
      "exhibition": 10,
      "lastUpdate": "22/01/04 10:20:30"
    },
      {
        "id": 2,
        "order": 2,
        "name": "Celi",
        "period": "22/01/04 - 23/01/04",
        "exhibition": 10,
        "lastUpdate": "22/01/04 10:20:30"
      }
    ];
    var devicesByGroups = [
      {
        "id": 1,
        "name": "Shopping Jardins",
        "devices": [1, 2, 3, 4]
      },
      {
        "id": 2,
        "name": "Shopping Prêmio",
        "devices": [1, 3, 4]
      }
    ];
    var devicesByGroup = [
      {
        "id":1,
        "name":"Shopping Jardins",
        "devices":[
          {
            "id":1,
            "name":"BABABA",
            "coords": {
              "latitude": -10.9426348,
              "longitude": -37.061769
            }
          },
          {
            "id":2,
            "name":"BABABA",
            "coords": {
              "latitude": -10.9526348,
              "longitude": -37.062769
            }
          },
          {
            "id":3,
            "name":"BEBEBE",
            "coords": {
              "latitude": -10.9458476,
              "longitude": -37.0504903
            }
          }]
      }
    ]
    var campaign = {
      "type": 1,
      "name": "Norcon",
      "startDate": "2015/04/22",
      "endDate": "2015/04/22",
      "duration": 11,
      "media": {
        "title": "Motos",
        "subtitle": "khsjdsds",
        "text": "lksjdljlksds",
        "source": "lskjdjsds",
        "url": "slkjdslkjd.jpg"
      },
      "groupedDevices": [{"idGroup": 1, "devices": [1]}, {"idGroup": 2, "devices": [1]}],
      "lastUpdated": "2015-09-24T15:29:31.433"
    };
    var messageSuccess = {
      "status": 200,
      "messages": [
        {
          "type": "SUCCESS",
          "code": "2003",
          "description": "Cadastro feito com sucesso",
          "parameters": {}
        }
      ]
    };
    var messageSuccessInvitation = {
      "status": 200,
      "messages": [
        {
          "type": "SUCCESS",
          "code": "2003",
          "description": "Convite aceito com sucesso",
          "parameters": {}
        }
      ]
    }
    var messageSuccessDelete = {
      "status": 200,
      "messages": [
        {
          "type": "SUCCESS",
          "code": "2003",
          "description": "Remoção feita com sucesso",
          "parameters": {}
        }
      ]
    }
    var messageSuccessUpload = {
      "status": 200,
      "messages": [
        {
          "type": "SUCCESS",
          "code": "2003",
          "description": "Permissão upload feito com sucesso.",
          "parameters": {}
        }
      ]
    };

    var messageSuccessUpdate = {
      "status": 200,
      "messages": [
        {
          "type": "SUCCESS",
          "code": "2003",
          "description": "Permissão adicionada com sucesso.",
          "parameters": {}
        }
      ]
    };
    var userRoles = [
      {"id": 1, "name": "Manager"},
      {"id": 2, "name": "Editor Notícias"},
      {"id": 3, "name": "Editor Comerciais"}
    ];

    var typeOfBusiness = [{"id":1,"name":"Restaurante"},
      {"id":1, "name":"Entretenimento"},
      {"id":3,"name":"Esportes"},
      {"id":4,"name":"Beleza"}];

    var users = [
      {"id":1, "name":"Michael",email:"michaeloc@gmail.com",roles:[{"id":1,"name":"Manager"},{"id":3,"name":"Editor Comerciais"}]},
      {"id":2, "name":"Fulano",email:"michaeloc@gmail.com",roles:[{"id":3,"name":"Editor Comerciais"}]},
      {"id":3, "name":"Ozzy",email:"michaeloc@gmail.com",roles:[{"id":2,"name":"Editor Notícia"}]},
    ];
    var invitations = [{"id":1,"email":"fulano@yahoo.com","token":"jslkjdlsjdsiouew99slkdj"},
      {"id":1,"email":"fulano@yahoo.com","token":"jslkjdlsjdsiouew99slkdj"},
      {"id":1,"email":"fulano@yahoo.com","token":"jslkjdlsjdsiouew99slkdj"},
      {"id":1,"email":"fulano@yahoo.com","token":"jslkjdlsjdsiouew99slkdj"},
      {"id":1,"email":"fulano@yahoo.com","token":"jslkjdlsjdsiouew99slkdj"}];
    var login = {"access_token":"ljlkjsdjlsjdlsjds","refresh_token":"ashuashuashuashua"};
    var profileUser = {"accountName":"ViaMidia","firstName":"Maria","lastName":"Antonia"};
    var invitationInformation = {"accountName":"Sony","invitationEmail":"saulo@blabla.com","isUser":false,"roles":[{"id": 1, "name": "Manager"}]}
    var locals = [
      {'id':0,
        'name':'Praça de Alimentação',
        'location':'jlskjdlskjds',
        'quantityOfViewers':100,
        'typeOfBusiness':'Alimentação',
        'type':'Fixo'},
      {
        'id':1,
        'name':'Empreendimentos',
        'location':'jlskjdlskjds',
        'quantityOfViewers':50,
        'typeOfBusiness':'Entretenimento',
        'type':'Fixo'
      }
    ]

    $httpBackend.whenPOST(/api\/accounts\/\d+\/groups\/\d+$/).respond(404);
    $httpBackend.whenGET(/api\/accounts\/\d+\/locals$/).respond(locals);
    $httpBackend.whenGET(/api\/accounts\/\d+\/typesOfBusiness$/).respond(typeOfBusiness);
    $httpBackend.whenGET(/api\/invitation\/.*/).respond(invitationInformation);
    $httpBackend.whenGET(/api\/accounts\/\d+\/invitations\/\d+$/).respond(invitations);
    $httpBackend.whenPOST(/api\/invitation\/.*\/accept$/).respond(messageSuccessInvitation);
    $httpBackend.whenGET(/api\/me\/profile$/).respond(profileUser);
    $httpBackend.whenPOST(/api\/accounts\/\d+\/thumbnail.*/).respond(messageSuccessUpload);
    $httpBackend.whenPOST(/api\/token$/).respond(login);
    $httpBackend.whenGET(/api\/accounts\/\d+\/users$/).respond(users);
    $httpBackend.whenDELETE(/api\/accounts\/\d+\/users\/\d+$/).respond(messageSuccessDelete);
    $httpBackend.whenGET(/api\/accounts\/\d+\/roles$/).respond(userRoles);
    $httpBackend.whenPOST(/api\/accounts\/\d+\/users$/).respond(messageSuccess);
    $httpBackend.whenPOST(/api\/accounts\/\d+\/users\/\d+\/roles\/\d+$/).respond(messageSuccessUpdate);
    $httpBackend.whenDELETE(/api\/accounts\/\d+\/users\/\d+\/roles\/\d+$/).respond(messageSuccessDelete);

    $httpBackend.whenPOST(/api\/register$/).respond(messageSuccess);
    $httpBackend.whenGET(/api\/accounts\/\d+\/campaigns$/).respond(campaigns);
    $httpBackend.whenGET(/api\/accounts\/\d+\/campaigns\/\d+$/).respond(campaign);
    $httpBackend.whenPOST(/api\/accounts\/\d+\/campaigns$/).respond(200);
    $httpBackend.whenGET(/api\/accounts\/\d+\/devices\?currentPage=1&totalItems=2$/).respond(devicesFirstPage);
    $httpBackend.whenGET(/api\/accounts\/\d+\/devices\?currentPage=2&totalItems=2$/).respond(devicesSecondPage);
    $httpBackend.whenGET(/api\/groups\/\d+$/).respond(deviceGroups);
    $httpBackend.whenGET(/api\/accounts\/\d+\/devices\/devicesByGroup$/).respond(devicesByGroups);
    $httpBackend.whenGET(/api\/accounts\/\d+\/devices\/devicesWithGroup$/).respond(devicesByGroup);
    $httpBackend.whenPOST('http://cmapi.azurewebsites.net/api/accounts/3/midias').respond(200);
    $httpBackend.whenPOST('http://fzback.azurewebsites.net/api/devicegroups').respond(200);
    $httpBackend.whenDELETE(/api\/accounts\/\d+\/devices\/\d+$/).respond(200);
    $httpBackend.whenDELETE(/api\/accounts\/\d+\/campaign/).respond(200);
    $httpBackend.whenGET(/api\/accounts\/\d+\/devices\/\d+$/).respond(updateDevice);
    $httpBackend.whenPUT(/api\/accounts\/\d+\/devices\/\d+$/).respond(200);

    /** Everything else **/
    $httpBackend.whenGET(/.*\.html/).passThrough();
    $httpBackend.whenGET(/.*\.json/).passThrough();
    $httpBackend.whenGET(/.*\.jpg/).passThrough();
  });

