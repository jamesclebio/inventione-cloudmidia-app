//'use strict';
//
//describe('ModifyControllerTest',function() {
//  var $controllerConstructor, scope, cloudMidiaApi,modalInstance,ctrl;
//
//  var accountId,deviceGroups,device,$httpBackend;
//
//  var inputData = {};
//
//  //load cloudmidiaApp
//  beforeEach(module('cloudmidiaApp'));
//
//  beforeEach(module(function($provide){
//   $provide.factory('DevicesService',['$q','$resource',function($q,$resource){
//     var updateDevice = function(accountId,deviceId,deviceName,groups){
//       var deferred = $q.defer();
//       var messages;
//       if(accountId !== undefined && deviceId!==undefined && deviceName!==undefined && groups.length>0){
//         messages = {'messages':[{'type':'SUCCESS'}]};
//         deferred.resolve(messages);
//       }
//       else{
//         messages = {'messages':[{'type':'ERROR'}]};
//         deferred.resolve(messages);
//       }
//       return deferred.promise;
//     };
//     return {
//       updateDevice:updateDevice
//     };
//    }]);
//  }));
//
//  beforeEach(inject(function ($controller, $rootScope,DevicesService) {
//    scope = $rootScope.$new();
//    modalInstance = {
//      close:function(){},
//      dismiss:function(){},
//      result:{
//        then:jasmine.createSpy('modalInstance.result.then')
//      }
//    };
//    spyOn(modalInstance,'close');
//    $controllerConstructor = $controller;
//    accountId = 17;
//    device = [{'id':1,'name':'Teste','groups':[{'id':1,'name':'Empresarial'}]}];
//    deviceGroups = [{'id':1,'name':'Empresarial'},{'id':2,'name':'Indoor'}];
//    ctrl = $controllerConstructor('ModalDevicesModifyController', {
//      $scope: scope,$modalInstance:modalInstance,CloudmidiaApi: CloudmidiaApi,
//      accountId:accountId,deviceGroups:deviceGroups, device:device,DevicesService:DevicesService
//    });
//  }));
//
//  it('testing update function with message type success',function(){
//    scope.selectedGroups = {'selected':[{'id':1,'name':'Empresarial'},{'id':2,'name':'Indoor'}]};
//    scope.device = {'id':1,'name':'teste','groups':[{'id':1,'name':'Empresarial'}]};
//    scope.deviceName = "teste";
//    scope.update();
//    scope.$digest();
//    expect(modalInstance.close).toHaveBeenCalled();
//  });
//
//  it('testing update function with message type error',function(){
//    scope.selectedGroups = {'selected':[{'id':1,'name':'Empresarial'},{'id':2,'name':'Indoor'}]};
//    scope.update();
//    scope.$digest();
//    expect(modalInstance.close).not.toHaveBeenCalled();
//  });
//
//  it('testing this work',function(){
//    expect(ctrl.values.length).toBe(1);
//  });
//});
