'use strict';

angular.module('cloudmidiaApp', [
  'ui.router',
  'ui.utils',
  'ui.bootstrap',
  'oc.lazyLoad',
  'ngResource',
  'ngFileUpload',
  'ngMessages',
  'ngTable',
  'LocalStorageModule',
  'checklist-model',
  'google.places',
  'uiGmapgoogle-maps',
  'angular-ladda',
  'block.exception',
  'block.logger',
  'satellizer'
])
  .run(function ($rootScope, $state, $urlRouter, $location, authService, ProviderService, Authorization, localStorageService,$http) {
    $http.defaults.headers.common['Access-Control-Allow-Origin'] = '*';
    $http.defaults.headers.post['dataType'] = 'json';
    $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams, options) {
      var authentication = authService.authentication();
      if (toState.data.requireAuthentication && !authentication.isAuth) {
        $rootScope.returnToStateParams = toParams.enterpriserSlug !== undefined ? toParams : undefined;
        $state.transitionTo('lite.login', {redirectTo: toState.name});
        event.preventDefault();
      }

      var userPermission = localStorageService.get('permissions');

      switch (toState.name){
        case 'app.campaign':
          $state.go('app.campaign.list',{enterpriserSlug:toParams.enterpriserSlug});
          event.preventDefault();
          break;
        case 'app.advertisers':
          $state.go('app.advertisers.list',{enterpriserSlug:toParams.enterpriserSlug});
          event.preventDefault();
          break;
        case 'app.locals':
          $state.go('app.locals.list',{enterpriserSlug:toParams.enterpriserSlug});
          event.preventDefault();
          break;
        case 'app.devices':
          $state.go('app.devices.list',{enterpriserSlug:toParams.enterpriserSlug});
          event.preventDefault();
          break;
        case 'app.account.users':
          $state.go('app.account.users.list',{enterpriserSlug:toParams.enterpriserSlug});
          event.preventDefault();
          break;
      }

      if(toState.data.action && userPermission &&
        !Authorization.authorized(toState.data.action,toState.data.context,userPermission.data.valueOf())){
        if(toState.name !== 'app.forbidden'){
          $state.go('app.forbidden');
          event.preventDefault();
        }
      }

      if (toParams.enterpriserSlug && authentication.isAuth) {
        ProviderService.verifyEnterpriser(toParams.enterpriserSlug);
      }
    });
  });
