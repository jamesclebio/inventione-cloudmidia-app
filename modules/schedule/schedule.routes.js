;(function () {
  'use strict';

  angular.module('cloudmidiaApp').config([
    '$stateProvider',

    function ($stateProvider) {
      $stateProvider.state('app.schedule', {
        url: '/schedule',
        templateUrl: 'modules/schedule/schedule.html',
        controller: 'scheduleController',
        data:{
          requireAuthentication:true
        },
        resolve: {
          deps: [
            '$ocLazyLoad',

            function ($ocLazyLoad) {
              return $ocLazyLoad.load([
                'select'
              ], {
                insertBefore: '#lazyload_placeholder'
              }).then(function () {
                return $ocLazyLoad.load([
                  'modules/schedule/schedule.controller.js'
                ]);
              });
            }
          ]}
      });
    }
  ]);
})();
