;(function () {
  'use strict';

  angular
    .module('cloudmidiaApp')
    .config(config);

  config.$inject = [
    '$stateProvider'
  ];

  function config($stateProvider) {
    $stateProvider
      .state('app.help', {
        url: '/help',
        templateUrl: 'modules/help/help.html',
        controller: 'HelpController',
        data: {
          requireAuthentication: true
        },
        resolve: {
          deps: ['$ocLazyLoad', function ($ocLazyLoad) {
            return $ocLazyLoad
              .load([], {
                insertBefore: '#lazyload_placeholder'
              })
              .then(function () {
                return $ocLazyLoad.load([
                  'modules/help/help.controller.js'
                ]);
              });
          }]
        }
      });
  }
}());
