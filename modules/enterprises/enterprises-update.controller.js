/**
 * Created by Admin on 4/27/2016.
 */
angular.module('cloudmidiaApp')
  .controller('EnterpriseUpdateController', [
    'AppSettings',
    '$scope',
    'authService',
    'AccountService',
    'Upload',
    '$q',
    '$rootScope',
    'ProviderService',
    'CloudmidiaApi',
    '$state',
    'Exception',
    function (AppSettings,$scope,authService,AccountService,Upload,$q,$rootScope,ProviderService,
              CloudmidiaApi,$state,Exception) {

      'use strict';

      $scope.isLoadingAvailabitily = false;

      var oldSlug = undefined;

      $scope.isValidCharacter = true;

      $scope.alreadyExist = false;

      $scope.account = undefined;

      $scope.baseUrl = AppSettings.urlOrigin + 'p/';

      var enterpriser = undefined;

      var init = function(){
        enterpriser = ProviderService.getCurrentEnterpriser();
        getAccountData();
      };

      $scope.removeImage = function(){
        $scope.account.thumbUrl = undefined;
        $scope.isLoadingImage = false;
      }

      var getAccountData = function(){
        $scope.isLoadingAvailabitily = true;
        authService.profileInfoById(enterpriser.id).then(
          function success(data){
            $scope.isLoadingAvailabitily = false;
            $scope.account = data;
            oldSlug = data.slug;
          },
          function error(err){
            $scope.isLoadingAvailabitily = false;
            Exception.catcherError(err,true);
          });
      };

      $scope.onBlur = function (data) {
        if(data !== undefined && oldSlug !== data && $scope.isValidCharacter){
          //var slug = formatData(data);
          $scope.isLoadingAvailabitily = true;
          CloudmidiaApi.EnterpriserApi.get({'slug': data},
            function success(response) {
              $scope.isLoadingAvailabitily = false;
              if(response.id === undefined){
                $scope.account.slug = data;
                $scope.alreadyExist = false;
              }else{
                $scope.account.slug = data;
                $scope.alreadyExist = true;
              }
            },
            function error(err){
              $scope.isLoadingAvailabitily = false;
              Exception.catcherError(err);
            });
        }else{
          //$scope.account.slug = oldSlug;
          $scope.alreadyExist = false;
        }
      };

      var formatData = function (palavra) {
        if (palavra !== undefined) {
          var comAcento = 'áàãâäéèêëíìîïóòõôöúùûüçÁÀÃÂÄÉÈÊËÍÌÎÏÓÒÕÖÔÚÙÛÜÇ´`^¨~*&$@';
          var semAcento = 'aaaaaeeeeiiiiooooouuuucAAAAAEEEEIIIIOOOOOUUUUC         ';
          for (var l = 0; l < palavra.length; l++) {
            for (var p = 0; p < comAcento.length; p++) {
              if (palavra[l] === comAcento[p]) {
                palavra = palavra.replace(palavra[l], semAcento[p]);
              }
            }
          }
          var lowerCase = palavra.toLowerCase();
          $scope.justSlug = lowerCase.replace(' ', '-');
          return lowerCase.replace(' ', '-');
        }
        else {
          return '';
        }
      };

      $scope.checkInvalidCharacters =  function(slug){
        var pattern = /^[a-zA-Z0-9]*$/;
        $scope.isValidCharacter = pattern.test(slug);
        return $scope.isValidCharacter;
      }

      function calcThumbSize(videoWidth,videoHeight) {
        var maxWidth = 140;
        var maxHeight = 105;
        var ratio = 0;
        var width = videoWidth;
        var height = videoHeight;
        if(videoWidth <= maxWidth && videoHeight <= maxHeight){
          return {'width': width, 'height': height};
        }else{
          if (width > height && width > maxWidth) {
            ratio = maxWidth / width;
            height = height * ratio;
            width = width * ratio;
          } else {
            ratio = maxHeight / height;
            width = width * ratio;
            height = height * ratio;
          }
          return {'width': width, 'height': height};
        }
      }

      var resize = function (file) {
        var deferred = $q.defer();
        var URL = window.URL || window.webkitURL;
        var imgUrl = URL.createObjectURL(file);
        var canvas = document.getElementById('canvas');

        var ctx = canvas.getContext('2d');
        var resizedFileUrl;

        var splitTypeFile = file.type.split('/');
        if (splitTypeFile[0] === 'video') {

          //var video = document.getElementById('video');
          var video = document.createElement('video');
          video.setAttribute('id',imgUrl);
          video.src = imgUrl + '#t=3';
          video.addEventListener('loadeddata', function () {
            var newSize = calcThumbSize(video.videoWidth,video.videoHeight);
            canvas.width = newSize.width;
            canvas.height = newSize.height;
            ctx.drawImage(video, 0, 0,newSize.width,newSize.height);
            resizedFileUrl = canvas.toDataURL('image/jpeg');
            console.log(resizedFileUrl);
            ctx.clearRect(0,0,newSize.width,newSize.height);
            deferred.resolve(resizedFileUrl);
          });

        } else {
          var img = new Image();
          img.src = imgUrl;
          img.setAttribute('id',imgUrl);
          img.onload = function () {
            var newSize = calcThumbSize(img.width,img.height);
            canvas.width = newSize.width;
            canvas.height = newSize.height;
            ctx.drawImage(img, 0, 0, newSize.width, newSize.height);
            resizedFileUrl = canvas.toDataURL('image/jpeg');
            console.log(resizedFileUrl);
            ctx.clearRect(0,0,newSize.width,newSize.height);
            deferred.resolve(resizedFileUrl);
          };
        }
        return deferred.promise;
      };

      // upload on file select or drop
      function doneUploadAll() {
        $scope.isLoading = false;
      }

      $scope.accountUploadMedia = function (files) {
        $scope.files = files;
        $scope.isLoading = true;
        var finishUploadCallback = _.after(files.length, doneUploadAll);
        _.each(files, function (file) {
          resize(file).then(function success(thumb) {
            uploadMedia(file, thumb, finishUploadCallback);
          }).catch(function(message){
            Exception.catcherError(message,true);
          });
        });
      };

      var uploadMedia = function (file, thumb, finishUploadCallback) {

        var url = AppSettings.urlApi + 'Account/' + enterpriser.id + '/media';
        $scope.isLoadingImage = true;

        file.upload = Upload.upload({
          url: url,
          data: {file: [file, Upload.dataUrltoBlob(thumb)]}
        });
        file.upload
          .success(function (resp) {
            $scope.isLoadingImage = false;
            $scope.selectedMediaId = resp.id;
            $scope.account.thumbUrl = resp.thumbUrl;
            file.isSuccess = true;
            finishUploadCallback();
          })
          .error(function (resp) {
            file.isError = true;
            if (resp.status > 0) {
              $scope.errorMsg = true;
            }
          })
          .progress(function (evt) {
            file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
          });
      };

      function validateForm(){
        $scope.enterprise.$setSubmitted();
        if($scope.enterprise.$invalid || $scope.alreadyExist || !$scope.isValidCharacter){
          return true;
        }
        return false;
      }


      $scope.accountUpdate = function(account){
        if(validateForm()){
          return;
        }
        $scope.submitLoading = true;
        AccountService.accountUpdate(account).then(
          function(data)
          {
            $scope.submitLoading = false;
            $state.go('lite.enterprises');

          }
          ).catch(function(message){
            $scope.submitLoading = false;
            Exception.catcherError(message);
          });
      }

      $rootScope.$on('changedEnterpriser', function () {
        enterpriser = ProviderService.getCurrentEnterpriser();
        getAccountData();
      });
      init();
    }
  ]);
