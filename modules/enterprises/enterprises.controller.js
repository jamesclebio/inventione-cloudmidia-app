;(function () {
  'use strict';

  angular
    .module('cloudmidiaApp')
    .controller('EnterprisesController', [
      '$scope',
      'authService',
      '$state',
      'AccountService',
      'ProviderService',
      '$rootScope',
      'Exception',
      'logger',
      '$location',
      function($scope, authService, $state, accountService, ProviderService, $rootScope, Exception) {

        var enterprise = ProviderService.getCurrentEnterpriser();
        $scope.choice = {editar:false,selecionar:false};
        $scope.hasEnterprise = false;
        var role = {adm:'Administrador', owner:'Dono', author:'Autor'};

        var init = function(){
          main.loaderPage.on();
          $scope.enterprises = {};
          enterprise = {};
          $scope.choice.editar = true;
          $scope.choice.selecionar = false;
          if(enterprise === null){
            getUserInfo();
            $scope.choice.editar = false;
            $scope.choice.selecionar = true;
          }else{
            getUserInfo();
          }
        };

        $scope.goToCreateNewEnterprise = function(){
          $state.go('lite.enterprise');
        };

        var getUserInfo = function () {
          if (authService.authentication().isAuth) {
            authService.profileInfo().then(
              function success(data) {
                $scope.profileInfo = data;
                getEnterprises();
              },
              function error(err){
                main.loaderPage.off();
                Exception.catcherError(err,true);
              })
          }
        };

        $rootScope.$on('updateEnterprise', function () {
          getEnterprises();
        });

        $scope.checked = function(slug){

          if(enterprise !== null && slug === enterprise.slug){
            return true;
          }
          return false;
        };

        $scope.goTo = function(slug){
          var allEnterprise = ProviderService.getAllEnterprise();
          var current = _.find(allEnterprise, function(data){
            return data.slug === slug
          });
          authService.getUserPermissions(current.id).then(
            function success(response){}
          );
          if(current.isPublisher && current.role !== role.author)
            $state.go('app.locals',{enterpriserSlug:slug});
          else
            $state.go('app.campaign',{enterpriserSlug:slug});
        };

        $scope.authorized = function(enterprise){
          if(enterprise.role === role.adm || enterprise.role === role.owner)
            return true;
          return false;
        }

        var userHasEnterprise = function(data){
          if(_.find(data,function(user){return user.role === role.adm || user.role === role.owner;})){
            $scope.hasEnterprise = true;
            return;
          }
          $scope.hasEnterprise = false;
        }

        var getEnterprises = function () {
          accountService.getEnterprises($scope.profileInfo.userName).then(
            function success(data) {
              main.loaderPage.off();
              ProviderService.saveAllEnterpriser(data);
              userHasEnterprise(data);

              //Condicional usada quando o usuário edita empresa
              if(ProviderService.getCurrentEnterpriser() !== null){
                ProviderService.updateCurrentEnterprise(data).then(function(){
                  $scope.enterprises = data;
                  $rootScope.$broadcast('updateSlugEnterprise');
                  enterprise = ProviderService.getCurrentEnterpriser();
                });
              }else{
                $scope.enterprises = data;
              }

              //Condicional utilizada em caso de redirecionamento ocorrido no login
              enterprise = ProviderService.getCurrentEnterpriser();
              if($state.params.redirectTo && enterprise){
                $rootScope.$broadcast('changedEnterpriser');
                $state.go($state.params.redirectTo, $rootScope.returnToStateParams);
              }

              if($state.params.redirectTo && !enterprise){
                $state.go($state.params.redirectTo,{enterpriserSlug:data[0].slug});
              }

              //Redirecionando caso o usuário somente tenha uma empresa.
              if(data.length === 1 && !$state.params.redirectTo && !ProviderService.getCurrentEnterpriser()){
                $scope.goTo(data[0].slug);
              }
            },
            function error(err){
              main.loaderPage.off();
              Exception.catcherError(err,true);
            }
          )
        };

        init();
      }
    ]);
})();
