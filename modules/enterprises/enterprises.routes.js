;(function () {
  'use strict';

  angular
    .module('cloudmidiaApp')
    .config([
      '$stateProvider',

      function ($stateProvider) {
        $stateProvider.state('lite.enterprises', {
          url: '/enterprises?redirectTo',
          cache:false,
          templateUrl: 'modules/enterprises/enterprises.html',
          controller: 'EnterprisesController',
          data: {
            requireAuthentication: true
          },
          resolve: {
            deps: [
              '$ocLazyLoad',

              function ($ocLazyLoad) {
                return $ocLazyLoad
                  .load([
                    'select'
                  ], {
                    insertBefore: '#lazyload_placeholder'
                  })
                  .then(function () {
                    return $ocLazyLoad.load([
                      'filters/propsFilter.js',
                      'services/account.service.js',
                      'modules/enterprises/enterprises.controller.js',
                      'services/auth.service.js',
                      'services/provider.services.js'
                    ]);
                  });
              }
            ]
          }
        }),
          $stateProvider.state('lite.update',{
            url:'/p/:enterpriserSlug/update',
            templateUrl:'modules/enterprises/enterprises-update.html',
            controller:'EnterpriseUpdateController',
            data:{
              requireAuthentication:true
            },
            resolve:{
              deps:['$ocLazyLoad',function ($ocLazyLoad) {
                  return $ocLazyLoad.load([
                      'select',
                    'underscore',
                    ], {
                      insertBefore: '#lazyload_placeholder'
                    }).then(function () {
                      return $ocLazyLoad.load([
                        'filters/propsFilter.js',
                        'modules/enterprises/enterprises-update.controller.js',
                        'services/auth.service.js',
                        'directives/password-check.js',
                        'services/account.service.js',
                        'services/cloudmidia-api.service.js'
                      ]);
                    });
                }]
            }
          }),
          $stateProvider.state('lite.enterprise',{
            url:'/create',
            templateUrl:'modules/enterprises/enterprises-create.html',
            controller:'EnterpriseCreateController',
            data:{
              requireAuthentication:true
            },
            resolve:{
              deps:['$ocLazyLoad',function ($ocLazyLoad) {
                return $ocLazyLoad.load([
                  'select',
                  'underscore',
                ], {
                  insertBefore: '#lazyload_placeholder'
                }).then(function () {
                  return $ocLazyLoad.load([
                    'filters/propsFilter.js',
                    'modules/enterprises/enterprises-create.controller.js',
                    'services/auth.service.js',
                    'services/account.service.js',
                    'services/cloudmidia-api.service.js'
                  ]);
                });
              }]
            }
          });
      }
    ]);
})();
