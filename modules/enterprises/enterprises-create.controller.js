/**
 * Created by Admin on 4/27/2016.
 */
angular.module('cloudmidiaApp')
  .controller('EnterpriseCreateController', [
    'AppSettings',
    '$scope',
    'authService',
    'AccountService',
    'Upload',
    '$q',
    '$rootScope',
    'ProviderService',
    'CloudmidiaApi',
    '$state',
    'Exception',
    function (AppSettings,$scope,authService,AccountService,Upload,$q,$rootScope,ProviderService,
              CloudmidiaApi,$state,Exception) {

      $scope.account = {};
      $scope.baseUrl = AppSettings.urlOrigin + 'p/';

      var enterpriser;

      var profileInf = authService.getSavedProfileInfo();

      if(profileInf){
        $scope.account.email = profileInf.userName;
      }

      var init = function(){
        enterpriser = ProviderService.getCurrentEnterpriser();
        if(!enterpriser){
          var allEnterprises = ProviderService.getAllEnterprise();
          enterpriser = _.first(allEnterprises);
        }
      };

      $scope.onBlur = function (data) {
        if(data !== undefined) {
          var slug = formatData(data);
          $scope.isLoadingAvailabitily = true;
          CloudmidiaApi.EnterpriserApi.get({'slug': slug},
            function success(response) {
              $scope.isLoadingAvailabitily = false;
              if (response.id === undefined) {
                $scope.account.slug = slug;
                $scope.alreadyExist = false;
              } else {
                $scope.account.slug = slug;
                $scope.alreadyExist = true;
              }
            },
            function error(err) {
              $scope.isLoadingAvailabitily = false;
              Exception.catcherError(err);
            });
        }
      };

      var formatData = function (palavra) {
        if (palavra !== undefined) {
          var comAcento = 'áàãâäéèêëíìîïóòõôöúùûüçÁÀÃÂÄÉÈÊËÍÌÎÏÓÒÕÖÔÚÙÛÜÇ´`^¨~';
          var semAcento = 'aaaaaeeeeiiiiooooouuuucAAAAAEEEEIIIIOOOOOUUUUC     ';
          for (var l = 0; l < palavra.length; l++) {
            for (var p = 0; p < comAcento.length; p++) {
              if (palavra[l] === comAcento[p]) {
                palavra = palavra.replace(palavra[l], semAcento[p]);
              }
            }
          }
          var lowerCase = palavra.toLowerCase();
          $scope.justSlug = lowerCase.replace(' ', '-');
          return lowerCase.replace(' ', '-');
        }
        else {
          return '';
        }
      };

      $scope.register = function () {
        $scope.enterprise.$setSubmitted();
        if($scope.enterprise.$invalid || $scope.alreadyExist){
          return;
        }
        $scope.submitLoading = true;
        $scope.account.slug = '';
        $scope.account.isPublisher = true;
        CloudmidiaApi.RegisterCompanyApi.save({accountId: enterpriser.id, slug: $scope.account.slug,
            accountName: $scope.account.accountName, email:$scope.account.email, isPublisher: $scope.account.isPublisher},
          function success(data) {
            $scope.submitLoading = false;
            Exception.catcherSuccess(data).then(
              function success(result){
                if (result.successes.length > 0 && result.successes[0].type === 'SUCCESS') {
                  $state.go('lite.enterprises');
                }
                else
                  return;
              }
            )
          },
          function error(err) {
            $scope.submitLoading = false;
            Exception.catcherError(err);
          });
      };

      init();

    }
  ]);
