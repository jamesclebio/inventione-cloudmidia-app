;(function () {
  'use strict';

  angular
    .module('virtualPage', [])
    .run(function ($rootScope) {
      if (analytics) {
        $rootScope.$on('$stateChangeStart', function(event, next, current) {
          if (next.templateUrl) {
            ga('send', 'pageview', {
              page: next.templateUrl
            });
          }
        });
      }
    });
})();
