angular.module('cloudmidiaApp')
  .config(['$stateProvider', function ($stateProvider) {
    $stateProvider
      .state('app.not-found', {
        url: '/404',
        templateUrl: 'modules/errors/404.html',
        data:{
          requireAuthentication:false
        }
      })
      .state('app.internal-server',{
        url: '/500',
        templateUrl: 'modules/errors/500.html',
        data:{
          requireAuthentication:false
        }
      })
      .state('app.forbidden',{
        url:'/403',
        templateUrl:'modules/errors/403.html',
        data:{
          requireAuthentication:false
        }
      });
  }]);
