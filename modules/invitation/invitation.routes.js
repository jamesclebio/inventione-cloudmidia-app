angular.module('cloudmidiaApp')
  .config(['$stateProvider', function ($stateProvider) {
    $stateProvider
      .state('/invitation',{
        url:'/accept-invitation-url?token',
        templateUrl:'modules/invitation/accept-invitation.html',
        controller:'InvitationController',
        data:{
          requireAuthentication:false
        },
        resolve: {
          deps: ['$ocLazyLoad', function ($ocLazyLoad) {
            return $ocLazyLoad.load([
              'underscore'
            ], {
              insertBefore: '#lazyload_placeholder'
            })
              .then(function () {
                return $ocLazyLoad.load([
                  'services/cloudmidia-api.service.js',
                  'services/invitation.service.js',
                  'modules/invitation/accept-invitation.controller.js',
                  'directives/password-check.js'
                ]);
              });
          }]
        }
      });
  }]);
