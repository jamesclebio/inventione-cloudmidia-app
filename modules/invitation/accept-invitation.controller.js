angular.module('cloudmidiaApp')
  .controller('InvitationController', ['$scope',
    'CloudmidiaApi',
    '$state',
    '$location',
    '$stateParams',
    '$http',
    'AppSettings',
    'authService',
    'InvitationService',
    '$timeout',
    'Exception',
    function ($scope, CloudmidiaApi, $state, $location, $stateParams, $http, AppSettings,
              authService, InvitationService,$timeout,Exception) {
      'use strict';

      $scope.showInvitation = false;

      var token = $stateParams.token;

      var redirectTo = $location.url();

      var redirect = function (url) {
        $location.path(url);
        $location.url($location.path());
      };

      $scope.user = {};

      $scope.user.token = token;

      var init = function () {
        getInvitationInformation();
      };

      var getInvitationInformation = function () {
        CloudmidiaApi.InvitationInformationApi.get({'token': token},
          function success(data) {
            $scope.invitedUser = data;
            if (data.isUser) {
              $scope.isUser = true;
            }
            $scope.showInvitation = true;
            var authentication = authService.authentication();
            if (authentication.isAuth) {
              $scope.acceptInviteUser();
            }
          });
      };

      $scope.acceptInviteUser = function (portlet) {
        InvitationService.acceptInvitation($scope.invitedUser.accountId,token).then(
          function success(data){
            $timeout(function() {
              $(portlet).portlet({
                refresh: false
              });
            }, 1000);
            if(data !== undefined){
              $location.url('/home');
            }
          }).catch(function(message){
            Exception.catcherError(message,true);
          });
      };

      $scope.acceptInvitation = function (user) {
        $scope.user.accountId = $scope.invitedUser.accountId;
        $scope.dataLogin = {'userName': $scope.invitedUser.email, 'password': $scope.user.password};
        InvitationService.acceptRegisterInvitation($scope.invitedUser.accountId,user).then(
          function success(response){
            if (response.data.messages[0].type === 'SUCCESS') {
              authService.login($scope.dataLogin).then(
                function success (response) {
                  $location.url('/home');
                },
                function error(){
                }
              );
            } else {
              return;
            }
          }
        ).catch(function(message){
            Exception.catcherError(message,true);
          });
      };
      init();
    }])
