angular.module('cloudmidiaApp')
  .controller('UpdateCampaignController',
  ['$scope',
    '$state',
    'CloudmidiaApi',
    '$stateParams',
    'Upload',
    'AppSettings',
    'authService',
    'campaignAction',
    'CampaignService',
    '$q',
    '$timeout',
    '$modal',
    'AdvertisersService',
    'ProviderService',
    'InstagramService',
    '$rootScope',
    '$location',
    'Exception',
    '$auth',
    '$window',
    function ($scope, $state, CloudmidiaApi, $stateParams, Upload, AppSettings, authService, campaignAction,
              CampaignService, $q, $timeout, $modal, AdvertisersService,
              ProviderService,InstagramService, $rootScope, $location, Exception, $auth, $window) {

      'use strict';

      $scope.isLoadingAvailabitily = true;

      $scope.checked = function (value) {
        if($scope.campaign.content !== value)
          $scope.removeContent();

        $scope.campaign.content = value;
        $scope.content = false;
        if($auth.getToken() && value === 3){
          verifySocialAuthentication();
        }
      };

      $scope.newAccessToken = false;

      $scope.campaignAction = campaignAction;

      $scope.existSocialUser = $auth.getToken() ? true : false;

      $scope.exhibitionCalc = function () {
        var days = parseInt(new Date($scope.campaign.endDate) - new Date($scope.campaign.startDate)) / (1000 * 60 * 60 * 24),
          sum = 0;

        if ($scope.locals) {
          for (var i = 0; i < $scope.locals.length; i++) {
            // 8 represents the bussiness hours of locals
            sum += 2 * $scope.locals[i].boosting * days * 8;
          }
        }
        $scope.exhibition = sum;
      };

      $scope.switchSocialAuthentication = function(){
        var wnd = $window.open("http://www.instagram.com/accounts/logout",'Logout','width=400,height=400');
        setTimeout(function() {
          $auth.removeToken();
          wnd.close();
        }, 1000);
        $scope.existSocialUser = false;
        return false;
      };

      $scope.instagramLogin = function(){
        $auth.authenticate('instagram').then(
          function(response){
            callback(response);
          },
          function(erro){
            console.log(erro);
          }
        )
      };


      var callback = function(data){
        InstagramService.authenticate($scope.enterpriser.id,data.code).then(
          function success(data){
            $auth.setToken(data.messages[0].parameters);
            $scope.newAccessToken = true;
            verifySocialAuthentication();
          }
        )
      };

      var verifySocialAuthentication = function(){
        InstagramService.verifyAuthentication($scope.enterpriser.id, $auth.getToken()).then(
          function success(data){
            if(data.messages && data.messages[0].type === "ERROR"){
              Exception.catcherSuccess(data).then(
                function (){
                  $scope.existSocialUser = false;
                })
            }else{
              $scope.existSocialUser = $auth.getToken() ? true : false;
              $scope.newAccessToken = true;
              $scope.userName = data.data.userName;
              getSocialMedia($auth.getToken());
            }
          }
        )
      };

      var getSocialMedia = function (accessToken){
        CloudmidiaApi.SocialMediaApi.get({accountId:$scope.enterpriser.id, accessToken:accessToken},
          function success(data){
            if(data.id === undefined){
              Exception.catcherSuccess(data).then(
                function(){
                }
              );
            }else{
              $scope.content = true;
              $scope.newAccessToken = true;
              $scope.campaign.media.id = data.id;
              $scope.campaign.media.name = data.name;
              $scope.campaign.media.thumbUrl = data.thumbUrl;
              $scope.campaign.media.url = data.url;
            }
          },
          function error(err){
            Exception.catcherError(err);
          })
      };

      $scope.$watch('selectedAdvertiser.selected', function(){
        if($scope.campaignForm.$submitted)
          $scope.selectAdvertiser = $scope.selectedAdvertiser.selected.length <= 0 ? true : false;
      });

      var calcCost = function (boosting) {
        if ($scope.locals) {
          var sum = 0;

          for (var i = 0; i < $scope.locals.length; i++) {
            sum += $scope.locals[i].price / 30;
          }
          return sum * boosting * parseInt(new Date($scope.campaign.endDate) - new Date($scope.campaign.startDate)) / (1000 * 60 * 60 * 24);
        }
        return 0;
      };


      $scope.generalCalc = function () {
        var sumDevice = 0,
          sumBoosting = 0,
          sumViewers = 0;

        $scope.exhibitionCalc();

        if ($scope.locals) {
          for (var i = 0; i < $scope.locals.length; i++) {
            sumDevice += $scope.locals[i].devices.length;
            sumBoosting += $scope.locals[i].boosting;
            sumViewers += $scope.locals[i].quantityOfViewers;
          }
        }

        $scope.viewers = sumViewers * parseInt(new Date($scope.campaign.endDate) - new Date($scope.campaign.startDate)) / (1000 * 60 * 60 * 24);
        $scope.percentage = (sumBoosting / sumDevice) * 100;
        $scope.cost = calcCost(sumBoosting);
        reload($scope.percentage, $scope.exhibition, $scope.viewers);
      }



      var reload = function (percent, exhibition, impact) {
        $timeout(function () {
          $scope.$apply(function () {
            main.impact.builder(percent, exhibition, impact);
          });
        }, 0);
      };

      $scope.removeContent = function () {
        $scope.content = false;
        $scope.campaign.media.id = undefined;
        $scope.campaign.media.name = undefined;
        $scope.campaign.media.thumbUrl = undefined;
        $scope.campaign.media.url = undefined;
      }

      var init = function () {
        $scope.selectAdvertiser = false;
        $scope.enterpriser = ProviderService.getCurrentEnterpriser();
        if ($scope.enterpriser !== null) {
          $q.all([
            CampaignService.getCampaign($scope.enterpriser.id, $stateParams.id),
            AdvertisersService.getAllAdvertisers($scope.enterpriser.id)
          ]).then(
            function success(data) {
            $scope.isLoadingAvailabitily = false;
            initScopeVariables(data[0]);
            $scope.advertisers = data[1];

          },
          function error(err){
            $scope.isLoadingAvailabitily = false;
            Exception.catcherError(err,true);
          })
        }

        $scope.fixedDate = [
          {
            id: 1,
            value: 7,
            checked: false
          }, {
            id: 2,
            value: 15,
            checked: false
          }, {
            id: 3,
            value: 30,
            checked: false
          }
        ];
      };

      $scope.availableItems = [];

      $scope.selectedAdvertiser = {selected: []};

      function initScopeVariables(campaign) {
        $scope.campaign = campaign;
        $scope.selectedAdvertiser = campaign.advertiser !== undefined ? {selected: campaign.advertiser} : {selected: []};
        $scope.locals = campaign.localsDevices !== undefined ? campaign.localsDevices : '';
        $scope.generalCalc();
        $scope.campaign.startDate = new Date(campaign.startDate);
        $scope.campaign.endDate = $scope.campaign.leftDays > 0 ? '' : new Date(campaign.endDate);
        $scope.mediaFilename = campaign.media.name;
        $scope.generalInfoClass = campaign.type === 1 ? 'col-lg-7 col-md-6' : 'col-md-12';
        $scope.resultItems = campaign.devices;
        $scope.campaign.content = campaign.type;
        $scope.content = true;
        $scope.duration = campaign.duration;
        $scope.userName = campaign.socialUserName;

      }

      // DatePickers' configurations
      $scope.dtConfig = {
        minDate: $scope.minDate ? null : new Date(),
        maxDate: new Date(2020, 5, 22),
        selectBegin: function () {
          return false;
        },
        selectEnd: function () {
          return false;
        }
      };

      $scope.status = {
        startOpened: false,
        endOpened: false
      };

      $scope.openStartDatepicker = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.status.startOpened = !$scope.status.startOpened;
      };

      $scope.openEndDatepicker = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.status.endOpened = !$scope.status.endOpened;
      };

      $scope.selectedItems = {};

      var mediaFilter= {
        0: ['image'],
        1: ['video'],
        2: ['image']
      };

      $scope.openFileLibraryModal = function (campaignType) {
        var modalInstance = $modal.open({
          templateUrl: 'modules/upload/modal-file-upload.html',
          controller: 'ModalFileUploadController',
          windowClass: 'fade stick-up',
          size: 'lg',
          backdrop: 'static',
          keyboard: false,
          resolve: {
            accountId: function () {
              return $scope.enterpriser.id;
            },
            mediaFilter: function () {
              return mediaFilter[$scope.campaign.content];
            }
          }
        });

        modalInstance.result.then(function (selectedItem) {
          if (!angular.isUndefined(selectedItem)) {
            $scope.content = true;
            $scope.campaign.media.id = selectedItem.id;
            $scope.campaign.media.thumbUrl = selectedItem.thumbUrl;
            $scope.campaign.media.name = selectedItem.name;
            $scope.campaign.media.url = selectedItem.url;
          }
        });
      };

      function validateForm(){
        $scope.campaignForm.$setSubmitted();
        if($scope.campaignForm.$invalid || $scope.selectedAdvertiser.selected.length <= 0){
          $scope.selectAdvertiser = $scope.selectedAdvertiser.selected.length <= 0 ? true : false;
          main.scrollTop.run();
          return true;
        }
        return false;
      }

      $scope.typeOfCampaign = {
        1: {description: 'imagem', pattern: 'image/*'},
        2: {description: 'imagem ou vídeo', pattern: 'image/*,video/*'}
      };

      $scope.save = function(portlet){
        if(validateForm()){
          return;
        }
        $scope.submitLoadingSave = true;
        send(portlet,true);
      };

      $scope.saveAndPublish = function(portlet){
        if(validateForm()){
          return;
        }
        $scope.submitLoadingSavePub = true;
        send(portlet,false)

      };

      // Completes the action for updating the campaign
      var send = function (portlet,save) {

        // Request data
        var campaign = {
          accountId: $scope.campaign.accountId = $scope.enterpriser.id,
          campaignId: $scope.campaign.campaignId = $scope.campaign.id,
          publish:!save ? true : $scope.campaign.publish,
          interrupted: $scope.campaign.interrupted && save ? $scope.campaign.interrupted : false,
          name: $scope.campaign.name,
          advertiserId: $scope.selectedAdvertiser.selected.advertiserId,
          type: $scope.campaign.content,
          duration:$scope.campaign.duration
        };
        switch($scope.campaign.content){
          case 0: // Adding an IMAGE
            campaign.media = {id: $scope.campaign.media.id};
            break;

          case 1:// Adding a VIDEO
            campaign.media = {id: $scope.campaign.media.id};
            break;
          case 3:
                campaign.media = {id:$scope.newAccessToken ? 3 : $scope.campaign.media.id};
                campaign.accessToken = $scope.newAccessToken ? $auth.getToken() : "";
                break;
          default :// Adding a TEXT and IMAGE
            campaign.media = { // If we are adding a NEWS
              title: $scope.campaign.media.title,
              subtitle: $scope.campaign.media.subtitle,
              text: $scope.campaign.media.text,
              source: $scope.campaign.media.source,
              id: $scope.campaign.media.id
            };
            break;
        };
        // Dispatch request
        CloudmidiaApi.CampaignApi.update(campaign,
          function success(response) {
            $scope.submitLoadingSave= false;
            $scope.submitLoadingSavePub = false;
            $timeout(function () {
              $(portlet).portlet({
                refresh: false
              });
            }, 500);
            Exception.catcherSuccess(response).then(
              function success(result){
              }
            )
          },
          function error(err) {
            $scope.submitLoadingSave = false;
            $scope.submitLoadingSavePub = false;
            Exception.catcherError(err);
          });
      };

      $scope.cancel = function () {
        $state.go('app.campaign', {enterpriserSlug: $scope.enterpriser.slug});
      };

      init();

    }]);


