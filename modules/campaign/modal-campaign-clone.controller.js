/**
 * Created by Admin on 10/27/2016.
 */
angular.module('cloudmidiaApp')
  .controller('ModalCampaignsCloneController',
  ['$scope',
    '$q',
    '$modal',
    '$log',
    '$modalInstance',
    'CampaignService',
    'accountId',
    'campaign',
    'Exception',
    function ($scope, $q, $modal, $log, $modalInstance, CampaignService, accountId, campaign, Exception) {
      $scope.campaign = campaign;

      $scope.ok = function () {
        $scope.submitLoading = true;
        CampaignService.cloneCampaign(accountId, campaign.id).then(
          function success(response) {
            $scope.submitLoading = false;
            Exception.catcherSuccess(response).then(
              function success(result){
                if (result.successes[0].type === 'SUCCESS') {
                  $modalInstance.close();
                }
              })
          },
          function error(err){
            $scope.submitLoading = false;
            Exception.catcherError(err);
          });
      };

      $scope.cancel = function () {
        $modalInstance.dismiss();
      };

    }]);

