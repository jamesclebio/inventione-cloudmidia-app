;(function () {
  'use strict';

  angular
    .module('cloudmidiaApp')
    .controller('CampaignModifyBasicController', CampaignModifyBasicController);

  CampaignModifyBasicController.$inject = [
    '$scope',
    '$state',
    '$q',
    '$stateParams',
    'ProviderService',
    'Exception',
    'AdvertisersService',
    'CloudmidiaApi',
    'CampaignService',
    '$rootScope'
  ];

  function CampaignModifyBasicController($scope, $state, $q, $stateParams, ProviderService, Exception, AdvertisersService,
                                         CloudmidiaApi, CampaignService,$rootScope) {

    // Sample code
    $('.card-float').addClass('on');

    var today = new Date(),
      tomorrow = new Date();

    $scope.enterpriser = ProviderService.getCurrentEnterpriser();

    $scope.selectedAdvertiser = {selected: []};

    $scope.selectAdvertiser = false;

    tomorrow.setDate(tomorrow.getDate() + 1);

    $scope.campaign = {
      accountId: $scope.enterpriser.id,
      campaignId:$stateParams.campaignId,
      name: '',
      startDate: today,
      endDate: tomorrow,
    };

    var init = function () {
      $scope.selectAdvertiser = false;
      main.loaderPage.on();
      if ($scope.enterpriser !== null) {
        $q.all([
          CampaignService.getBasicCampaign($scope.enterpriser.id, $stateParams.campaignId),
          AdvertisersService.getAllAdvertisers($scope.enterpriser.id)
        ]).then(
          function success(data) {
            main.loaderPage.off();
            initScopeVariables(data[0]);
            $scope.advertisers = data[1];

          },
          function error(err){
            Exception.catcherError(err,true);
          })
      }
    };

    function initScopeVariables(campaign) {
      $scope.campaign.name = campaign.name ? campaign.name : '';
      $scope.selectedAdvertiser = campaign.advertiser !== undefined ? {selected: campaign.advertiser} : {selected: []};
      $scope.campaign.startDate = new Date(campaign.startDate);
      if(campaign.status)
        $scope.campaign.endDate = $scope.campaign.leftDays > 0 ? '' : new Date(campaign.endDate);
      else
        $scope.campaign.endDate = new Date(campaign.endDate);

      $scope.campaign.status = campaign.status;
    }

   init();

    // Default dates
    $scope.fixedDate = [
      {
        id: 1,
        value: 7,
        checked: false
      }, {
        id: 2,
        value: 15,
        checked: false
      }, {
        id: 3,
        value: 30,
        checked: false
      }
    ];

    // DatePickers' configurations
    $scope.dtConfigStart = {
      minDate: $scope.minDate ? null : new Date(),
      maxDate: new Date(2020, 5, 22),
      selectBegin: function () {
        return false;
      },
      selectEnd: function () {
        return false;
      }
    };

    $scope.dtConfigEnd ={
      minDate: $scope.campaign.startDate ? new Date($scope.campaign.startDate.getTime()+ (24*3500*1000)) : new Date(),
      maxDate: new Date(2020, 5, 22),
      selectBegin: function () {
        return false;
      },
      selectEnd: function () {
        return false;
      }
    }

    $scope.status = {
      startOpened: false,
      endOpened: false
    };

    $scope.openStartDatepicker = function ($event) {
      $event.preventDefault();
      $event.stopPropagation();
      $scope.status.startOpened = !$scope.status.startOpened;
    };

    $scope.$watch('campaign.startDate',function(){
      var numDays = 0;
      var today = new Date();
      $scope.dtConfigEnd.minDate = new Date($scope.campaign.startDate.getTime() + getMillisenconds(1));
      if($scope.campaign.startDate.getDate()){
        for(var i = 0; i < $scope.fixedDate.length; i++){
          if($scope.fixedDate[i].checked === true){
            numDays = $scope.fixedDate[i].value;
            break;
          }
        }
        if(numDays > 0 && $scope.campaign.startDate.getDate() !== today.getDate()){
          $scope.campaign.endDate = new Date($scope.campaign.startDate.getTime() + getMillisenconds(numDays));
        }
      }
    });

    var getMillisenconds = function(data){
      return  (data * 24 * 3600 * 1000);
    }

    $scope.openEndDatepicker = function ($event) {
      $event.preventDefault();
      $event.stopPropagation();
      $scope.status.endOpened = !$scope.status.endOpened;
    };

    $scope.select = function (date) {
      var today = new Date();

      for (var i=0; i < $scope.fixedDate.length; i++) {
        if ($scope.fixedDate[i].id === date.id && $scope.fixedDate[i].checked == false) {
          $scope.fixedDate[i].checked = true;
          $scope.campaign.endDate = new Date($scope.campaign.startDate.getTime() + getMillisenconds(date.value));
        } else {
          $scope.fixedDate[i].checked = false;
        }
      }
    };

    $scope.$watch('selectedAdvertiser.selected', function(){
      if($scope.campaignForm.$submitted)
        $scope.selectAdvertiser = $scope.selectedAdvertiser.selected.length <= 0 ? true : false;

    });

    $scope.select($scope.fixedDate[2]);

    // Get advertisers
    var advertisers = function () {
      AdvertisersService.getAllAdvertisers($scope.enterpriser.id).then(
        function success(data) {
          $scope.Advertisers = data;
        },
        function error(err) {
          Exception.catcherError(err);
        }
      )
    };

    advertisers();

    // Validate form
    function validateForm(){
      $scope.campaignForm.$setSubmitted();
      if(($scope.campaignForm.$invalid || $scope.selectedAdvertiser.selected.length <= 0 )){
        $scope.selectAdvertiser = $scope.selectedAdvertiser.selected.length <= 0 ? true : false;
        main.scrollTop.run();
        return true;
      }
      return false;
    }

    $scope.create = function(){
      $scope.loadingSubmitNext = true;
      if(validateForm()){
        $scope.loadingSubmitNext = false;
        return;
      }

      var campaign = {
        accountId: $scope.campaign.accountId,
        name: $scope.campaign.name,
        campaignId:$scope.campaign.campaignId,
        advertiserId: $scope.selectedAdvertiser.selected.advertiserId,
        startDate: $scope.campaign.startDate,
        endDate: $scope.campaign.endDate,
      };

      CloudmidiaApi.CampaignBasicApi.update(campaign,
        function success(response){
          Exception.catcherSuccess(response).then(
            function success(result){
              $scope.loadingSubmitNext = false;
              $rootScope.$broadcast('changedBasic',{any:$stateParams.campaignId});
              $state.go('app.campaign.modify.content', {enterpriserSlug: $scope.enterpriser.slug});
            }
          )
        },function error(err){
          Exception.catcherError(err);
          $scope.LoadingSubmitNext = false;
        })
    }
  }
})();
