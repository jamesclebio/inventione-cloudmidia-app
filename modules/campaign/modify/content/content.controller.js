;(function () {
  'use strict';

  angular
    .module('cloudmidiaApp')
    .controller('CampaignModifyContentController', CampaignModifyContentController);

  CampaignModifyContentController.$inject = [
    '$scope',
    '$state',
    '$stateParams',
    '$modal',
    'CloudmidiaApi',
    'ProviderService',
    'InstagramService',
    'Exception',
    '$auth',
    'CampaignService',
    '$rootScope',
    '$window',
  ];

  function CampaignModifyContentController($scope, $state, $stateParams, $modal, CloudmidiaApi, ProviderService,
                                           InstagramService, Exception, $auth,CampaignService,$rootScope, $window) {

    // Sample code
    $('.card-float').addClass('on');

    var campaignTypes = {
        '1': {
          id: 1,
          name: 'Notícia'
        },
        '2': {
          id: 2,
          name: 'Comercial'
        }
      },
      typeOfContent = {
        'image': {id: 0},
        'video': {id: 1},
        'textAndImage': {id: 2},
        'social':{id:3}
      };

    var defaultSocialMidiaId = 3;

    //$scope.needAuthenticate = true;

    $scope.content = false;

    $scope.enterpriser = ProviderService.getCurrentEnterpriser();

    $scope.existSocialUser = $auth.getToken() ? true : false;

    $scope.newAccessToken = false;


    $scope.campaign = {
      accountId: $scope.enterpriser.id,
      content: typeOfContent['image'].id,
      duration: 3,
      campaignId:$stateParams.campaignId,
      media: {
        title: '',
        subtitle: '',
        text: '',
        source: '',
        id: null
      }
    };

    var getContentCampaign = function(){
      main.loaderPage.on();
      CampaignService.getContentCampaign($scope.enterpriser.id,$stateParams.campaignId).then(
        function success(data){
          $scope.campaign.content = data.type;
          main.loaderPage.off();
          if(data.duration){
            $scope.content = true;
            $scope.campaign.duration = data.duration;
            $scope.campaign.media = data.media;
            $scope.campaign.type = data.type;
            $scope.socialUserName = data.socialUserName;
          }else{
            $scope.campaign.type = typeOfContent.image.id;
          }
        },
        function error(err){
          Exception.catcherError(err);
        }
      )
    };

    getContentCampaign();

    // Social login
    $scope.instagramLogin = function(){
      $auth.authenticate('instagram').then(
        function(response){
          callback(response);
        },
        function(erro){
          console.log(erro);
        }
      )
    };

    var callback = function(data){
      InstagramService.authenticate($scope.enterpriser.id,data.code).then(
        function success(data){
          $auth.setToken(data.messages[0].parameters);
          $scope.newAccessToken = true;
          verifySocialAuthentication();
        }
      )
    };

    var verifySocialAuthentication = function(){
      InstagramService.verifyAuthentication($scope.enterpriser.id, $auth.getToken()).then(
        function success(data){
          if(data.messages && data.messages[0].type === "ERROR"){
            Exception.catcherSuccess(data).then(
              function (){
                $scope.existSocialUser = false;
              })
          }else{
            $scope.existSocialUser = $auth.getToken() ? true : false;
            //$scope.needAuthenticate = false;
            $scope.newAccessToken = true;
            $scope.socialUserName = data.data.userName;
            getSocialMedia($auth.getToken());
          }
        }
      )
    };

    $scope.checked = function (value) {
      if($scope.campaign.content !== value)
        $scope.removeContent();

      $scope.campaign.content = value;
      $scope.content = false;
      if($auth.getToken() && value === 3){
        verifySocialAuthentication();
      }
    };

    // Change social user
    $scope.switchSocialAuthentication = function(){
      var wnd = $window.open("http://www.instagram.com/accounts/logout",'Logout','width=400,height=400');
      setTimeout(function() {
        $auth.removeToken();
        wnd.close();
      }, 1000);
      $scope.removeContent();
      //$scope.needAuthenticate = true;
      $scope.existSocialUser = false;
      return false;
    };

    // Get social media
    var getSocialMedia = function (accessToken){
      CloudmidiaApi.SocialMediaApi.get({accountId:$scope.campaign.accountId, accessToken:accessToken},
        function success(data){
          if(data.id === undefined){
            Exception.catcherSuccess(data).then(
              function(){
              }
            );
          }else{
            $scope.content = true;
            $scope.newAccessToken = true;
            $scope.campaign.media.id = data.id;
            $scope.campaign.media.name = data.name;
            $scope.campaign.media.thumbUrl = data.thumbUrl;
            $scope.campaign.media.url = data.url;
          }
        },
        function error(err){
          Exception.catcherError(err);
        })
    };

    // Used to select the type of media such as: image, video etc
    var mediaFilter= {
      0: ['image'],
      1: ['video'],
      2: ['image'],
      3: ['social']
    };

    // Used to select media if it is a video or image
    $scope.openFileLibraryModal = function () {
      var modalInstance = $modal.open({
        templateUrl: 'modules/upload/modal-file-upload.html',
        controller: 'ModalFileUploadController',
        windowClass: 'fade stick-up',
        size: 'lg',
        backdrop: 'static',
        keyboard: false,
        resolve: {
          accountId: function () {
            return $scope.enterpriser.id;
          },
          mediaFilter: function () {
            return mediaFilter[$scope.campaign.content];
          }
        }
      });

      modalInstance.result.then(function (selectedItem) {
        if (!angular.isUndefined(selectedItem)) {
          $scope.content = true;
          $scope.campaign.media.id = selectedItem.id;
          $scope.campaign.media.name = selectedItem.name;
          $scope.campaign.media.thumbUrl = selectedItem.thumbUrl;
          $scope.campaign.media.url = selectedItem.url;
        }
      });
    };

    // Remove all content related to media
    $scope.removeContent = function () {
      $scope.content = false;
      $scope.campaign.media.id = undefined;
      $scope.campaign.media.name = undefined;
      $scope.campaign.media.thumbUrl = undefined;
      $scope.campaign.media.url = undefined;
    }

    // Validate form
    function validateForm(){
      $scope.campaignForm.$setSubmitted();
      if(($scope.campaignForm.$invalid || $scope.campaign.media.url === undefined )
        && !($scope.campaign.content === 3)){
        main.scrollTop.run();
        return true;
      }
      return false;
    }

    $scope.saveContent = function(){
      $scope.loadingSubmitNext = true;
      if(validateForm()){
        $scope.loadingSubmitNext = false;
        return;
      }

      var campaign = {
        accountId: $scope.campaign.accountId,
        type: $scope.campaign.content,
        interrupted:false,
        duration:$scope.campaign.duration,
        campaignId:$scope.campaign.campaignId
      }
      switch ($scope.campaign.content) {
        case 0: // Adding an IMAGE
          campaign.media = {id: $scope.campaign.media.id};
          break;

        case 1:// Adding a VIDEO
          campaign.media = {id: $scope.campaign.media.id};
          break;

        case 2:// Adding a TEXT and IMAGE
          campaign.media = { // If we are adding a NEWS
            title: $scope.campaign.media.title,
            subtitle: $scope.campaign.media.subtitle,
            text: $scope.campaign.media.text,
            source: $scope.campaign.media.source,
            id: $scope.campaign.media.id
          };
          break;
        case 3:
          // Using id default, because social midia id is string
          campaign.media = {id:defaultSocialMidiaId};
          campaign.token = $auth.getToken();
          break;
      }

      CloudmidiaApi.CampaignContentApi.update(campaign,
        function success(response){
          Exception.catcherSuccess(response).then(
            function success(result){
              if(response.messages[0].parameters.id)
                subscription(response.messages[0].parameters.id);
              $scope.loadingSubmitNext = false;
              $rootScope.$broadcast('changedContent',{any:$stateParams.campaignId});
              $state.go('app.campaign.modify.places', {enterpriserSlug: $scope.enterpriser.slug});
            }
          )
        },
        function error(err){
          $scope.loadingSubmitNext = false;
          Exception.catcherError(err);
        });
    }

    var subscription = function (userId){
      CloudmidiaApi.CampaignSubscriptionApi.save({accountId:$scope.campaign.accountId, userId:userId},
        function success(data){
          Exception.catcherSuccess(data).then(
            function success(){}
          )
        },
        function error(err){
          Exception.catcherError(err);
        })
    };
  }
})();
