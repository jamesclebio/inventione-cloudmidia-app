;(function () {
  'use strict';

  angular
    .module('cloudmidiaApp')
    .controller('CampaignModifyPaymentController', CampaignModifyPaymentController);

  CampaignModifyPaymentController.$inject = [
    '$scope',
    'Exception',
    'CampaignService',
    'ProviderService',
    '$stateParams',
    'AppSettings',
    '$sce',
    '$location',
    '$rootScope',
    '$state',
    '$q'
  ];

  function CampaignModifyPaymentController($scope, Exception, CampaignService, ProviderService, $stateParams, AppSettings, $sce,
                                           $location, $rootScope, $state, $q) {

    $scope.paymentCampaign = true;

    $scope.bonus = false;

    $scope.paymentMethod = {paypal:{name:'paypal',value: true}, bonus:{name:'bonus', value:false}};

    $scope.completedBasic = false;

    $scope.completedLocal = false;

    $scope.completedContent = false;

    $scope.enterpriser = ProviderService.getCurrentEnterpriser();

    var limitCampaignBonus = $scope.enterpriser.bonus;

    $scope.publish = true;

    $scope.canBePaid = true;

    $scope.alreadyPaid = false;

    $scope.total = 0;

    $scope.loadCheckout = false;

    $scope.loadExecutePayment = false;

    $scope.totalBonus = 0;

    main.loaderPage.on();

    var init = function(){
      $q.all([CampaignService.getBasicCampaign($scope.enterpriser.id,$stateParams.campaignId),
        CampaignService.getContentCampaign($scope.enterpriser.id,$stateParams.campaignId),
        CampaignService.verifyCampaign($scope.enterpriser.id,$stateParams.campaignId),
        CampaignService.getLocalCampaign($scope.enterpriser.id,$stateParams.campaignId,data),
        CampaignService.getPaidCampaign($scope.enterpriser.id,$stateParams.campaignId),
        CampaignService.getQuantityOfValidCampaigns($scope.enterpriser.id),
        CampaignService.isOnlyMyLocalsInCampaign($scope.enterpriser.id,$stateParams.campaignId)
      ]).then(
        function success(data){
          verifyBasicCampaign(data[0]);
          verifyContentCampaign(data[1]);
          verifyCampaign(data[2]);
          verifyLocalCampaign(data[3]);
          verifyPaidCampaign(data[4]);
          if(data[5].quantity < limitCampaignBonus && data[6].allMyLocals)
            $scope.bonus = true;
          $scope.totalBonus = Math.abs(data[5].quantity - limitCampaignBonus);
          main.loaderPage.off();
          verifyRedirect();
        },function error(err){
          main.loaderPage.off();
        });
    }

    $scope.paymentChoice = function(typeOfPayment){
      if(typeOfPayment === $scope.paymentMethod.paypal.name){
        $scope.paymentMethod.paypal.value = true;
        $scope.paymentMethod.bonus.value = false;
      }else{
        $scope.paymentMethod.paypal.value = false;
        $scope.paymentMethod.bonus.value = true;
      }
    }

    var verifyBasicCampaign = function(data){
      if(data.advertiser){
          $scope.completedBasic = true;
      }
    };

    var verifyPaidCampaign = function (data){
      if(data.id != 0){
        $scope.publish = false;
        $scope.alreadyPaid = true;
      }
    };

    var verifyContentCampaign = function(data){
      if(data.duration){
        $scope.completedContent = true;
      }
    }

    // Verify if campaign can be paid
    function verifyCampaign(data){
      $scope.loadVerifyCampaign = true;
      $scope.canBePaid = data.completed;
      $scope.interrupted = data.interrupted;
      $scope.published = data.publish;
      $scope.loadVerifyCampaign = false;
    };

    $scope.interruptCampaign = function(){
      $scope.loadInterrupt = true;
      CampaignService.interruptCampaign($scope.enterpriser.id,$stateParams.campaignId).then(
        function success(response){
          $scope.loadInterrupt = false;
          Exception.catcherSuccess(response).then(
            function success(result){
              if(result.successes[0].type === 'SUCCESS'){
                $rootScope.$broadcast('campaignInterrupted',{any:$stateParams.campaignId});
                $state.go('app.campaign', {enterpriserSlug: $scope.enterpriser.slug});
              }
            }
          )
        },
        function error(err){
          Exception.catcherError(err);
        }
      )
    };

    var executePayment = function(data){
      main.loaderPage.on();
      CampaignService.executePayment($scope.enterpriser.id,$stateParams.campaignId,data).then(
        function success(response){
          Exception.catcherSuccess(response).then(
            function success(){
              $rootScope.$broadcast('campaignPaid',{any:$stateParams.campaignId});
              $scope.publish = false;
              $scope.alreadyPaid = true;
              main.loaderPage.off();
              $scope.publishCampaign();
          })
        },
        function error(err){
          Exception.catcherError(err);
          $scope.publish = false;
          main.loaderPage.off();
          $scope.alreadyPaid = false;
        }
      )
    };

    var data = $location.search();

    function verifyRedirect(){
      if(data.token && !$scope.alreadyPaid){
        $scope.loadExecutePayment = true;
        executePayment(data);
      }
    }

    var verifyLocalCampaign = function(data){
        if(data[0]){
          calcCostTotal(data);
          $scope.completedLocal = true;
          $scope.paymentCampaign = false;
          $scope.locals = data[0].name !== undefined ? data : '';
          $scope.localRangeDisable = false;
        }else{
          $scope.paymentCampaign = false;
        }
    };

    $scope.action = $sce.trustAsResourceUrl(AppSettings.urlApi + 'accounts/'+$scope.enterpriser.id+'/campaigns/'+$stateParams.campaignId+'/payment');

    var calcCostTotal = function(data){
      $scope.total=0;
      for(var i = 0; i < data.length; i++){
        $scope.total += data[i].price*data[i].boosting;
      }
    };

    $scope.publishCampaign = function(){
      $scope.loadPublish = true;
      CampaignService.publishCampaign($scope.enterpriser.id,$stateParams.campaignId).then(
        function success(data){
          $scope.loadPublish = false;
          Exception.catcherSuccess(data).then(
            function success(result){
              $rootScope.$broadcast('campaignPublished',{any:$stateParams.campaignId});
              $state.go('app.campaign', {enterpriserSlug: $scope.enterpriser.slug});
            }
          );
        },
        function error(err){
          $scope.loadPublish = false;
          Exception.catcherError(err);
        }
      )
    };

    init();
  }
})();
