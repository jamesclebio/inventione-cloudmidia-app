;(function () {
  'use strict';

  angular
    .module('cloudmidiaApp')
    .controller('CampaignModifyPlacesController', CampaignModifyPlacesController);

  CampaignModifyPlacesController.$inject = [
    '$scope',
    '$modal',
    '$state',
    '$timeout',
    '$stateParams',
    'CloudmidiaApi',
    'ProviderService',
    'Exception',
    'CampaignService',
    '$rootScope'
  ];

  function CampaignModifyPlacesController($scope, $modal, $state, $timeout, $stateParams, CloudmidiaApi, ProviderService,
                                          Exception,CampaignService,$rootScope) {

    // Sample code
    $('.card-float').addClass('on');

    $scope.enterpriser = ProviderService.getCurrentEnterpriser();
    $scope.localRangeDisable = false;
    $scope.selectedLocals = {selected: []};

    $scope.campaign = {
      accountId: $scope.enterpriser.id,
      campaignId: $stateParams.campaignId
    };

    main.impact.builder(0, 0, 0);

    var reload = function (percent,exhibition,impact) {
      $timeout(function () {
        $scope.$apply(function () {
          main.impact.builder(percent, exhibition, impact);
        });
      }, 0);
    };

    $scope.exhibitionCalc = function () {
      var days = parseInt($scope.campaign.endDate - $scope.campaign.startDate) / (1000 * 60 * 60 * 24),
        sum = 0;

      if ($scope.locals) {
        for (var i = 0 ; i < $scope.locals.length; i++) {
          // 8 represents the hours of locals
          sum += 2 * $scope.locals[i].boosting * days * 8;
        }
      }

      $scope.exhibition = sum;
    };

    var calcCost = function (boosting) {
      if ($scope.locals) {
        var sum = 0;

        for (var i = 0; i < $scope.locals.length; i++) {
          sum += $scope.locals[i].price/30;
        }

        return sum * boosting * parseInt($scope.campaign.endDate - $scope.campaign.startDate) / (1000 * 60 * 60 * 24);
      }
      return 0;
    };

    $scope.generalCalc = function () {
      var sumDevice = 0,
        sumBoosting = 0,
        sumViewers = 0;

      $scope.exhibitionCalc();

      if ($scope.locals) {
        for (var i = 0; i < $scope.locals.length; i++) {
          sumDevice += $scope.locals[i].quantityOfDevices ? $scope.locals[i].quantityOfDevices: $scope.locals[i].devices.length;
          sumBoosting += parseInt($scope.locals[i].boosting);
          sumViewers += $scope.locals[i].quantityOfViewers;
        }
      }

      $scope.viewers = sumViewers * parseInt($scope.campaign.endDate - $scope.campaign.startDate) / (1000 * 60 * 60 * 24);
      $scope.percentage = sumDevice > 0 ? Math.round((sumBoosting/sumDevice) * 100) : 0;
      $scope.cost = calcCost(sumBoosting);
      reload($scope.percentage,$scope.exhibition,$scope.viewers);
    }

    var getBasicCampaign = function(){
      CampaignService.getBasicCampaign($scope.enterpriser.id,$stateParams.campaignId).then(
        function success(data){
          $scope.campaign.endDate = new Date(data.endDate);
          $scope.campaign.startDate = new Date(data.startDate);
          $scope.campaign.status = data.status;
          main.loaderPage.off();
          $scope.generalCalc();
        },
        function error(err){

        }
      )
    };

    var getLocalCampaign = function(){
      main.loaderPage.on();
      CampaignService.getLocalCampaign($scope.enterpriser.id,$stateParams.campaignId).then(
        function success(data){
          if(data[0]){
            $scope.locals = data[0].name !== undefined ? data : '';
            $scope.localRangeDisable = false;
          }
          getBasicCampaign();
        },
        function error(err){

        }
      )
    };

    $scope.removeLocal = function (local) {
      var index = _.indexOf($scope.locals, local);

      $scope.locals.splice(index,1);

      if ($scope.locals.length === 0) {
        $scope.locals = undefined;
      }
      $scope.generalCalc();
    };

    getLocalCampaign();

    $scope.openLocalsModal = function () {
      var modalInstance = $modal.open({
        templateUrl: 'modules/locals/modal-local.html',
        controller: 'ModalLocalController',
        windowClass: 'fade slide-right',
        size: 'lg',
        backdrop: 'static',
        keyboard: false,
        resolve: {
          accountId: function () {
            return $scope.enterpriser.id;
          },
          isPublish: function(){
            return $scope.enterpriser.isPublisher;
          }
        }
      });

      modalInstance.result.then(function (selectedItems) {
        if (!angular.isUndefined(selectedItems)) {
          $scope.locals = selectedItems;
          $scope.generalCalc();

          $timeout(function () {
            $scope.$apply(function () {
              main.inputRange.update();
            });
          }, 0);
        }
      });
    };

    // Validate form
    function validateForm(){
      $scope.campaignForm.$setSubmitted();
      if(($scope.campaignForm.$invalid || $scope.locals === undefined)){
        main.scrollTop.run();
        return true;
      }
      return false;
    }

    $scope.SaveLocal = function(){
      $scope.loadingSubmitNext = true;
      if(validateForm()){
        $scope.loadingSubmitNext = false;
        return;
      }
      // Building the request campaign data
      var campaign = {
        accountId: $scope.campaign.accountId,
        localId: _.map($scope.locals, function (item) {return {id: item.id, boost: item.boosting}}),
        campaignId: $scope.campaign.campaignId,
        publish:false
      };

      CloudmidiaApi.CampaignLocalApi.update(campaign,
        function success(response){
          Exception.catcherSuccess(response).then(
            function success(result){
              $scope.loadingSubmitNext = false;
              $rootScope.$broadcast('changedLocal',{any:$stateParams.campaignId});
              $state.go('app.campaign.modify.payment', {enterpriserSlug: $scope.enterpriser.slug});
            }
          )
        },
        function error(err){
          Exception.catcherError(err);
          $scope.loadingSubmitNext = false;
        })
    }

  }
})();
