angular.module('cloudmidiaApp')
  .controller('CampaignController', ['$scope','$state',
    '$modal',
    '$log',
    '$q',
    'CampaignService',
    'authService',
    'ProviderService',
    '$rootScope',
    'Exception',
    function ($scope, $state, $modal, $log, $q, CampaignService, authService, ProviderService, $rootScope, Exception) {
      'use strict';

      //$scope.showTable = false;
      $scope.ord = true;
      $scope.showEmptyState = false;
      $scope.isLoadingAvailability = false;
      $scope.emptySearch = false;
      $scope.dropdown = [{id:0,name:'Nome'},
        {id:1, name:'Anunciantes'},{id:2, name:'Última edição'}, {id:3, name:'Início'},
        {id:4, name:'Fim'}];

      $scope.myCampaigns = {checked:true};

      $scope.change = function(){
        $scope.showEmptyState = false;
        init();
      }

      var columnsData=['name','advertiser','lastUpdate','start','end','state'];

      $scope.enterpriser = undefined;

      $scope.ordColumn = {selected:{id:0,name:'Nome'}};

      $scope.$watch('ordColumn.selected',function(){
        if($scope.totalItems)
          getCampaigns();
      });

      $scope.campaignType = {
        '0': {id: 0, name: 'Imagem', trans:'image'},
        '1': {id: 1, name: 'Vídeo', trans:'video'},
        '2': {id: 2, name: 'Texto', trans:'text'}
      };

      $scope.pageChanged = function (currentPage) {
        $scope.currentPage = currentPage;
        getCampaigns();
      };

      $scope.changeOrd = function(){
        if($scope.ord){
          $scope.ord = false;
        }else{
          $scope.ord = true;
        }
        getCampaigns();
      }

      function decreaseOrCrescent(){
        if($scope.ord){
          return 'p'+columnsData[$scope.ordColumn.selected.id];
        }else{
          return 'm'+columnsData[$scope.ordColumn.selected.id];
        }
      }

      var getCampaigns = function () {
        $scope.isLoadingAvailability = true;
        $scope.filterName = $scope.filterName !== '' ? $scope.filterName : null;
        var ord = decreaseOrCrescent();
        CampaignService.getCampaigns($scope.enterpriser.id, $scope.filterName, $scope.currentPage, $scope.pageSize, ord, $scope.myCampaigns.checked).then(
          function (result) {
            $scope.isLoadingAvailability = false;
            $scope.campaigns = result.items;
            $scope.totalItems = result.totalItems;
            $scope.pageSize = result.pageSize;
            $scope.showTable = result.totalItems > 0;
            $scope.showEmptyState = !$scope.showTable;
            $scope.emptySearch = false;
            if($scope.showEmptyState && ($scope.filterName !== undefined && $scope.filterName !== null)){
              $scope.showEmptyState = false;
              $scope.emptySearch = true;
            }
          },
          function error (err){
            Exception.catcherError(err,true);
            $scope.isLoadingAvailability = false;
          }
        );
      };

      $scope.impact = function(boost, quantityOfDevices){
        return quantityOfDevices > 0 ? Math.round((boost/quantityOfDevices)*100) : 0;
      };

      var getMyCampaigns = function(){
        $scope.isLoadingAvailability = true;
        $scope.filterName = $scope.filterName !== '' ? $scope.filterName : null;
        var ord = decreaseOrCrescent();
        CampaignService.getCampaigns($scope.enterpriser.id, $scope.filterName, $scope.currentPage, $scope.pageSize, ord, true).then(
          function (result) {
            if(result.totalItems > 0)
              $scope.privateCampaign = true;
          },
          function error (err){
            Exception.catcherError(err,true);
            $scope.isLoadingAvailability = false;
          }
        );
      }

      var getOthersCampaigns = function(){
        $scope.isLoadingAvailability = true;
        $scope.filterName = $scope.filterName !== '' ? $scope.filterName : null;
        var ord = decreaseOrCrescent();
        CampaignService.getCampaigns($scope.enterpriser.id, $scope.filterName, $scope.currentPage, $scope.pageSize, ord, false).then(
          function (result) {
            if(result.totalItems > 0)
              $scope.publicCampaign = true;
          },
          function error (err){
            Exception.catcherError(err,true);
            $scope.isLoadingAvailability = false;
          }
        );
      }

      var init = function(){
        $scope.privateCampaign = false;
        $scope.publicCampaign = false;
        $scope.showTable = false;
        $scope.enterpriser = ProviderService.getCurrentEnterpriser();
        if($scope.enterpriser!== null){
          getMyCampaigns();
          getOthersCampaigns();
          getCampaigns();
        }
      };

      $scope.search = function (keyCode, filterName) {
        if (angular.isUndefined(keyCode) || keyCode === 13) {
          $scope.filterName = filterName;
          getCampaigns();
        }
      };

      $scope.modifyState = function(campaign){
        var modalInstance = $modal.open({
          templateUrl: 'modules/campaign/modal-campaign-modify-status.html',
          controller: 'ModalCampaignsModifyStatusController',
          backdrop: 'static',
          keyboard: false,
          resolve: {
            accountId: function () {
              return $scope.enterpriser.id;
            },
            campaign: function () {
              return campaign;
            }
          }
        });
        modalInstance.result.then(function () {
          getCampaigns();
        }, function () {
          $log.info('Modal dismissed at: ' + new Date());
        });
      };

      $scope.remove = function (campaign) {
        var modalInstance = $modal.open({
          templateUrl: 'modules/campaign/modal-campaign-delete.html',
          controller: 'ModalCampaignsRemoveController',
          backdrop: 'static',
          keyboard: false,
          resolve: {
            accountId: function () {
              return $scope.enterpriser.id;
            },
            campaign: function () {
              return campaign;
            }
          }
        });
        modalInstance.result.then(function () {
          init();
        }, function () {
          $log.info('Modal dismissed at: ' + new Date());
        });
      };

      $scope.clone = function(campaign){
        var modalInstance = $modal.open({
          templateUrl: 'modules/campaign/modal-campaign-clone.html',
          controller: 'ModalCampaignsCloneController',
          backdrop: 'static',
          keyboard: false,
          resolve: {
            accountId: function () {
              return $scope.enterpriser.id;
            },
            campaign: function () {
              return campaign;
            }
          }
        });
        modalInstance.result.then(function () {
          init();
        }, function () {
          $log.info('Modal dismissed at: ' + new Date());
        });
      }

      $scope.createEmptyCampaign = function(){
        CampaignService.createEmptyCampaign($scope.enterpriser.id).then(
          function success(data){
            Exception.catcherSuccess(data).then(
              function success(result){
                $state.go('app.campaign.modify.basic',{enterpriserSlug: $scope.enterpriser.slug,campaignId:data.messages[0].parameters});
              }
            )
          },
          function error(err){
            Exception.catcherError(err);
          }
        )
      }

      init();
    }
  ]);
