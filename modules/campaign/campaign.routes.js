angular.module('cloudmidiaApp')
  .config(['$stateProvider',
    function ($stateProvider) {
      $stateProvider
        .state('app.campaign',{
          url:'/p/:enterpriserSlug',
          template:'<ui-view></ui-view>',
          data: {
            requireAuthentication: true,
            context:'campaign'
          }
        })

        .state('app.campaign.list', {
          url: '/campaigns',
          templateUrl: 'modules/campaign/campaign.html',
          controller: 'CampaignController',
          data: {
            requireAuthentication: true,
            action:'ListCampaign'
          },
          resolve: {
            deps: ['$ocLazyLoad', function ($ocLazyLoad) {
              return $ocLazyLoad.load([
                'select',
                'underscore'
              ], {
                insertBefore: '#lazyload_placeholder',

              })
                .then(function () {
                  return $ocLazyLoad.load([
                    'filters/propsFilter.js',
                    'modules/campaign/campaign.controller.js',
                    'services/cloudmidia-api.service.js',
                    'services/campaigns.service.js',
                    'services/devices.service.js',
                    'services/auth.service.js',
                    'services/groups.service.js',
                    'modules/campaign/campaign-create.controller.js',
                    'modules/campaign/modal-campaign-delete.controller.js',
                    'modules/campaign/modal-campaign-clone.controller.js',
                    'modules/campaign/modal-campaign-modify-status.controller.js',
                    'modules/campaign/campaign-update.controller.js',
                    'modules/upload/modal-file-upload.controller.js',
                    'services/advertisers.service.js',
                    'directives/dropdownOptions.js',
                    'directives/dropdown.js'
                  ]);
                });
            }]
          }
        })

        .state('app.campaign.create', {
          url: '/campaign/create/{type:int}',
          templateUrl: 'modules/campaign/campaign-modify.html',
          controller: 'CampaignModifyController',
          data: {
            requireAuthentication: true,
            action:'CreateCampaign'
          },
          resolve: {
            campaignAction: function () {
              return 'Adicionar';
            },
            deps: ['$ocLazyLoad', function ($ocLazyLoad) {
              return $ocLazyLoad.load([
                'select',
                'underscore',
                'rangeslider',
                'circleProgress',
                'animateNumber'
              ], {
                insertBefore: '#lazyload_placeholder'
              }).then(function () {
                  return $ocLazyLoad.load([
                    'filters/propsFilter.js',
                    'services/cloudmidia-api.service.js',
                    'services/campaigns.service.js',
                    'services/devices.service.js',
                    'services/auth.service.js',
                    'services/groups.service.js',
                    'modules/campaign/campaign-create.controller.js',
                    'modules/upload/modal-file-upload.controller.js',
                    'modules/locals/modal-locals.controller.js',
                    'services/advertisers.service.js',
                    'services/locals.service.js',
                    'services/instagram.service.js'
                  ]);
                });
            }]
          }
        })

        .state('app.campaign.updateCampaign', {
          url: '/campaign/:id',
          templateUrl: 'modules/campaign/campaign-update.html',
          controller: 'UpdateCampaignController',
          data: {
            requireAuthentication: true,
            action:'UpdateCampaign'
          },
          resolve: {
            campaignAction: function () {
              return 'Atualizar';
            },
            deps: ['$ocLazyLoad', function ($ocLazyLoad) {
              return $ocLazyLoad.load([
                'select',
                'underscore',
                'rangeslider',
                'circleProgress',
                'animateNumber'
              ], {
                insertBefore: '#lazyload_placeholder'
              }).then(function () {
                  return $ocLazyLoad.load([
                    'filters/propsFilter.js',
                    'services/cloudmidia-api.service.js',
                    'services/campaigns.service.js',
                    'services/devices.service.js',
                    'services/campaigns.service.js',
                    'services/auth.service.js',
                    'services/groups.service.js',
                    'modules/campaign/campaign-update.controller.js',
                    'modules/upload/modal-file-upload.controller.js',
                    'services/advertisers.service.js',
                    'services/locals.service.js',
                    'services/instagram.service.js'
                  ]);
                });
            }]
          }
        })

        // Modify
        .state('app.campaign.modify', {
          abstract: true,
          url: '/campaign/:campaignId',
          templateUrl: 'modules/campaign/campaign-modify.html',
          controller: 'CampaignModifyController',
          data: {
            requireAuthentication: true
          },
          resolve: {
            deps: ['$ocLazyLoad', function ($ocLazyLoad) {
              return $ocLazyLoad
                .load([
                  'select',
                  'underscore',
                  'rangeslider',
                  'circleProgress',
                  'animateNumber'
                ], {
                  insertBefore: '#lazyload_placeholder'
                })
                .then(function () {
                  return $ocLazyLoad.load([
                    'modules/campaign/campaign-modify.controller.js',
                    'services/instagram.service.js',
                    'services/campaigns.service.js'
                  ]);
                });
            }]
          }
        })

        // Modify Basic
        .state('app.campaign.modify.basic', {
          url: '/basic',
          templateUrl: 'modules/campaign/modify/basic/basic.html',
          controller: 'CampaignModifyBasicController',
          controllerAs: 'vm',
          resolve: {
            deps: ['$ocLazyLoad', function ($ocLazyLoad) {
              return $ocLazyLoad
                .load(['select',
                  'underscore'], {
                  insertBefore: '#lazyload_placeholder'
                })
                .then(function () {
                  return $ocLazyLoad.load([
                    'filters/propsFilter.js',
                    'modules/campaign/modify/basic/basic.controller.js',
                    'services/auth.service.js',
                    'services/cloudmidia-api.service.js',
                    'services/provider.services.js',
                    'services/account.service.js',
                    'services/advertisers.service.js',
                    'services/instagram.service.js',
                    'services/campaigns.service.js'
                  ]);
                });
            }]
          }
        })

        // Modify Content
        .state('app.campaign.modify.content', {
          url: '/content',
          templateUrl: 'modules/campaign/modify/content/content.html',
          controller: 'CampaignModifyContentController',
          controllerAs: 'vm',
          resolve: {
            deps: ['$ocLazyLoad', function ($ocLazyLoad) {
              return $ocLazyLoad
                .load([], {
                  insertBefore: '#lazyload_placeholder'
                })
                .then(function () {
                  return $ocLazyLoad.load([
                    'modules/campaign/modify/content/content.controller.js',
                    'services/auth.service.js',
                    'services/cloudmidia-api.service.js',
                    'services/provider.services.js',
                    'services/account.service.js',
                    'services/campaigns.service.js'
                  ]);
                });
            }]
          }
        })

        // Modify Places
        .state('app.campaign.modify.places', {
          url: '/places',
          templateUrl: 'modules/campaign/modify/places/places.html',
          controller: 'CampaignModifyPlacesController',
          controllerAs: 'vm',
          resolve: {
            deps: ['$ocLazyLoad', function ($ocLazyLoad) {
              return $ocLazyLoad
                .load([
                  'select',
                  'underscore',
                  'rangeslider',
                  'circleProgress',
                  'animateNumber'], {
                  insertBefore: '#lazyload_placeholder'
                })
                .then(function () {
                  return $ocLazyLoad.load([
                    'modules/campaign/modify/places/places.controller.js',
                    'services/auth.service.js',
                    'services/cloudmidia-api.service.js',
                    'services/provider.services.js',
                    'services/account.service.js',
                    'modules/locals/modal-locals.controller.js',
                    'services/locals.service.js',
                    'services/campaigns.service.js'
                  ]);
                });
            }]
          }
        })

        // Modify Payment
        .state('app.campaign.modify.payment', {
          url: '/payment?paymentId',
          templateUrl: 'modules/campaign/modify/payment/payment.html',
          controller: 'CampaignModifyPaymentController',
          controllerAs: 'vm',
          resolve: {
            deps: ['$ocLazyLoad', function ($ocLazyLoad) {
              return $ocLazyLoad
                .load([
                ], {
                  insertBefore: '#lazyload_placeholder'
                })
                .then(function () {
                  return $ocLazyLoad.load([
                    'modules/campaign/modify/payment/payment.controller.js',
                    'services/auth.service.js',
                    'services/cloudmidia-api.service.js',
                    'services/provider.services.js',
                    'services/account.service.js',
                    'services/campaigns.service.js'
                  ]);
                });
            }]
          }
        });
    }]);
