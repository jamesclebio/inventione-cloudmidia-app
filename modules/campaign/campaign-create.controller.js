;(function () {
  'use strict';

  angular
    .module('cloudmidiaApp')
    .controller('CreateCampaignController', [
      '$scope',
      '$state',
      'CloudmidiaApi',
      '$stateParams',
      'Upload',
      'AppSettings',
      'authService',
      'campaignAction',
      'DevicesService',
      '$q',
      '$timeout',
      '$modal',
      'AdvertisersService',
      'ProviderService',
      'InstagramService',
      'Exception',
      '$auth',
      '$window',
      function ($scope, $state, CloudmidiaApi, $stateParams, Upload, AppSettings, authService, campaignAction,
                DevicesService, $q, $timeout, $modal, AdvertisersService,ProviderService,InstagramService,
                Exception,$auth,$window) {

        var campaignTypes = {
              '1': {
                id: 1,
                name: 'Notícia'
              },
              '2': {
                id: 2,
                name: 'Comercial'
              }
            },
            typeOfContent = {
              'image': {id: 0},
              'video': {id: 1},
              'textAndImage': {id: 2},
              'social':{id:3}
            };

        $scope.submitLoadingSave = false;
        $scope.submitLoadingSavePub = false;
        main.impact.builder(0, 0, 0);
        $scope.content = false;
        $scope.locals = undefined;
        var defaultSocialMidiaId = 3;
        $scope.needAuthenticate = true;

        var reload = function (percent,exhibition,impact) {
          $timeout(function () {
            $scope.$apply(function () {
              main.impact.builder(percent, exhibition, impact);
            });
          }, 0);
        };


        var callback = function(data){
          InstagramService.authenticate($scope.enterpriser.id,data.code).then(
            function success(data){
              $auth.setToken(data.messages[0].parameters);
              verifySocialAuthentication();
            }
          )
        };

        $scope.exhibitionCalc = function () {
          var days = parseInt($scope.campaign.endDate - $scope.campaign.startDate) / (1000 * 60 * 60 * 24),
              sum = 0;

          if ($scope.locals) {
            for (var i = 0 ; i < $scope.locals.length; i++) {
              // 8 represents the hours of locals
              sum += 2 * $scope.locals[i].boosting * days * 8;
            }
          }

          $scope.exhibition = sum;
        };

        var calcCost = function (boosting) {
          if ($scope.locals) {
            var sum = 0;

            for (var i = 0; i < $scope.locals.length; i++) {
              sum += $scope.locals[i].price/30;
            }
            return sum * boosting * parseInt($scope.campaign.endDate - $scope.campaign.startDate) / (1000 * 60 * 60 * 24);
          }
          return 0;
        };

        $scope.generalCalc = function () {
          var sumDevice = 0,
              sumBoosting = 0,
              sumViewers = 0;

          $scope.exhibitionCalc();

          if ($scope.locals) {
            for (var i = 0; i < $scope.locals.length; i++) {
              sumDevice += $scope.locals[i].quantityOfDevices;
              sumBoosting += parseInt($scope.locals[i].boosting);
              sumViewers += $scope.locals[i].quantityOfViewers;
            }
          }

          $scope.viewers = sumViewers * parseInt($scope.campaign.endDate - $scope.campaign.startDate) / (1000 * 60 * 60 * 24);
          $scope.percentage = sumDevice > 0 ? Math.round((sumBoosting/sumDevice) * 100) : 0;
          $scope.cost = calcCost(sumBoosting);
          reload($scope.percentage,$scope.exhibition,$scope.viewers);
        }

        $scope.enterpriser = ProviderService.getCurrentEnterpriser();

        $scope.checked = function (value) {
          if($scope.campaign.content !== value)
            $scope.removeContent();

          $scope.campaign.content = value;
          $scope.content = false;
          if($auth.getToken() && value === 3){
            verifySocialAuthentication();
          }
        };

        $scope.switchSocialAuthentication = function(){
          var wnd = $window.open("http://www.instagram.com/accounts/logout",'Logout','width=400,height=400');
          setTimeout(function() {
            $auth.removeToken();
            wnd.close();
          }, 1000);
          $scope.removeContent();
          $scope.needAuthenticate = true;
          return false;
        };

        var verifySocialAuthentication = function(){
          InstagramService.verifyAuthentication($scope.enterpriser.id, $auth.getToken()).then(
            function success(data){
              if(data.messages && data.messages[0].type === "ERROR"){
                Exception.catcherSuccess(data).then(
                  function (){
                    $scope.needAuthenticate = true;
                  })
              }else{
                $scope.needAuthenticate = false;
                $scope.socialUserName = data.data.userName;
                getSocialMedia($auth.getToken());
              }
            }
          )
        };

        var getSocialMedia = function (accessToken){
          CloudmidiaApi.SocialMediaApi.get({accountId:$scope.campaign.accountId, accessToken:accessToken},
            function success(data){
              if(data.id === undefined){
                Exception.catcherSuccess(data).then(
                  function(){
                  }
                );
              }else{
                $scope.content = true;
                $scope.campaign.media.id = data.id;
                $scope.campaign.media.name = data.name;
                $scope.campaign.media.thumbUrl = data.thumbUrl;
                $scope.campaign.media.url = data.url;
              }
            },
            function error(err){
              Exception.catcherError(err);
            })
        };

        $scope.campaignAction = campaignAction;
        $scope.campaignType = campaignTypes[$stateParams.type];

        $scope.selectedAdvertiser = {selected: []};
        $scope.selectedLocals = {selected: []};

        $scope.availableItems = [];

        $scope.removeLocal = function (local) {
          var index = _.indexOf($scope.locals, local);

          $scope.locals.splice(index,1);

          if ($scope.locals.length === 0) {
            $scope.locals = undefined;
          }
          $scope.generalCalc();
        };

        var advertisers = function () {
          AdvertisersService.getAllAdvertisers($scope.enterpriser.id).then(
            function success(data) {
              $scope.Advertisers = data;
            },
            function error(err) {
              Exception.catcherError(err);
            }
          )
        };

        var init = function () {
          advertisers();
          $scope.selectAdvertiser = false;
          $scope.fixedDate = [
            {
              id: 1,
              value: 7,
              checked: false
            }, {
              id: 2,
              value: 15,
              checked: false
            }, {
              id: 3,
              value: 30,
              checked: false
            }
          ];

          $scope.campaign = {
            accountId: $scope.enterpriser.id,
            content: typeOfContent['image'].id,
            name: '',
            startDate: today,
            endDate: tomorrow,
            duration: 3,
            media: {
              title: '',
              subtitle: '',
              text: '',
              source: '',
              id: null
            }
          };
          $scope.select($scope.fixedDate[2]);
        };

        $scope.$watch('campaign.endDate', function () {
          $scope.generalCalc();
        });

        $scope.$watch('campaign.endDate', function () {
          $scope.generalCalc();
        });

        $scope.$watch('selectedAdvertiser.selected', function(){
          if($scope.campaignForm.$submitted)
            $scope.selectAdvertiser = $scope.selectedAdvertiser.selected.length <= 0 ? true : false;

        });

        $scope.generalInfoClass = $scope.campaignType.id === 1 ? 'col-lg-7 col-md-6' : 'col-md-12';

        $scope.select = function (date) {
          var today = new Date();
          $scope.campaign.endDate = new Date(today.setDate(today.getDate() + date.value));

          for (var i=0; i < $scope.fixedDate.length; i++) {
            if ($scope.fixedDate[i].id === date.id) {
              $scope.fixedDate[i].checked = true;
            } else {
              $scope.fixedDate[i].checked = false;
            }
          }
          $scope.generalCalc();
        };

        // Initialize campaign properties
        var today = new Date(),
          tomorrow = new Date();
        tomorrow.setDate(tomorrow.getDate() + 1);

        // DatePickers' configurations
        $scope.dtConfig = {
          minDate: $scope.minDate ? null : new Date(),
          maxDate: new Date(2020, 5, 22),
          selectBegin: function () {
            return false;
          },
          selectEnd: function () {
            return false;
          }
        };

        $scope.status = {
          startOpened: false,
          endOpened: false
        };

        $scope.openStartDatepicker = function ($event) {
          $event.preventDefault();
          $event.stopPropagation();

          $scope.status.startOpened = !$scope.status.startOpened;
        };

        $scope.openEndDatepicker = function ($event) {
          $event.preventDefault();
          $event.stopPropagation();

          $scope.status.endOpened = !$scope.status.endOpened;
        };

        $scope.selectedItems = {};

       var mediaFilter= {
          0: ['image'],
          1: ['video'],
          2: ['image'],
          3: ['social']
        };

        $scope.openFileLibraryModal = function () {
          var modalInstance = $modal.open({
            templateUrl: 'modules/upload/modal-file-upload.html',
            controller: 'ModalFileUploadController',
            windowClass: 'fade stick-up',
            size: 'lg',
            backdrop: 'static',
            keyboard: false,
            resolve: {
              accountId: function () {
                return $scope.enterpriser.id;
              },
              mediaFilter: function () {
                return mediaFilter[$scope.campaign.content];
              }
            }
          });

          modalInstance.result.then(function (selectedItem) {
            if (!angular.isUndefined(selectedItem)) {
              $scope.content = true;
              $scope.campaign.media.id = selectedItem.id;
              $scope.campaign.media.name = selectedItem.name;
              $scope.campaign.media.thumbUrl = selectedItem.thumbUrl;
              $scope.campaign.media.url = selectedItem.url;
            }
          });
        };


        $scope.openLocalsModal = function () {
          var modalInstance = $modal.open({
            templateUrl: 'modules/locals/modal-local.html',
            controller: 'ModalLocalController',
            windowClass: 'fade slide-right',
            size: 'lg',
            backdrop: 'static',
            keyboard: false,
            resolve: {
              accountId: function () {
                return $scope.enterpriser.id;
              }
            }
          });

          modalInstance.result.then(function (selectedItems) {
            if (!angular.isUndefined(selectedItems)) {
              $scope.locals = selectedItems;
              $scope.generalCalc();

              $timeout(function () {
                $scope.$apply(function () {
                  main.inputRange.update();
                });
              }, 0);
            }
          });
        };

        $scope.removeContent = function () {
          $scope.content = false;
          $scope.campaign.media.id = undefined;
          $scope.campaign.media.name = undefined;
          $scope.campaign.media.thumbUrl = undefined;
          $scope.campaign.media.url = undefined;
        }

        function validateForm(){
          $scope.campaignForm.$setSubmitted();
          if(($scope.campaignForm.$invalid || $scope.selectedAdvertiser.selected.length <= 0 || $scope.campaign.media.url === undefined
            || $scope.locals === undefined) ){
            $scope.selectAdvertiser = $scope.selectedAdvertiser.selected.length <= 0 ? true : false;
            main.scrollTop.run();
            return true;
          }
          return false;
        }

        $scope.save = function(portlet){
          if(validateForm()){
            return;
          }
          $scope.submitLoadingSave = true;
          $scope.submitLoadingSavePub = false;
          send(portlet, false);
        };

        $scope.saveAndPublish = function(portlet){
          if(validateForm()){
            return;
          }
          $scope.submitLoadingSavePub = true;
          $scope.submitLoadingSave = false;
          send(portlet,true);
        };

        var send = function (portlet,publish) {
          // Building the request campaign data
          var campaign = {
            accountId: $scope.campaign.accountId,
            type: $scope.campaign.content,
            publish:publish,
            interrupted:false,
            name: $scope.campaign.name,
            advertiserId: $scope.selectedAdvertiser.selected.advertiserId,
            localId: _.map($scope.locals, function (item) {return {id: item.id, boost: item.boosting}}),
            startDate: $scope.campaign.startDate,
            endDate: $scope.campaign.endDate,
            duration: $scope.campaign.duration,
          };

          switch ($scope.campaign.content) {
            case 0: // Adding an IMAGE
              campaign.media = {id: $scope.campaign.media.id};
              break;

            case 1:// Adding a VIDEO
              campaign.media = {id: $scope.campaign.media.id};
              break;

            case 2:// Adding a TEXT and IMAGE
              campaign.media = { // If we are adding a NEWS
                title: $scope.campaign.media.title,
                subtitle: $scope.campaign.media.subtitle,
                text: $scope.campaign.media.text,
                source: $scope.campaign.media.source,
                id: $scope.campaign.media.id
              };
              break;
            case 3:
              // Using id default, because social midia id is string
              campaign.media = {id:defaultSocialMidiaId};
              campaign.token = $auth.getToken();
              break;
          }
          // Dispatch request
          CloudmidiaApi.CampaignApi.save(campaign,
            function success(response) {
              $timeout(function () {
                $(portlet).portlet({
                  refresh: false
                });
              }, 500);
              Exception.catcherSuccess(response).then(
                function success(result){
                  if(response.messages[0].parameters.id)
                    subscription(response.messages[0].parameters.id);
                  $state.go('app.campaign', {enterpriserSlug: $scope.enterpriser.slug});
                  $scope.submitLoadingSave = false;
                  $scope.submitLoadingSavePub = false;
                }
              )
            },
            function error(err) {
              Exception.catcherError(err);
              $scope.submitLoadingSave = false;
              $scope.submitLoadingSavePub = false;
              $timeout(function () {
                $(portlet).portlet({
                  refresh: false
                });
              }, 500);
            });
        };

        var subscription = function (userId){
          CloudmidiaApi.CampaignSubscriptionApi.save({accountId:$scope.campaign.accountId, userId:userId},
            function success(data){
              Exception.catcherSuccess(data).then(
                function success(){}
              )
            },
            function error(err){
              Exception.catcherError(err);
            })
        };

        $scope.cancel = function () {
          $state.go('app.campaign', {enterpriserSlug: $scope.enterpriser.slug});
        };

        init();
      }
    ]);
})();
