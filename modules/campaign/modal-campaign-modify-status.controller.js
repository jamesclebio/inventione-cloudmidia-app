angular.module('cloudmidiaApp')
  .controller('ModalCampaignsModifyStatusController',
  ['$scope',
    '$q',
    '$modal',
    '$log',
    '$modalInstance',
    'CloudmidiaApi',
    'CampaignService',
    'accountId',
    'campaign',
    'Exception',
    function ($scope, $q, $modal, $log, $modalInstance,CloudmidiaApi,CampaignService,accountId, campaign, Exception) {

      $scope.campaign = {
        accountId: accountId,
        campaignId: campaign.id,
        interrupted: campaign.interrupted,
        name: campaign.name,
        status: campaign.status,
        expired: campaign.expired
      };

      $scope.ok = function () {
        $scope.submitLoading = true;
        if(!campaign.interrupted && campaign.status){
          CampaignService.interruptCampaign(accountId,campaign.id).then(
            function success(response){
              $scope.submitLoading = false;
              Exception.catcherSuccess(response).then(
                function success(result){
                  if(result.successes[0].type === 'SUCCESS'){
                    $modalInstance.close();
                  }
                }
              )
            },
            function error(err){
              $scope.submitLoading = false;
              Exception.catcherError(err);
            }
          )
        }else{
          CampaignService.publishCampaign(accountId,campaign.id).then(
            function success(response){
              $scope.submitLoading = false;
              Exception.catcherSuccess(response).then(
                function success(result){
                  if(result.successes[0].type === 'SUCCESS'){
                    $modalInstance.close();
                  }
                }
              )
            },
            function error(err){
              $scope.submitLoading = false;
              Exception.catcherError(err);
            }
          )
        }
      };

      $scope.cancel = function () {
        $modalInstance.dismiss();
      };
    }]);
