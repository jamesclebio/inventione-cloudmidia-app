;(function () {
  'use strict';

  angular
    .module('cloudmidiaApp')
    .controller('CampaignModifyController', CampaignModifyController);

  CampaignModifyController.$inject = [
    '$scope',
    '$stateParams',
    'CampaignService',
    'ProviderService',
    'Exception',
    '$rootScope'
  ];

  function CampaignModifyController($scope,$stateParams, CampaignService, ProviderService, Exception,$rootScope) {

    // Sample code
    main.impact.builder(76, 21300, 3870);
    var enterpriser = ProviderService.getCurrentEnterpriser();
    $scope.basic = false;
    $scope.content = false;
    $scope.local = false;

    function getCurrentEnterprise(){
      return ProviderService.getCurrentEnterpriser();
    };

    var getBasicCampaign = function(campaignId){
      enterpriser = getCurrentEnterprise();
      CampaignService.getBasicCampaign(enterpriser.id,campaignId).then(
        function success(data){
          if(data.advertiser){
            $scope.basic = true;
          }
        },
        function error(err){
          console.log(err);
        }
      )
    };

    var getContentCampaign = function(campaignId){
      enterpriser = getCurrentEnterprise();
      CampaignService.getContentCampaign(enterpriser.id,campaignId).then(
        function success(data){
          if(data.duration){
            $scope.content = true;
          }
        },
        function error(err){
          console.log(err);
        }
      )
    };

    var getLocalCampaign = function(campaignId){
      enterpriser = getCurrentEnterprise();
      CampaignService.getLocalCampaign(enterpriser.id,campaignId).then(
        function success(data){
          if(data[0]){
            $scope.locals = true;
          }
        },
        function error(err){
          console.log(err);
        }
      )
    };

    var getPaidCampaign = function(campaignId){
      enterpriser = getCurrentEnterprise();
      CampaignService.getPaidCampaign(enterpriser.id,campaignId).then(
        function success(data){
          if(data.id != 0){
            $scope.payment = true;
          }
        },
        function error(err){
          console.log(err);
        }
      )
    };

    function verifyCampaign(campaignId){
      enterpriser = getCurrentEnterprise();
      $scope.loadVerifyCampaign = true;
      CampaignService.verifyCampaign(enterpriser.id,campaignId).then(
        function success(response){
          $scope.published = response.publish;
          $scope.interrupted = response.interrupted;
        }
      )
    };

    $rootScope.$on('changedBasic', function (event,args) {
      getBasicCampaign(args.any);
    });

    $rootScope.$on('changedContent', function (event,args) {
      getContentCampaign(args.any);
    });

    $rootScope.$on('changedLocal', function (event, args) {
      getLocalCampaign(args.any);
    });

    $rootScope.$on('campaignPaid', function(event, args){
      getPaidCampaign(args.any);
    });

    $rootScope.$on('campaignPublished', function(event, args){
      verifyCampaign(args.any);
    });

    $rootScope.$on('campaignInterrupted', function(event, args){
      verifyCampaign(args.any);
    })

    getBasicCampaign($stateParams.campaignId);
    getContentCampaign($stateParams.campaignId);
    getLocalCampaign($stateParams.campaignId);
    getPaidCampaign($stateParams.campaignId);
    verifyCampaign($stateParams.campaignId);
  }
})();
