angular.module('cloudmidiaApp')
  .controller('UserController', ['CloudmidiaApi',
    '$scope',
    '$modal',
    '$log',
    '$q',
    'authService',
    'InvitationService',
    'ProviderService',
    '$rootScope',
    'exception',
    function (CloudmidiaApi, $scope, $modal, $log, $q,authService,InvitationService,ProviderService,
    $rootScope,exception) {
      'use strict';

      var accountId = authService.getSavedProfileInfo().accountId;

      $scope.showTable = false;
      $scope.showEmptyState = false;
      var enterpriser = undefined;

      var init = function () {
        enterpriser = ProviderService.getCurrentEnterpriser();
        getRoles(enterpriser.id).then(function(data){
          $scope.allRoles = data;
        }).catch(function(message){
            exception.catcher('Erro no servidor')(message,true);
          });

        getUsers(enterpriser.id);
        getInvitations(enterpriser.id);
      };

      var getInvitations = function(accountId){
        CloudmidiaApi.InvitationApi.query({accountId:accountId},
        function success(data){
          $scope.invitations = data;
          $scope.showTableInvitations = true;
        },
          function error(error){
          }
        );
      };

      $scope.pageChanged = function (currentPage) {
        $scope.currentPage = currentPage;
        getUsers();
      };

      $scope.isSelected = function(userRoles,roleId){
        return _.where(userRoles,{id:roleId}).length>0;
      };

      var getUsers = function (accountId) {
        $scope.isLoadingAvailability = true;
        $scope.filterName = $scope.filterName !== '' ? $scope.filterName : null;
        CloudmidiaApi.UsersApi.get({'accountId': accountId,
            's':$scope.filterName,'page':$scope.currentPage,'pageSize':$scope.pageSize},
          function success(data) {
            $scope.isLoadingAvailability = false;
            $scope.quantity = data.items.length;
            $scope.pageSize = data.pageSize;
            $scope.showTable = data.totalItems > 0;
            $scope.showEmptyState = !$scope.showTable;
            $scope.data = data.items;
          },
          function error(err) {
            exception.catcher('')(err,true);
          }
        );
      };

      var getRoles = function (accountId) {
        var deferred = $q.defer();
        CloudmidiaApi.RolesApi.query({accountId: accountId},
          function success(data) {
            deferred.resolve(data);
          }, function failure(error) {
            exception.catcher('')(error,true);
            deferred.reject(error);
          }
        );
        return deferred.promise;
      };

      $scope.remove = function (user, size) {
        var modalInstance = $modal.open({
          animation: $scope.animationsEnabled,
          templateUrl: 'modules/users/modal-users-delete.html',
          controller: 'ModalUserDeleteController',
          windowClass: 'fade stick-up',
          backdrop: 'static',
          keyboard: false,
          size: size,
          resolve: {
            accountId: function () {
              return enterpriser.id;
            },
            user: function () {
              return user;
            }
          }
        });
        modalInstance.result.then(function () {
            getUsers(enterpriser.id);
          },
          function () {
          });
      };

      $scope.changeRoles = function (userData,role) {
        var user = {};
        user.accountId = enterpriser.id;
        user.userId = userData.id;
        user.roleId = role.id;
        var matchRole = _.find(userData.roles, function(roles){
          return roles.id === role.id;
        });
        if($scope.isSelected(userData.roles,role.id)){
          CloudmidiaApi.UserApi.delete(user,
            function success() {
              getUsers(enterpriser.id);
            },
            function error(err) {
              exception.catcher('')(err,false);
            });
        }else{
          CloudmidiaApi.UserApi.save(user,
            function success() {
              getUsers(enterpriser.id);
            },
            function error(err) {
              exception.catcher('')(err,false);

            });
        }
      };

      $scope.createUser = function (size) {
        $q.all([getRoles(accountId)]).then(function (data) {
          var modalInstance = $modal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'modules/users/modal-users-create.html',
            controller: 'ModalUserCreateController',
            windowClass: 'fade stick-up',
            backdrop: 'static',
            keyboard: false,
            size: size,
            resolve: {
              roles: function () {
                return data[0];
              },
              accountId: function () {
                return enterpriser.id;
              }
            }
          });
          modalInstance.result.then(function () {
              getInvitations(enterpriser.id);
            },
            function () {
            });
        }).catch(function(message){
          exception.catcher('Erro no servidor')(message,false);
        });
      };

      $scope.resendInvitation = function(invitationId){
        InvitationService.resendInvitationById(invitationId).then(
          function success(){
            getInvitations(enterpriser.id);
          }
        ).catch(function(message){
            exception.catcher('Erro no servidor')(message,false);
          });
      };

      $scope.cancelInvitation = function(invitationId){
        InvitationService.cancelInvitationById(invitationId).then(
          function success(){
            getInvitations(enterpriser.id);
          }).catch(function(message){
            exception.catcher('Erro no servidor')(message,false);
          });
      };

      $rootScope.$on('changedEnterpriser', function () {
        enterpriser = ProviderService.getCurrentEnterpriser();
        init();
      });

      init();
    }
  ]);
