angular.module('cloudmidiaApp')
  .config(['$stateProvider', function ($stateProvider) {
    $stateProvider
      .state('app.user', {
        url: '/p/:enterpriserSlug/user',
        templateUrl: 'modules/users/users.html',
        controller: 'UserController',
        data: {
          requireAuthentication: true
        },
        resolve: {
          deps: ['$ocLazyLoad', function ($ocLazyLoad) {
            return $ocLazyLoad.load([
              'select',
              'underscore'
            ], {
              insertBefore: '#lazyload_placeholder'
            })
              .then(function () {
                return $ocLazyLoad.load([
                  'filters/propsFilter.js',
                  'modules/users/users.controller.js',
                  'services/cloudmidia-api.service.js',
                  'services/invitation.service.js',
                  'directives/require-multiple-select.js',
                  'modules/users/modal-users-create.controller.js',
                  'modules/users/modal-users-delete.controller.js'
                ]);
              });
          }]
        }
      })
      .state('/reset-password', {
        url: '/p/:enterpriserSlug/reset-password?user&token',
        templateUrl: 'modules/users/reset-password.html',
        controller: 'ResetUserPasswordController',
        data: {
          requireAuthentication: false
        },
        resolve: {
          deps: ['$ocLazyLoad', function ($ocLazyLoad) {
            return $ocLazyLoad.load([
              'underscore'
            ], {
              insertBefore: '#lazyload_placeholder'
            })
              .then(function () {
                return $ocLazyLoad.load([
                  'services/cloudmidia-api.service.js',
                  'modules/users/reset-passwowrd.controller.js'
                ]);
              });
          }]
        }
      });
  }]);
