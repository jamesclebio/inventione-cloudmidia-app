angular.module('cloudmidiaApp')
  .controller('ModalUserDeleteController',['$scope','accountId','user','$modalInstance','CloudmidiaApi','exception',
    function ($scope,accountId,user,$modalInstance,CloudmidiaApi,exception){
      'use strict';

      $scope.user = user;

      $scope.confirm = function(){
        $scope.submitLoading = true;
        var user = {'accountId':accountId,
                    'userId':$scope.user.id
                    };
        CloudmidiaApi.UsersApi.save(user,
          function success(){
            $scope.submitLoading = false;
            $modalInstance.close();
        },
        function error(err){
          exception.catcher('')(err,false);
          $scope.submitLoading = false;
        });
      };

      $scope.cancel = function(){
        $modalInstance.dismiss();
      };
    }]);
