angular.module('cloudmidiaApp')
  .controller('ModalUserCreateController',['$scope','CloudmidiaApi','$modalInstance','accountId','roles','exception',
    function($scope,CloudmidiaApi,$modalInstance,accountId,roles,exception){
        'use strict';
      $scope.roles = roles;
      $scope.selectRoles = {selected:[]};

      $scope.confirm = function(){
        $scope.submitLoading = true;
        var user = {
          'accountId':accountId,
          'email':$scope.email,
          'message':$scope.message,
          'roles': _.pluck($scope.selectRoles.selected, 'id')
        };
        CloudmidiaApi.InvitationApi.save(user,function success(){
            $scope.submitLoading = false;
            $modalInstance.close();
        },
        function error(err){
          exception.catcher('')(err,false);
          $scope.submitLoading = false;
        });

      };

      $scope.cancel = function(){
        $modalInstance.dismiss();
      };

    }]);
