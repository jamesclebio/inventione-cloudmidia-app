angular.module('cloudmidiaApp')
  .controller('ResetUserPasswordController', ['$scope', '$location', '$stateParams', 'CloudmidiaApi',
    function($scope, $location, $stateParams, CloudmidiaApi){
      'use strict';

      $scope.user = {
        userId: $stateParams.user,
        token: $stateParams.token
      };

      var redirect = function(url){
        $location.path(url);
        $location.url($location.path());
      };

      $scope.register = function(){
        //$scope.resetPasswordForm.$setSubmitted();
        //if($scope.resetPasswordForm.$invalid){
        //  return true;
        //}
        CloudmidiaApi.ResetPasswordApi.save($scope.user,
          function success(data) {
            if (data.messages[0].type === 'SUCCESS') {
              redirect('/login');
            }
          },
          function error(){
          });
      };

  }]);
