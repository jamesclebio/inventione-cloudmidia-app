angular.module('cloudmidiaApp')
  .controller('ModalFileUploadController', [
    'AppSettings', '$q', '$scope', '$modalInstance', 'accountId', 'mediaFilter', 'CloudmidiaApi', 'Upload',
    function (AppSettings, $q, $scope, $modalInstance, accountId, mediaFilter, CloudmidiaApi, Upload) {

      $scope.uploadPattern = 'image/*,video/*';

      $scope.mediaTypes = [
        {description: 'Imagem', value: 'image'},
        {description: 'Vídeo', value: 'video'}
      ];

      $scope.mediaTypeFiltered = angular.copy(mediaFilter);

      $scope.disabledCheck = function (type) {
        return !_.contains(mediaFilter, type) || mediaFilter.length === 1;
      };

      function getMedias() {
        $scope.searchTerm = $scope.searchTerm !== '' ? $scope.searchTerm : null;
        CloudmidiaApi.MediaApi.get({
            'accountId': accountId,
            's': $scope.searchTerm,
            'page': $scope.currentPage,
            'pageSize': $scope.pageSize,
            'contentTypes': $scope.mediaTypeFiltered
          },
          function success(result) {
            $scope.medias = result.items;
            $scope.totalItems = result.totalItems;
            $scope.pageSize = result.pageSize;
            $scope.showContent = result.totalItems > 0;
            $scope.showEmptyState = !$scope.showContent;
          });
      }

      getMedias();

      $scope.search = function (keyCode) {
        if (angular.isUndefined(keyCode) || keyCode === 13) {
          getMedias();
        }
      };

      $scope.pageChanged = function (currentPage) {
        $scope.currentPage = currentPage;
        getMedias();
      };

      // upload on file select or drop
      function doneUploadAll() {
        $scope.isLoading = false;
        getMedias();
      }

      function calcThumbSize(videoWidth,videoHeight) {
        var maxWidth = 140;
        var maxHeight = 105;
        var ratio = 0;
        var width = videoWidth;
        var height = videoHeight;
        if(videoWidth <= maxWidth && videoHeight <= maxHeight){
          return {'width': width, 'height': height};
        }else{
          if (width > height && width > maxWidth) {
            ratio = maxWidth / width;
            height = height * ratio;
            width = width * ratio;
          } else {
            ratio = maxHeight / height;
            width = width * ratio;
            height = height * ratio;
          }
          return {'width': width, 'height': height};
        }
      }

      var resize = function (file) {
        var deferred = $q.defer();
        var URL = window.URL || window.webkitURL;
        var imgUrl = URL.createObjectURL(file);
        var canvas = document.getElementById('canvas');

        var ctx = canvas.getContext('2d');
        var resizedFileUrl;

        var splitTypeFile = file.type.split('/');
        if (splitTypeFile[0] === 'video') {

          //var video = document.getElementById('video');
          var video = document.createElement('video');
          video.setAttribute('id',imgUrl);
          video.src = imgUrl + '#t=3';
          video.addEventListener('loadeddata', function () {
            var newSize = calcThumbSize(video.videoWidth,video.videoHeight);
            canvas.width = newSize.width;
            canvas.height = newSize.height;
            ctx.drawImage(video, 0, 0,newSize.width,newSize.height);
            resizedFileUrl = canvas.toDataURL('image/jpeg');
            console.log(resizedFileUrl);
            ctx.clearRect(0,0,newSize.width,newSize.height);
            deferred.resolve(resizedFileUrl);
          });

        } else {
          var img = new Image();
          img.src = imgUrl;
          img.setAttribute('id',imgUrl);
          img.onload = function () {
            var newSize = calcThumbSize(img.width,img.height);
            canvas.width = newSize.width;
            canvas.height = newSize.height;
            ctx.drawImage(img, 0, 0, newSize.width, newSize.height);
            resizedFileUrl = canvas.toDataURL('image/jpeg');
            console.log(resizedFileUrl);
            ctx.clearRect(0,0,newSize.width,newSize.height);
            deferred.resolve(resizedFileUrl);
          };
        }
        return deferred.promise;
      };

      $scope.upload = function (files) {
        $scope.files = files;
        $scope.isLoading = true;

        var finishUploadCallback = _.after(files.length, doneUploadAll);

        _.each(files, function (file) {
            resize(file).then(function success(thumb) {
              uploadMedia(file, thumb, finishUploadCallback);
            });
        });
      };

      var uploadMedia = function (file, thumb, finishUploadCallback) {

        file.upload = Upload.upload({
          url: AppSettings.urlApi + 'accounts/' + accountId + '/medias',
          data: {file: [file, Upload.dataUrltoBlob(thumb)]}
        });
        file.upload
          .success(function (resp) {
            $scope.selectedMediaId = resp.id;
            file.isSuccess = true;
            finishUploadCallback();
          })
          .error(function (resp) {
            file.isError = true;
            if (resp.status > 0) {
              $scope.errorMsg = true;
            }
          })
          .progress(function (evt) {
            file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
          });
      };

      $scope.isSelected = function (mediaId) {
        return $scope.selectedMediaId === mediaId;
      };

      $scope.selectMedia = function () {
        $modalInstance.close(_.find($scope.medias, function (item) {
          return item.id === $scope.selectedMediaId;
        }));
      };

      $scope.cancel = function () {
        $modalInstance.dismiss();
      };

      $scope.preview = function () {
        var dlg = new DialogFx($('#previewModal').get(0));
        dlg.toggle();
      }
    }]);
