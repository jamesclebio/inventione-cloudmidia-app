angular.module('cloudmidiaApp')
  .controller('ModalAdvertiserController',
  ['$scope',
    'accountId',
    'advertiser',
    '$modalInstance',
    'AdvertisersService',
    'Exception',
    function ($scope, accountId, advertiser, $modalInstance, AdvertisersService, Exception) {
      'use strict';

      $scope.advertiserName = advertiser.advertiserName;
      $scope.ok = function () {
        $scope.submitLoading = true;
        AdvertisersService.excludeAdvertiser(accountId, advertiser.advertiserId).then(
          function success(response) {
            $scope.submitLoading = false;
            Exception.catcherSuccess(response).then(
              function success(result){
                if (response.messages[0].type === 'SUCCESS') {
                  $modalInstance.close();
                }
              }
            )
          },
          function error(err){
            $scope.submitLoading = false;
            Exception.catcherError(err);
          }
        );
      };

      $scope.cancel = function () {
        $modalInstance.close('cancel');
      };
    }]);
