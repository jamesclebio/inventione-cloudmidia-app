angular
  .module('cloudmidiaApp')
  .controller('AdvertisersModifyController', [
    '$scope',
    '$state',
    'authService',
    'AdvertisersService',
    'ProviderService',
    'Exception',
    function ($scope,$state, authService, AdvertisersService, ProviderService, Exception) {
      'use strict';

      $scope.enterpriser = ProviderService.getCurrentEnterpriser();

      $scope.typesOfBusiness = [{id:1,name:'Arte e Entretenimento'},
        {id:2, name:'Automotivo'},
        {id:3,name:'Beleza e Fitness'},
        {id:4,name:'Livros e Literatura'},
        {id:5,name:'Negócio e Marketing Industrial'},
        {id:6,name:'Tecnologia'},
        {id:7, name:'Finanças'},
        {id:8, name:'Comida e Bebida'},
        {id:9, name:'Jogos'},
        {id:10, name:'Saúde'},
        {id:11, name:'Hobbies e Lazer'},
        {id:12, name:'Casa e Jardim'},
        {id:13, name:'Internet e Telecom'},
        {id:14, name:'Trabalho e Educação'},
        {id:15, name:'Leis e Governos'},
        {id:16, name:'Notícias'},
        {id:17, name:'Comunidades Online'},
        {id:18, name:'Pessoas e Sociedade'},
        {id:19, name:'Pets e Animais'},
        {id:20, name:'Imobiliária'},
        {id:21, name:'Referência'},
        {id:22, name:'Ciência'},
        {id:23, name:'Shopping'},
        {id:24, name:'Esporte'},
        {id:25, name:'Viagem'},
        {id:26, name:'Outros'},
        {id:25, name:'Não Especificado'}];

      var init = function(){
        $scope.selectBusiness = false;
        if($state.params.id !== undefined){
          AdvertisersService.getAdvertiser($scope.enterpriser.id,$state.params.id).then(
            function success(data){
              $scope.update = true;
              $scope.advertiserId = data.advertiserId;
              $scope.advertiserName = data.advertiserName,
              $scope.selectedGroups = {selected:data.typeOfBusiness};
              $scope.advertiserEmail = data.advertiserEmail;
              $scope.advertiserPhone = data.advertiserPhone;
              $scope.contactName = data.contactName;
              $scope.selectedGroups = {selected:{name:data.typeOfBusiness}};
            },
            function error(err){
              Exception.catcherError(err);
            }
          )
        }else{
          $scope.update = false;
          $scope.selectedGroups = {selected: []};
        }
      }

      function update(adv){
        AdvertisersService.updateAdvertiser(adv).then(
          function success(response){
            $scope.submitLoading = false;
            Exception.catcherSuccess(response).then(
              function success(result){
                if(result.successes[0].type === 'SUCCESS'){
                }
              }
            )
          },
          function error(err){
            $scope.submitLoading = false;
            Exception.catcherError(err);
          }
        );
      };

      $scope.$watch('selectedGroups.selected', function(){
        if($scope.advertiserForm.$submitted)
          $scope.selectBusiness = $scope.selectedGroups.selected.length <= 0 ? true : false;
      });

      function create(adv){
        AdvertisersService.createAdvertiser(adv).then(
          function success(response){
            $scope.submitLoading = false;
            Exception.catcherSuccess(response).then(
              function success(result){
                if(result.successes[0].type === 'SUCCESS'){
                  $state.go('app.advertisers', {enterpriserSlug: $scope.enterpriser.slug});
                }
              })
          },
          function error(err){
            $scope.submitLoading = false;
            Exception.catcherError(err);
          }
        );
      };

      $scope.save = function(){
        $scope.advertiserForm.$setSubmitted();
        if($scope.advertiserForm.$invalid || $scope.selectedGroups.selected.length <= 0){
          $scope.selectBusiness = $scope.selectedGroups.selected.length <= 0 ? true : false;
          main.scrollTop.run();
          return;
        }

        $scope.submitLoading = true;
        var adv = {
          'advertiserId':$scope.advertiserId,
          'accountId':$scope.enterpriser.id,
          'name':$scope.advertiserName,
          'typeOfBusiness':$scope.selectedGroups.selected.name,
          'email':$scope.advertiserEmail,
          'phone':$scope.advertiserPhone,
          'contactName':$scope.contactName,
        };

        if($state.params.id !== undefined){
          update(adv);
        }else{
          create(adv);
        }
      };

      init();
    }
  ]);
