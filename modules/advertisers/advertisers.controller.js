angular.module('cloudmidiaApp')
  .controller('AdvertisersController', ['$scope',
    'AdvertisersService',
    '$modal',
    '$q',
    'authService',
    'CampaignService',
    'ProviderService',
    'Exception',
    function ($scope,AdvertisersService,$modal, $q, authService,CampaignService,ProviderService,Exception) {
      'use strict';

      $scope.enterpriser = undefined;

      $scope.$watch('ordColumn.selected',function(){
        getAdvertisers();
      });

      var init = function(){
        $scope.enterpriser = ProviderService.getCurrentEnterpriser();
        getAdvertisers();
      };

      $scope.changeOrd = function(){
        if($scope.ord){
          $scope.ord = false;
        }else{
          $scope.ord = true;
        }
        getAdvertisers();
      }

      function decreaseOrCrescent(){
        if($scope.ord){
          return 'p'+columnsData[$scope.ordColumn.selected.id];
        }else{
          return 'm'+columnsData[$scope.ordColumn.selected.id];
        }
      }

      var columnsData=['name','email','contact','business','campaign','state'];

      $scope.dropdown = [{id:0,name:'Nome'},
        {id:1, name:'Email'},{id:2, name:'Contato'}, {id:3, name:'Segmento'},
        {id:4, name:'Campanhas'}];

      $scope.ordColumn = {selected:{id:0,name:'Nome'}};

      var getAdvertisers = function(){
        $scope.isLoadingAvailability = true;
        $scope.filterName = $scope.filterName !== '' ? $scope.filterName : null;
        var ord = decreaseOrCrescent()
        AdvertisersService.getAdvertisers($scope.enterpriser.id, $scope.filterName,$scope.currentPage,$scope.pageSize,ord).then(
          function (result){
            $scope.isLoadingAvailability = false;
            $scope.advertisers = result.items;
            $scope.totalItems = result.totalItems;
            $scope.pageSize = result.pageSize;
            $scope.showTable = result.totalItems > 0;
            $scope.showEmptyState = !$scope.showTable;
            $scope.emptySearch = false;
            if($scope.showEmptyState && ($scope.filterName !== undefined && $scope.filterName !== null)){
              $scope.showEmptyState = false;
              $scope.emptySearch = true;
            }
          },
          function error(err){
            Exception.catcherError(err, true);
            $scope.isLoadingAvailability = false;
          }
        );
      };

      $scope.search = function(keyCode){
        if (angular.isUndefined(keyCode) || keyCode === 13) {
          getAdvertisers();
        }
      };

      $scope.pageChanged = function (currentPage) {
        $scope.currentPage = currentPage;
        getAdvertisers();
      };

      $scope.remove = function(advertiser){
        var modalInstance = $modal.open({
          templateUrl: 'modules/advertisers/modal-advertiser-delete.html',
          controller: 'ModalAdvertiserController',
          backdrop: 'static',
          keyboard: false,
          resolve: {
            accountId: function () {
              return $scope.enterpriser.id;
            },
            advertiser: function () {
              return advertiser;
            }
          }
        });
        modalInstance.result.then(function () {
          init();
        }, function () {
          $log.info('Modal dismissed at: ' + new Date());
        });
      };

      init();

    }]);
