angular
  .module('cloudmidiaApp')
  .config(['$stateProvider', function ($stateProvider) {
    $stateProvider
      .state('app.advertisers',{
        url:'/p/:enterpriserSlug',
        template:'<ui-view></ui-view>',
        data:{
          requireAuthentication:true,
          context:'advertiser'
        }
      })
      .state('app.advertisers.list', {
        url: '/advertisers',
        templateUrl: 'modules/advertisers/advertisers.html',
        controller: 'AdvertisersController',
        data:{
          requireAuthentication:true,
          action:'ListAdvertiser'
        },
        resolve: {
          deps: ['$ocLazyLoad', function ($ocLazyLoad) {
            return $ocLazyLoad.load([
              'select',
              'underscore',
              'fileSaver',
              'ui-mask',
            ], {
              insertBefore: '#lazyload_placeholder'
            })
              .then(function () {
                return $ocLazyLoad.load([
                  'filters/propsFilter.js',
                  'modules/advertisers/advertisers.controller.js',
                  'services/advertisers.service.js',
                  'services/auth.service.js',
                  'services/campaigns.service.js',
                  'modules/advertisers/modal-advertiser-delete.controller.js',
                  'directives/phone-mask.js'
                ]);
              });
          }]
        }
      })
      .state('app.advertisers.create', {
        url: '/advertisers/create',
        templateUrl: 'modules/advertisers/advertisers-modify.html',
        controller: 'AdvertisersModifyController',
        data: {
          requireAuthentication: true,
          action:'CreateAdvertiser'
        },
        resolve: {
          deps: ['$ocLazyLoad', function ($ocLazyLoad) {
            return $ocLazyLoad
              .load([
                'select',
                'underscore',
                'fileSaver',
                'ui-mask',], {
                insertBefore: '#lazyload_placeholder'
              })
              .then(function () {
                return $ocLazyLoad.load([
                  'filters/propsFilter.js',
                  'services/advertisers.service.js',
                  'modules/advertisers/advertisers-modify.controller.js',
                  'services/auth.service.js',
                  'directives/phone-mask.js'

                ]);
              });
          }]
        }
      })
      .state('app.advertisers.updateAdvertisers', {
        url: '/advertisers/:id',
        templateUrl: 'modules/advertisers/advertisers-modify.html',
        controller: 'AdvertisersModifyController',
        data: {
          requireAuthentication: true,
          action:'UpdateAdvertiser'
        },
        resolve: {
          deps: ['$ocLazyLoad', function ($ocLazyLoad) {
            return $ocLazyLoad
              .load([
                'select',
                'underscore',
                'fileSaver',
                'ui-mask',], {
                insertBefore: '#lazyload_placeholder'
              })
              .then(function () {
                return $ocLazyLoad.load([
                  'filters/propsFilter.js',
                  'services/advertisers.service.js',
                  'modules/advertisers/advertisers-modify.controller.js',
                  'services/auth.service.js',
                  'directives/phone-mask.js'
                ]);
              });
          }]
        }
      });
  }]);
