; (function () {
  'use strict';

  angular
    .module('cloudmidiaApp')
    .controller('ContactController', [
      '$scope',
      '$rootScope',
      'authService',
      '$state',
      '$http',
      '$q',
      'AppSettings',
      'Exception',

      function ($scope, $rootScope, authService, $state,$http,$q,AppSettings, Exception) {

        var authentication = authService.authentication();
        function init(){
          $state.reload();
        }

        $scope.submitLoading = false;
        $scope.sendMessage = send;
        var url = AppSettings.urlApi + 'contact';

        function postMessage(){
          var deferred = $q.defer();
          $http.post(url,{'name':$scope.contact.name, 'phone':$scope.contact.phone,
            'message':$scope.contact.message,'email':$scope.contact.email})
            .success(function(data){
              deferred.resolve(data);
            })
            .error(function(error){
              Exception.catcherError(error);
              deferred.reject(error);
            });
          return deferred.promise;
        }

        function validateForm(){
          $scope.formContact.$setSubmitted();
          if($scope.formContact.$invalid){
            return true;
          }
        }

        function send(){

          if(validateForm()){
            return true;
          }
          $scope.submitLoading = true;
          postMessage().then(
            function success(response){
              $scope.submitLoading = false;
              Exception.catcherSuccess(response).then(
                function success(data){

                }
              )
              init();
            },
            function error(err){
              $scope.submitLoading = false;
              Exception.catcherError(err);
            }
          )
        }
        if (authentication.isAuth) {
          $state.go('lite.enterprises');
        }
      }
    ]);
})();
