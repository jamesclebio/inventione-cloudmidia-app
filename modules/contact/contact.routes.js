;(function () {
  'use strict';

  angular
    .module('cloudmidiaApp')
    .config([
    '$stateProvider',

    function ($stateProvider) {
      $stateProvider.state('app.contact', {
        url: '/contact',
        templateUrl: 'modules/contact/contact.html',
        controller: 'ContactController',
        data:{
          requireAuthentication: false
        },
        resolve: {
          deps: [
            '$ocLazyLoad',
            function ($ocLazyLoad) {
              return $ocLazyLoad.load([
                'simpleTextRotator',
                'ui-mask'
              ], {
                insertBefore: '#lazyload_placeholder'
              }).then(function () {
                return $ocLazyLoad.load([
                  'modules/contact/contact.controller.js',
                  'directives/phone-mask.js'
                ]);
              });
            }
          ]}
      });
    }
  ]);
})();
