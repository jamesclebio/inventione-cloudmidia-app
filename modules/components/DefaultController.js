angular.module('cloudmidiaApp')
  .controller('DefaultController',
  ['$rootScope',
    '$scope',
    'authService',
    '$state',
    'ProviderService',
    'Authorization',
    'localStorageService',
    function ($rootScope, $scope, authService, $state,ProviderService,Authorization, localStorageService) {
      'use strict';

      var getUserInfo = function () {
        if (authService.authentication().isAuth) {
          authService.profileInfo().then(
            function success(data) {
              $scope.profileInfo = data;
              $scope.authentication = true;
              $scope.enterprises = ProviderService.getAllEnterprise();
              $scope.enterpriser = ProviderService.getCurrentEnterpriser();
            }, function error() {
            });
        }
      };

      var isAuthorized = function(){
        var permission = localStorageService.get('permissions');
        if(permission){
          $scope.isPublishEnterpriser = localStorageService.get('currentEnterpriser').isPublisher;
          $scope.authorizeCamp = Authorization.authorized('ListCampaign','campaign',permission.data);
          $scope.authorizeLocal = Authorization.authorized('ListLocal','local',permission.data);
          $scope.authorizeAdv = Authorization.authorized('ListAdvertiser','advertiser',permission.data);
          $scope.authorizeDev  =Authorization.authorized('ListDevice','device',permission.data);
        }
      }

      isAuthorized();

      $scope.changeEnterprise = function(enterprise){
        $state.go($state.current.name,{enterpriserSlug:enterprise.slug});
      };

      var getUserInfoById = function(){
        if (authService.authentication().isAuth) {
          authService.profileInfoById($scope.enterpriser.id).then(
            function success(data) {
              $scope.profileInfo = data;
              $scope.enterprises = ProviderService.getAllEnterprise();
              $scope.enterpriser = ProviderService.getCurrentEnterpriser();
              authService.getUserPermissions($scope.enterpriser.id).then(
                function success(data){
                  isAuthorized();
                }
              );
            }, function error() {
            });
        }
      }

      $scope.isHomeState = function() {
        return $state.is('app.home');
      }

      getUserInfo();

      $rootScope.$on('updateSlugEnterprise',function(){
        $scope.enterpriser = ProviderService.getCurrentEnterpriser();
      });

      $rootScope.$on('updateEnterprise',function(){
        $scope.enterpriser = ProviderService.getCurrentEnterpriser();
      });

      $rootScope.$on('changedEnterpriser', function () {
        $scope.enterpriser = ProviderService.getCurrentEnterpriser();
        getUserInfoById();
      });

      $rootScope.$on('$$userAuthenticated', function () {
        getUserInfo();
      });

      $rootScope.$on('imageUpdatedEvent',function(){
          getUserInfo();
      });

      $scope.logout = function () {
        authService.logOut();
        $scope.authentication = false;
        $scope.profileInfo = {};
        $state.go('lite.login');
      };
    }]);
