angular.module('cloudmidiaApp')
  .controller('LocalsController', ['$scope',
    '$modal',
    '$log',
    'LocalsService',
    '$q',
    'authService',
    'ProviderService',
    'Exception',
    function ($scope, $modal, $log, LocalsService, $q, authService, ProviderService, Exception) {
      'use strict';

      $scope.showTable = false;
      $scope.status = {enabled:0,disabled:1};
      $scope.showEmptyState = false;
      $scope.localType = [{id: 0, name: 'Fixo'}, {id: 1, name: 'Movel'}];
      $scope.data = {};
      var enterpriser = undefined;
      $scope.dropdown = [{id:0,name:'Nome'},
        {id:1, name:'Tipo'},{id:2, name:'Acesso'}, {id:3, name:'Endereço'},
        {id:4, name:'Segmento'},{id:5, name:'Volume de pessoas'},{id:6, name:'Dispositivos'}];

      $scope.ordColumn = {selected:{id:0,name:'Nome'}};

      var init = function () {
        enterpriser = ProviderService.getCurrentEnterpriser();
        getLocals();
      };

      $scope.$watch('ordColumn.selected',function(){
        getLocals();
      });

      var columnsData = ['name','type','private','address','business','quantityOfViewers','devices'];

      $scope.changeOrd = function(){
        if($scope.ord){
          $scope.ord = false;
        }else{
          $scope.ord = true;
        }
        getLocals();
      };

      function decreaseOrCrescent(){
        if($scope.ord){
          return 'p'+columnsData[$scope.ordColumn.selected.id];
        }else{
          return 'm'+columnsData[$scope.ordColumn.selected.id];
        }
      };

      var getLocals = function () {
        $scope.isLoadingAvailability = true;
        $scope.filterName = $scope.filterName !== '' ? $scope.filterName : null;
        var ord = decreaseOrCrescent();
        LocalsService.getLocalsByPage(enterpriser.id, $scope.filterName, $scope.currentPage, $scope.pageSize, ord).then(
          function (result) {
            $scope.isLoadingAvailability = false;
            $scope.locals = result.items;
            $scope.totalItems = result.totalItems;
            $scope.pageSize = result.pageSize;
            $scope.showTable = $scope.totalItems > 0;
            $scope.showEmptyState = !$scope.showTable;
            $scope.emptySearch = false;
            if($scope.showEmptyState && ($scope.filterName !== undefined && $scope.filterName !== null)){
              $scope.showEmptyState = false;
              $scope.emptySearch = true;
            }
          },
          function error (err) {
            $scope.isLoadingAvailability = false;
            Exception.catcherError(err,true);
          }
        );
      };

      $scope.remove = function (local, size) {
        var modalInstance = $modal.open({
          animation: $scope.animationsEnabled,
          templateUrl: 'modules/locals/modal-locals-delete.html',
          controller: 'ModalLocalsDeleteController',
          windowClass: 'fade stick-up',
          backdrop: 'static',
          keyboard: false,
          size: size,
          resolve: {
            local: function () {
              return local;
            },
            accountId: function () {
              return enterpriser.id;
            }
          }
        });
        modalInstance.result.then(function () {
          getLocals();
        });
      };

      $scope.search = function (keyCode,filterName) {
        $scope.filterName = filterName;
        if (angular.isUndefined(keyCode) || keyCode === 13) {
          getLocals();
        }
      };

      $scope.pageChanged = function (currentPage) {
        $scope.currentPage = currentPage;
        getLocals();
      };

      $scope.updateStatus = function(local, status, size){
        var modalInstance = $modal.open({
          animation: $scope.animationsEnabled,
          templateUrl: 'modules/locals/modal-locals-update-status.html',
          controller: 'ModalLocalsUpdateStatusController',
          windowClass: 'fade stick-up',
          backdrop: 'static',
          keyboard: false,
          size: size,
          resolve: {
            local: function () {
              return local;
            },
            accountId: function () {
              return enterpriser.id;
            },
            localStatus: function(){
              return status;
            }
          }
        });
        modalInstance.result.then(function () {
          getLocals();
        });
      };

      init();
    }
  ]);
