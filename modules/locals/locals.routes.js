;(function () {
  'use strict';

  angular
    .module('cloudmidiaApp')
    .config([
      '$stateProvider',

      function ($stateProvider) {
        $stateProvider
          .state('app.locals',{
            url:'/p/:enterpriserSlug',
            template:'<ui-view></ui-view>',
            data: {
              requireAuthentication: true,
              context:'local'
            }
          })
          .state('app.locals.list', {
            url: '/locals',
            templateUrl: 'modules/locals/locals.html',
            controller: 'LocalsController',
            data: {
              requireAuthentication: true
            },
            resolve: {
              deps: [
                '$ocLazyLoad',

                function ($ocLazyLoad) {
                  return $ocLazyLoad
                    .load([
                      'select',
                      'underscore',
                      'fileSaver'
                    ], {
                      insertBefore: '#lazyload_placeholder'
                    })
                    .then(function () {
                      return $ocLazyLoad.load([
                        'filters/propsFilter.js',
                        'modules/locals/locals.controller.js',
                        'modules/locals/modal-locals-delete.controller.js',
                        'modules/locals/modal-locals-update-status.controller.js',
                        'services/locals.service.js',
                        'services/map.service.js',
                        'services/auth.service.js',
                        'directives/require-multiple-select.js',
                        'directives/autocomplete-googleplace.js'
                      ]);
                    });
                }
              ]
            }
          })
          .state('app.locals.create', {
            url: '/locals/create',
            templateUrl: 'modules/locals/locals-modify.html',
            controller: 'LocalsModifyController',
            data: {
              requireAuthentication: true
            },
            resolve: {
              deps: [
                '$ocLazyLoad',

                function ($ocLazyLoad) {
                  return $ocLazyLoad
                    .load([
                      'select',
                      'switchery',
                      'rangeslider',
                      'animateNumber'
                    ], {
                      insertBefore: '#lazyload_placeholder'
                    })
                    .then(function () {
                      return $ocLazyLoad.load([
                        'filters/propsFilter.js',
                        'modules/locals/locals-modify.controller.js',
                        'services/map.service.js',
                        'services/locals.service.js',
                        'directives/autocomplete-googleplace.js'
                      ]);
                    });
                }
              ]
            }
          }).
          state('app.locals.update', {
            url: '/locals/:id',
            templateUrl: 'modules/locals/locals-modify.html',
            controller: 'LocalsModifyController',
            data: {
              requireAuthentication: true
            },
            resolve: {
              deps: [
                '$ocLazyLoad',
                function ($ocLazyLoad) {
                  return $ocLazyLoad
                    .load([
                      'select',
                      'switchery',
                      'rangeslider',
                      'animateNumber'
                    ], {
                      insertBefore: '#lazyload_placeholder'
                    })
                    .then(function () {
                      return $ocLazyLoad.load([
                        'filters/propsFilter.js',
                        'modules/locals/locals-modify.controller.js',
                        'services/map.service.js',
                        'services/locals.service.js',
                        'directives/autocomplete-googleplace.js'
                      ]);
                    });
                }
              ]
            }
          })
      }
    ]);
})();
