;
(function () {
  'use strict';

  angular
    .module('cloudmidiaApp')
    .controller('LocalsModifyController', [
      '$scope',
      '$rootScope',
      '$state',
      'authService',
      'LocalsService',
      'Exception',
      '$timeout',
      'ProviderService',
      function ($scope, $rootScope, $state, authService, LocalsService, Exception, $timeout, ProviderService) {

        function reload() {
          $timeout(function () {
            $scope.$apply(function () {
              main.percentBar.builder($scope.plataform, $scope.infra, $scope.seller, $scope.owner);
            });
          }, 0);
        };

        $scope.typeLocal = [{id: 0, name: 'Fixo'}, {id: 1, name: 'Movel'}];

        $scope.typesOfBusiness = [{id: 1, name: 'Arte e Entretenimento'},
          {id: 2, name: 'Automotivo'},
          {id: 3, name: 'Beleza e Fitness'},
          {id: 4, name: 'Livros e Literatura'},
          {id: 5, name: 'Negócio e Marketing Industrial'},
          {id: 6, name: 'Tecnologia'},
          {id: 7, name: 'Finanças'},
          {id: 8, name: 'Comida e Bebida'},
          {id: 9, name: 'Jogos'},
          {id: 10, name: 'Saúde'},
          {id: 11, name: 'Hobbies e Lazer'},
          {id: 12, name: 'Casa e Jardim'},
          {id: 13, name: 'Internet e Telecom'},
          {id: 14, name: 'Trabalho e Educação'},
          {id: 15, name: 'Leis e Governos'},
          {id: 16, name: 'Notícias'},
          {id: 17, name: 'Comunidades Online'},
          {id: 18, name: 'Pessoas e Sociedade'},
          {id: 19, name: 'Pets e Animais'},
          {id: 20, name: 'Imobiliária'},
          {id: 21, name: 'Referência'},
          {id: 22, name: 'Ciência'},
          {id: 23, name: 'Shopping'},
          {id: 24, name: 'Esporte'},
          {id: 25, name: 'Viagem'},
          {id: 26, name: 'Outros'},
          {id: 25, name: 'Não Especificado'}];

        $scope.labels = ["Proprietário", "Vendedor", "Plataforma", "Infraestrutura"];


        var init = function () {
          $scope.selectBusiness = false;
          $scope.plataform = 25;
          $scope.enterpriser = ProviderService.getCurrentEnterpriser();
          if ($state.params.id !== undefined) {
            LocalsService.getLocal($scope.enterpriser.id, $state.params.id).then(
              function success(data) {
                $scope.update = true;
                $scope.localName = data.name;
                $scope.place = data.address;
                $scope.viewers = data.quantityOfViewers;
                $scope.owner = data.owner;
                $scope.price = data.price;
                $scope.seller = data.sales;
                $scope.infra = data.infra;
                $scope.chosenLocal = $scope.typeLocal[data.localsType];
                $scope.selectedGroups = {selected: businessMatch(data.typeOfBusiness)};
                $scope.private = data.private;
                reload();
              });
          } else {
            $scope.update = false;
            $scope.private = true;
            $scope.localName = '';
            $scope.place = '';
            $scope.viewers = '';
            $scope.owner = 25;
            $scope.price = '';
            $scope.seller = 25;
            $scope.infra = 25;
            $scope.selectedGroups = {selected: []};
            $scope.chosenLocal = $scope.typeLocal[0];
            main.percentBar.builder(25, 25, 25, 25);
          }
          $scope.maxOwner = 100 - ($scope.seller + $scope.plataform + $scope.infra);
          $scope.maxSeller = 100 - ($scope.owner + $scope.plataform + $scope.infra);

        }
        var totalPercent = 75;

        $scope.ownerCalc = function () {

          if ((parseInt($scope.owner)) === totalPercent) {
            $scope.seller = 0;
            $scope.infra = 0;
          } else {
            $scope.infra = ((parseInt($scope.seller) + parseInt($scope.owner))) >= totalPercent ? 0 : (totalPercent - (parseInt($scope.seller) + parseInt($scope.owner)));
            if ((parseInt($scope.owner) + parseInt($scope.seller)) > totalPercent) {
              $scope.seller = $scope.seller - Math.abs(totalPercent - (parseInt($scope.owner) + parseInt($scope.seller)));
            }
          }
          reload();
        };

        $scope.checked = function (value) {
          $scope.chosenLocal = $scope.typeLocal[value];
        };

        $scope.sellerCalc = function () {

          if ((parseInt($scope.seller)) === totalPercent) {
            $scope.owner = 0;
            $scope.infra = 0;
          } else {
            $scope.infra = ((parseInt($scope.seller) + parseInt($scope.owner))) >= totalPercent ? 0 : (totalPercent - (parseInt($scope.seller) + parseInt($scope.owner)));
            if ((parseInt($scope.owner) + parseInt($scope.seller)) > totalPercent) {
              $scope.owner = $scope.owner - Math.abs(totalPercent - (parseInt($scope.owner) + parseInt($scope.seller)));
            }
          }
          reload();
        };

        var businessMatch = function (businessName) {
          var value = _.filter($scope.typesOfBusiness, function (a) {
            return a.name === businessName
          });
          return value[0];
        }

        var geocode = new google.maps.Geocoder();

        function create(local) {
          LocalsService.addLocal(local).then(
            function success(response) {
              $scope.submitLoading = false;
              Exception.catcherSuccess(response).then(
                function success(result){
                  if (result.successes[0].type === 'SUCCESS') {
                    $state.go('app.locals', {enterpriserSlug: $scope.enterpriser.slug});
                  }
                }
              )
            },
            function error(err){
              Exception.catcherError(err);
            }
          )
        }

        function update(local, id) {
          var upLocal = local;
          upLocal.localId = id;
          LocalsService.updateLocal(local).then(
            function success(response) {
              $scope.submitLoading = false;
              Exception.catcherSuccess(response).then(
                function success(result){
                  if (result.successes[0].type === 'SUCCESS') {
                  }
                }
              )
            },
            function error(err){
              Exception.catcherError(err);
            }
          )
        }

        $scope.$watch('selectedGroups.selected', function(){
          if($scope.placesForm.$submitted)
            $scope.selectBusiness = $scope.selectedGroups.selected.length <= 0 ? true : false;
        });

        $scope.save = function () {
          $scope.placesForm.$setSubmitted();
          if($scope.placesForm.$invalid || $scope.selectedGroups.selected.length <= 0){
            $scope.selectBusiness = $scope.selectedGroups.selected.length <= 0 ? true : false;
            main.scrollTop.run();
            return;
          }
          $scope.submitLoading = true;
          var local = {
            'accountId': $scope.enterpriser.id,
            'name': $scope.localName,
            'typeOfBusiness': $scope.selectedGroups.selected.name,
            'localType': $scope.chosenLocal.id,
            'quantityOfViewers': $scope.viewers,
            'address': $scope.place,
            'price': $scope.price,
            'owner': $scope.owner,
            'infra':$scope.infra,
            'sales': $scope.seller,
            'private': $scope.private
          };
          geocode.geocode({'address': $scope.place},
            function (results, status) {
              if (status === google.maps.GeocoderStatus.OK && results.length > 0) {
                var location = results[0].geometry.location;
                local.latitude = location.lat();
                local.longitude = location.lng();
                if ($state.params.id !== undefined) {
                  update(local, $state.params.id);
                } else {
                  create(local);
                }
              }
            },
            function (err) {
              $scope.submitLoading = false;
              Exception.catcherError(err);
            });
        };

        init();
      }
    ]);
})();
