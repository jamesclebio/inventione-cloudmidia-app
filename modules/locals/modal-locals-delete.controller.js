angular.module('cloudmidiaApp')
  .controller('ModalLocalsDeleteController',
  ['$scope',
    'accountId',
    'local',
    '$modalInstance',
    'LocalsService',
    'Exception',
    function ($scope, accountId, local, $modalInstance, LocalsService,Exception) {
      'use strict';
      $scope.localName = local.name;
      $scope.ok = function(){
        $scope.submitLoading = true;
        LocalsService.excludeLocal(accountId,local.id).then(
          function success(response){
            $scope.submitLoading = false;
            Exception.catcherSuccess(response).then(
              function success(result){
                if(result.successes[0].type ==='SUCCESS'){
                  $modalInstance.close();
                }
              }
            )
          },
          function error(err){
            $scope.submitLoading = false;
            Exception.catcherError(err);
          }
        )
      };

      $scope.cancel = function () {
        $modalInstance.close('cancel');
      };

    }]);
