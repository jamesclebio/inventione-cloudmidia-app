/**
 * Created by Admin on 10/26/2016.
 */
angular.module('cloudmidiaApp')
  .controller('ModalLocalsUpdateStatusController',
  ['$scope',
    'accountId',
    'local',
    'localStatus',
    '$modalInstance',
    'LocalsService',
    'Exception',
    function ($scope, accountId, local, localStatus, $modalInstance, LocalsService,Exception) {
      'use strict';

      $scope.statusChoice= {enabled:0,disabled:1};
      $scope.status = localStatus;
      $scope.locals = local;
      $scope.ok = function(){
        $scope.submitLoading = true;
        LocalsService.updateLocalStatus(accountId,local.id, localStatus).then(
          function success(response){
            $scope.submitLoading = false;
            Exception.catcherSuccess(response).then(
              function success(result){
                if(result.successes[0].type ==='SUCCESS'){
                  $modalInstance.close();
                }
              }
            )
          },
          function error(err){
            $scope.submitLoading = false;
            Exception.catcherError(err);
          }
        )
      };
      $scope.cancel = function () {
        $modalInstance.close('cancel');
      };
    }
  ]);
