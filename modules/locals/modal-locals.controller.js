angular.module('cloudmidiaApp')
  .controller('ModalLocalController',
  ['$scope',
    '$state',
    'authService',
    '$q',
    '$modal',
    'ProviderService',
    'Exception',
    'LocalsService',
    'accountId',
    'isPublish',
    '$modalInstance',
    function ($scope, $state, authService, $q, $modal, ProviderService,
              Exception,LocalsService,accountId,isPublish, $modalInstance) {

      $scope.enterpriser = ProviderService.getCurrentEnterpriser();

      $scope.myLocal = {checked:isPublish};

      var init = function(){
        $scope.Locals = [];
        privateOrPublicLocals(!$scope.myLocal.checked);
        privateOrPublicLocals($scope.myLocal.checked);
        locals($scope.myLocal.checked)
      }

      var privateOrPublicLocals = function(isPrivate){
        var accountId = 0;
        if(isPrivate)
          accountId = $scope.enterpriser.id;
        $scope.filterName = $scope.filterName !== '' ? $scope.filterName : null;
        LocalsService.getValidLocals(accountId,$scope.filterName).then(
          function success(data){
            if(data.totalItems > 0 && isPrivate)
              $scope.privateLocals = true;
            else
              if(isPrivate)
                $scope.privateLocals = false;

            if(data.totalItems > 0 && !isPrivate)
              $scope.publicLocals = true;
            else
              if(!isPrivate)
                $scope.publicLocals = false;
          },
          function error(err){
            $scope.localLoading = false;
            Exception.catcherError(err,false);
          }
        )
      };

      $scope.search = function (keyCode, filterName) {
        $scope.filterName = filterName;
        if (angular.isUndefined(keyCode) || keyCode === 13) {
          locals($scope.myLocal.checked);
        }
      };

      $scope.change = function(){
          locals($scope.myLocal.checked);
          //$scope.Locals = undefined;
      }

      var fillOutData = function(data){
        $scope.Locals = data.items;
        $scope.totalItems = data.totalItems;
        $scope.pageSize = data.pageSize;
        $scope.showTable = data.totalItems > 0;
        $scope.showEmptyState = !$scope.showTable;
        $scope.emptySearch = false;
        if($scope.showEmptyState && ($scope.filterName !== undefined && $scope.filterName !== null)){
          $scope.showEmptyState = false;
          $scope.emptySearch = true;
        }
        for(var i = 0; i < $scope.Locals.length; i++){
          $scope.Locals[i].quantityOfDevices = $scope.Locals[i].quantDevices;
          $scope.Locals[i].boosting = $scope.Locals[i].quantDevices;
        }
      }

      var locals = function(allLocals){
        $scope.localLoading = true;
        var accountId = 0;
        if(allLocals)
          accountId = $scope.enterpriser.id;
        $scope.filterName = $scope.filterName !== '' ? $scope.filterName : null;
        LocalsService.getValidLocals(accountId,$scope.filterName).then(
          function success(data){
            if($scope.myLocal.checked && data.totalItems > 0)
              $scope.privateLocals = true;
            else
              if($scope.myLocal.checked)
                $scope.privateLocals = false;

            if(!$scope.myLocal.checked && data.totalItems > 0)
              $scope.publicLocals = true;
            else
              if(!$scope.myLocal.checked)
                $scope.publicLocals = false;

            fillOutData(data);
            $scope.localLoading = false;
          },
          function error(err){
            $scope.localLoading = false;
            Exception.catcherError(err,false);
          }
        )
      };

      $scope.checkedLocals = function (){
        var local = _.find($scope.Locals, function(item){
          return item.checked == true;
        });
        return local;
      }

      $scope.selected = function(){
        $modalInstance.close(_.filter($scope.Locals, function(item){
          return item.checked == true;
        }))
      }

      $scope.cancel = function () {
        $modalInstance.dismiss();
      };

      init();
    }
  ]
);
