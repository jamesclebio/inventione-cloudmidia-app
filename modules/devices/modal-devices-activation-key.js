angular
  .module('cloudmidiaApp')
  .controller('ModalDevicesActivationKey',
  ['$scope', '$modalInstance', 'device',
    function ($scope, $modalInstance, device) {
      'use strict';

      $scope.device = device;

      $scope.confirm = function () {
        $modalInstance.close();
      };

    }]);
