angular.module('cloudmidiaApp')
  .config(['$stateProvider', function ($stateProvider) {
    $stateProvider
      .state('app.devices',{
        url:'/p/:enterpriserSlug',
        template:'<ui-view></ui-view>',
        data: {
          requireAuthentication: true,
          context:'device'
        }
      })
      .state('app.devices.list', {
        url: '/devices',
        templateUrl: 'modules/devices/devices.html',
        controller: 'DevicesController',
        data:{
          requireAuthentication:true,
          action:'ListDevice'
        },
        resolve: {
          deps: ['$ocLazyLoad', function ($ocLazyLoad) {
            return $ocLazyLoad.load([
              'select',
              'underscore',
              'fileSaver'
            ], {
              insertBefore: '#lazyload_placeholder'
            })
              .then(function () {
                return $ocLazyLoad.load([
                  'filters/propsFilter.js',
                  'modules/devices/devices.controller.js',
                  'services/cloudmidia-api.service.js',
                  'services/devices.service.js',
                  'services/locals.service.js',
                  'services/map.service.js',
                  'services/auth.service.js',
                  'directives/require-multiple-select.js',
                  'directives/autocomplete-googleplace.js',
                  'modules/devices/modal-devices-activation-key.js',
                  'modules/devices/modal-devices-delete.controller.js',
                  'services/provider.services.js'
                ]);
              });
          }]
        }
      })
      .state('app.devices.create', {
        url: '/devices/create/{type:int}',
        templateUrl: 'modules/devices/devices-modify.html',
        controller: 'DevicesModifyController',
        data: {
          requireAuthentication: true
        },
        resolve: {
          deps: ['$ocLazyLoad', function ($ocLazyLoad) {
            return $ocLazyLoad
              .load([
                'select',
                'underscore',
                'fileSaver'
              ], {
                insertBefore: '#lazyload_placeholder'
              })
              .then(function () {
                return $ocLazyLoad.load([
                  'filters/propsFilter.js',
                  'modules/devices/devices-modify.controller.js',
                  'services/locals.service.js',
                  'services/auth.service.js',
                  'services/devices.service.js',
                  'services/provider.services.js',
                  'services/cloudmidia-api.service.js',
                ]);
              });
          }]
        }
      })
      .state('app.devices.update', {
        url: '/devices/:id',
        templateUrl: 'modules/devices/devices-modify.html',
        controller: 'DevicesModifyController',
        data: {
          requireAuthentication: true
        },
        resolve: {
          deps: ['$ocLazyLoad', function ($ocLazyLoad) {
            return $ocLazyLoad
              .load([
                'select',
                'underscore',
                'fileSaver'
              ], {
                insertBefore: '#lazyload_placeholder'
              })
              .then(function () {
                return $ocLazyLoad.load([
                  'filters/propsFilter.js',
                  'modules/devices/devices-modify.controller.js',
                  'services/locals.service.js',
                  'services/auth.service.js',
                  'services/devices.service.js',
                  'services/provider.services.js',
                  'services/cloudmidia-api.service.js',
                ]);
              });
          }]
        }
      });
  }]);
