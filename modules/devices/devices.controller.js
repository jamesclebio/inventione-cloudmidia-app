angular.module('cloudmidiaApp')
  .controller('DevicesController', ['NgTableParams',
    '$scope',
    '$modal',
    '$log',
    'DevicesService',
    '$q',
    'authService',
    'LocalsService',
    'ProviderService',
    'Exception',
    function (NgTableParams, $scope, $modal, $log, DevicesService, $q, authService,LocalsService,ProviderService,Exception) {
      'use strict';

      $scope.showTable = false;
      $scope.showEmptyState = false;
      var enterpriser = undefined;
      $scope.dropdown = [{id:0,name:'Nome'},{id:1, name:'Displays'},{id:2, name:'Locais'}, {id:3, name:'Status'}];

      var columnsData=['name','displays','locals','state'];
      $scope.ordColumn = {selected:{id:0,name:'Nome'}};

      $scope.$watch('ordColumn.selected',function(){
        getDevices();
      });

      var init = function(){
        enterpriser = ProviderService.getCurrentEnterpriser();
        if(enterpriser !== null) {
          getDevices();
        }
      }

      $scope.changeOrd = function(){
        if($scope.ord){
          $scope.ord = false;
        }else{
          $scope.ord = true;
        }
        getDevices();
      }

      function decreaseOrCrescent(){
        if($scope.ord){
          return 'p'+columnsData[$scope.ordColumn.selected.id];
        }else{
          return 'm'+columnsData[$scope.ordColumn.selected.id];
        }
      }

      $scope.online = function (deviceDate){
          var dateNow = new Date();
          var oldDate = new Date(deviceDate)
          if((dateNow-oldDate)/(60*1000)>10){
            return false;
          }
          return true;
      }

      var getDevices = function () {
        $scope.isLoadingAvailabitily = true;
        $scope.filterName = $scope.filterName !== '' ? $scope.filterName : null;
        var ord = decreaseOrCrescent();
        DevicesService.getDevices(enterpriser.id, $scope.filterName, $scope.currentPage, $scope.pageSize, ord).then(
          function (result) {
            $scope.devices = result.items;
            $scope.totalItems = result.totalItems;
            $scope.pageSize = result.pageSize;
            $scope.showTable = result.totalItems > 0;
            $scope.showEmptyState = !$scope.showTable;
            $scope.isLoadingAvailabitily = false;
            $scope.emptySearch = false;
            if($scope.showEmptyState && ($scope.filterName !== undefined && $scope.filterName !== null)){
              $scope.showEmptyState = false;
              $scope.emptySearch = true;
            }
          },
          function error (err){
            Exception.catcherError(err,true);
            $scope.isLoadingAvailabitily = false;
          }
        );
      };

      $scope.search = function (keyCode) {
        if (angular.isUndefined(keyCode) || keyCode === 13) {
          getDevices();
        }
      };

      $scope.pageChanged = function (currentPage) {
        $scope.currentPage = currentPage;
        getDevices();
      };


      $scope.showActivationKey = function (deviceInfo) {
        var modalInstance = $modal.open({
          animation: $scope.animationsEnabled,
          templateUrl: 'modules/devices/modal-devices-activation-key.html',
          controller: 'ModalDevicesActivationKey',
          backdrop: 'static',
          keyboard: false,
          size: 'md',
          windowClass: 'fade stick-up',
          resolve: {
            device: function () {
              return deviceInfo;
            }
          }
        });
      };


      $scope.remove = function (device, size) {
        var modalInstance = $modal.open({
          animation: $scope.animationsEnabled,
          templateUrl: 'modules/devices/modal-devices-delete.html',
          controller: 'DevicesModalController',
          windowClass: 'fade stick-up',
          backdrop: 'static',
          keyboard: false,
          size: size,
          resolve: {
            device: function () {
              return device;
            },
            accountId: function () {
              return enterpriser.id;
            }
          }
        });

        modalInstance.result.then(function (selectedItem) {
          $scope.selected = selectedItem;
          getDevices();
        }, function () {
          $log.info('Modal dismissed at: ' + new Date());
        });
      };

      $scope.download = function (device) {
        DevicesService.downloadFile(enterpriser.id, device.deviceId);
      };

      $scope.toggleAnimation = function () {
        $scope.animationsEnabled = !$scope.animationsEnabled;
      };

      init();
    }
  ]);
