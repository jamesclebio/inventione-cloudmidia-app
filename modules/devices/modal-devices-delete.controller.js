angular.module('cloudmidiaApp')
  .controller('DevicesModalController', ['$scope',
    'accountId',
    'device',
    '$modalInstance',
    'DevicesService',
    'Exception',
    function ($scope, accountId, device, $modalInstance, DevicesService,Exception) {
      'use strict';

      $scope.deviceName = device.deviceName;
      $scope.ok = function () {
        $scope.submitLoading = true;
        DevicesService.excludeDevice(accountId, device.deviceId).then(
          function success(response) {
            $scope.submitLoading = false;
            Exception.catcherSuccess(response).then(
              function success(result){
                if (result.successes[0].type === 'SUCCESS') {
                  $modalInstance.close();
                }
              }
            )
          },
          function error(err){
            Exception.catcherError(err);
            $scope.submitLoading = false;
          }
        );
      };

      $scope.cancel = function () {
        $modalInstance.close('cancel');
      };
    }]);
