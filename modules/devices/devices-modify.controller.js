angular
  .module('cloudmidiaApp')
  .controller('DevicesModifyController', [
    '$state',
    'authService',
    'DevicesService',
    '$scope',
    'LocalsService',
    'ProviderService',
    'CloudmidiaApi',
    'Exception',
    function ($state, authService, DevicesService,$scope, LocalsService, ProviderService,CloudmidiaApi, Exception) {
      'use strict';

      $scope.enterpriser = ProviderService.getCurrentEnterpriser();
      $scope.listLocals = [];

      var init = function(){
        $scope.selectedLocals = {selected:{}};
        if($state.params.id !== undefined){
          $scope.update = true;
          DevicesService.getDevice($scope.enterpriser.id, $state.params.id).then(
            function success(data){
              getLocals();
              $scope.deviceId = data.deviceId;
              $scope.deviceName = data.deviceName;
              $scope.numOfTvs = data.deviceNumOfTvs;
              $scope.listLocals = data.locals;
            },
            function error(err){
              Exception.catcherError(err);
            }
          )

        }else{
          $scope.update = false;
          getLocals();
        }
      }

      $scope.hasLocals = false;

      $scope.addLocal = function(){
        if($scope.selectedLocals.selected.id !== undefined ){
          $scope.listLocals.push($scope.selectedLocals.selected);
          $scope.selectLocals = $scope.listLocals.length <= 0 ? true : false;
          var index =  $scope.locals.indexOf($scope.selectedLocals.selected);
          if(index > -1)
            $scope.locals.splice(index,1);
          $scope.selectedLocals = {selected:{}};
        }
      }

      $scope.removeLocal = function(local){
        $scope.locals.push(local);
        var index =  $scope.listLocals.indexOf(local);
        if(index > -1)
          $scope.listLocals.splice(index,1);
        $scope.selectLocals = $scope.listLocals.length <= 0 ? true : false;
      }

      function getLocals (){
        LocalsService.getLocals($scope.enterpriser.id).then(
          function success(data){
            $scope.locals = data;
            if($scope.update){
              for(var i = 0; i < $scope.listLocals.length; i++){
                var obj = _.where($scope.locals, {'id':$scope.listLocals[i].id});
                var index = $scope.locals.indexOf(obj[0]);
                $scope.locals.splice(index,1);
              }
            }
          },
          function error(err){
            Exception.catcherError(err);
          }
        )
      }

      function create(device){
        CloudmidiaApi.DeviceApi.save(device,
          function success(response) {
            $scope.submitLoading = false;
            Exception.catcherSuccess(response).then(
              function(result){
                if (result.successes[0].type === 'SUCCESS') {
                  $state.go('app.devices', {enterpriserSlug: $scope.enterpriser.slug});
                }
              }
            )
          },
          function error(err) {
            Exception.catcherError(err);
            $scope.submitLoading = false;
          }
        );
      }

      function update(device){
        device.id = $scope.deviceId;
        DevicesService.updateDevice(
          $scope.enterpriser.id, device
        ).then(
          function success(response) {
            Exception.catcherSuccess(response).then(
              function success(result){
                $scope.submitLoading = false;
              }
            )
          },
          function error(err){
            Exception.catcherError(err);
            $scope.submitLoading = false;
          }
        );
      }

      $scope.save = function () {
        $scope.deviceForm.$setSubmitted();
        if($scope.deviceForm.$invalid || $scope.listLocals.length <= 0){
          $scope.selectLocals = $scope.listLocals.length <= 0 ? true : false;
          main.scrollTop.run();
          return;
        }
        $scope.submitLoading = true;
        var device = {
          'accountId': $scope.enterpriser.id,
          'name': $scope.deviceName,
          'numOfTvs': $scope.numOfTvs,
          'locals': _.pluck($scope.listLocals,'id'),
        };
        if($scope.update){
          $scope.deviceForm.$submitted = false;
          update(device);
        }else{
          create(device);
        }
      };

      init();
    }
  ]);
