angular.module('cloudmidiaApp')
  .controller('ModalGroupsDeleteController', ['$scope', 'accountId', 'group', '$modalInstance', 'GroupsService',
    function ($scope, accountId, group, $modalInstance, GroupsService) {
      'use strict';
      $scope.groupName = group.name;
      $scope.ok = function(){
        $scope.submitLoading = true;
        GroupsService.excludeGroup(accountId,group.id).then(
          function success(response){
            $scope.submitLoading = false;
            if(response.messages[0].type ==='SUCCESS'){
              $modalInstance.close();
            }
          },
          function error(err){
            $scope.submitLoading = false;
          }
        );
      };

      $scope.cancel = function () {
        $modalInstance.close('cancel');
      };

    }]);
