angular.module('cloudmidiaApp')
  .config(['$stateProvider', function ($stateProvider) {
    $stateProvider
      .state('app.groups', {
        url: '/p/:enterpriserSlug/groups',
        templateUrl: 'modules/groups/groups.html',
        controller: 'GroupsController',
        data:{
          requireAuthentication:true
        },
        resolve: {
          deps: ['$ocLazyLoad', function ($ocLazyLoad) {
            return $ocLazyLoad.load([
              'select',
              'underscore',
              'fileSaver'
            ], {
              insertBefore: '#lazyload_placeholder'
            })
              .then(function () {
                return $ocLazyLoad.load([
                  'filters/propsFilter.js',
                  'modules/groups/groups.controller.js',
                  'services/groups.service.js',
                  'services/auth.service.js',
                  'modules/groups/modal-groups-modify.controller.js',
                  'modules/groups/modal-groups-delete.controller.js'
                ]);
              });
          }]
        }
      });
  }]);
