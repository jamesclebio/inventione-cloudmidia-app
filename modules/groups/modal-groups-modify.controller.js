angular
  .module('cloudmidiaApp')
  .controller('ModalGroupsModifyController',
  ['$scope', '$modalInstance','group', 'accountId','GroupsService',
    function ($scope, $modalInstance,group, accountId,GroupsService) {
      'use strict';

      $scope.type = group === '' ? true : false;

      $scope.group = group !== ''? group : '';

      $scope.groupName = group.name;

      $scope.create = function () {
        var group = {
          'accountId': accountId,
          'name': $scope.groupName,
        };
        $scope.submitLoading = true;
        GroupsService.createGroup(group).then(
          function success(response) {
            $scope.submitLoading = false;
            if ( response.messages[0].type === 'SUCCESS' ) {
              $modalInstance.close();
            }
          },
          function error(err){
            $scope.submitLoading = false;
          }
        );
      };

      $scope.update = function () {
        $scope.submitLoading = true;
        GroupsService.updateGroup(
          accountId,$scope.group.id,$scope.groupName
        ).then(
          function success(response) {
            $scope.submitLoading = false;
            if (response.messages[0].type === 'SUCCESS') {
              $modalInstance.close();
            }
          },
          function error(err){
            $scope.submitLoading = false;
          });
      };

      $scope.cancel = function () {
        $modalInstance.dismiss();
      };


    }]);


