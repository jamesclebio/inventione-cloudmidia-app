angular.module('cloudmidiaApp')
  .controller('GroupsController', ['$scope',
    '$modal',
    '$log',
    'GroupsService',
    '$q',
    'authService',
    '$rootScope',
    'ProviderService',
    'exception',
    'logger',
    '$location',
    function ($scope, $modal, $log, GroupsService, $q, authService,$rootScope,ProviderService,exception,
              logger,$location) {
      'use strict';

      $scope.showTable = false;
      $scope.showEmptyState = false;
      var enterpriser = undefined;

      var accountId = authService.getSavedProfileInfo().accountId;

      var init = function(){
        enterpriser = ProviderService.getCurrentEnterpriser();
        getGroups();
      };

      var getGroups = function () {
        $scope.isLoadingAvailability = true;
        $scope.filterName = $scope.filterName !== '' ? $scope.filterName : null;
        GroupsService.getGroupsByPage(enterpriser.id, $scope.filterName, $scope.currentPage, $scope.pageSize).then(
          function (result) {
            $scope.isLoadingAvailability = false;
            $scope.groups = result.items;
            $scope.totalItems = result.totalItems;
            $scope.pageSize = result.pageSize;
            $scope.showTable = result.totalItems > 0;
            $scope.showEmptyState = !$scope.showTable;
          },
          function(error){
            $scope.isLoadingAvailability = false;
          }
        );
      };

      $scope.search = function (keyCode) {
        if (angular.isUndefined(keyCode) || keyCode === 13) {
          getGroups();
        }
      };

      $scope.pageChanged = function (currentPage) {
        $scope.currentPage = currentPage;
        getGroups();
      };

      $scope.addGroup = function () {
        var modalInstance = $modal.open({
          animation: $scope.animationsEnabled,
          templateUrl: 'modules/groups/modal-groups-modify.html',
          controller: 'ModalGroupsModifyController',
          backdrop: 'static',
          keyboard: false,
          size: 'md',
          windowClass: 'fade stick-up',
          resolve: {
            accountId: function () {
              return enterpriser.id;
            },
            group: function(){
              return '';
            }
          }
        });
        modalInstance.result.then(function () {
          getGroups();
        });
      };

      $scope.update = function (group) {
        var modalInstance = $modal.open({
          animation: $scope.animationsEnabled,
          templateUrl: 'modules/groups/modal-groups-modify.html',
          controller: 'ModalGroupsModifyController',
          backdrop: 'static',
          keyboard: false,
          size: 'md',
          windowClass: 'fade stick-up',
          resolve: {
            accountId: function () {
              return enterpriser.id;
            },
            group: function(){
              return group;
            }
          }
        });
        modalInstance.result.then(function () {
          getGroups();
        });
      };

      $scope.remove = function (group, size) {
        var modalInstance = $modal.open({
          animation: $scope.animationsEnabled,
          templateUrl: 'modules/groups/modal-groups-delete.html',
          controller: 'ModalGroupsDeleteController',
          windowClass: 'fade stick-up',
          backdrop: 'static',
          keyboard: false,
          size: size,
          resolve: {
            group: function () {
              return group;
            },
            accountId: function () {
              return enterpriser.id;
            }
          }
        });

        modalInstance.result.then(function (selectedItem) {
          $scope.selected = selectedItem;
          getGroups();
        }, function () {
          $log.info('Modal dismissed at: ' + new Date());
        });
      };

     init();
    }
  ]);
