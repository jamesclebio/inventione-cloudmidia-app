/**
 * Created by Admin on 8/27/2016.
 */
;(function () {
  'use strict';

  angular
    .module('cloudmidiaApp')
    .config([
      '$stateProvider',

      function ($stateProvider) {
        $stateProvider.state('app.privacy', {
          url: '/privacy',
          templateUrl: 'modules/privacy/privacy.html',
          controller: 'PrivacyController',
          data:{
            requireAuthentication: false
          },
          resolve: {
            deps: [
              '$ocLazyLoad',
              function ($ocLazyLoad) {
                return $ocLazyLoad.load([
                  'simpleTextRotator'
                ], {
                  insertBefore: '#lazyload_placeholder'
                }).then(function () {
                  return $ocLazyLoad.load([
                    'modules/privacy/privacy.controller.js'
                  ]);
                });
              }
            ]}
        });
      }
    ]);
})();
