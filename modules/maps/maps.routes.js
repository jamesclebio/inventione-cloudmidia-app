angular.module('cloudmidiaApp')
  .config(['$stateProvider', function ($stateProvider) {
    $stateProvider
      .state('app.maps', {
        url: '/p/:enterpriserSlug/maps',
        templateUrl: 'modules/maps/maps-group.html',
        controller: 'MapController',
        data:{
          requireAuthentication:true
        },
        resolve: {
          deps: ['$ocLazyLoad', function ($ocLazyLoad) {
            return $ocLazyLoad.load([
              'select',
              'underscore',
              'fileSaver'
            ], {
              insertBefore: '#lazyload_placeholder'
            })
              .then(function () {
                return $ocLazyLoad.load([
                  'modules/maps/maps.controller.js',
                  'services/cloudmidia-api.service.js',
                  'services/devices.service.js',
                  'services/auth.service.js',
                  'services/groups.service.js',
                  'services/location.service.js'
                ]);
              });
          }]
        }
      });
  }]);
