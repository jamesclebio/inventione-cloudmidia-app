angular.module('cloudmidiaApp')
  .controller('MapController', ['$scope',
    'GroupsService',
    'authService',
    'DevicesService',
    'LocationService',
    '$q',
    '$rootScope',
    'ProviderService',
    'exception',
    function ($scope, GroupsService, authService, DevicesService, LocationService, $q, $rootScope,
              ProviderService, exception) {
      'use strict';

      var accountId = authService.getSavedProfileInfo().accountId;
      var deviceByGroup;
      var defaultMarkers;
      $scope.mapControl = {};
      $scope.map = {center: {latitude: 45, longitude: -73}, zoom: 8};

      var enterpriser = undefined;

      var init = function () {
        enterpriser = ProviderService.getCurrentEnterpriser();
        getGroups().then(function success() {
          addDeviceMarker();
        }).catch(function (message) {
          exception.catcher('Erro no servidor')(message, true);
        });
        ;
      }

      google.maps.event.addListener($scope.map, 'zoom_changed', function () {
        alert('zoom');
      });

      var getLocation = function () {
        LocationService.getCurrentPosition().then(
          function (data) {
            $scope.map.center.latitude = data.coords.latitude;
            $scope.map.center.longitude = data.coords.longitude;
          }
        ).catch(function (message) {
            exception.catcher('Erro no servidor')(message, true);
          });
      }

      $scope.onClick = function (marker, eventName, model) {
        model.show = !model.show;

      };

      var getGroups = function () {
        $scope.isLoadingAvailability = true;
        var deferred = $q.defer();
        GroupsService.getGroups(enterpriser.id).then(
          function success(data) {
            $scope.isLoadingAvailability = false;
            $scope.groups = data;
            for (var i = 0; i < data.length; i++) {
              $scope.groups[i].devices = 0;
            }
            deferred.resolve(data);
          }
        ).catch(function (message) {
            exception.catcher('Erro no servidor')(message, true);
          });
        return deferred.promise;
      }

      $scope.isActive = function (item) {
        if (item === 'default') {
          return true;
        } else {
          return $scope.selected === item;
        }
      }

      $scope.filterDeviceByGroup = function (group) {
        $scope.GroupMarkersSelected = [];
        $scope.selected = group;
        var markers = [];
        if (group !== 'default') {
          for (var i = 0; i < deviceByGroup.length; i++) {
            var groups = [];
            for (var j = 0; j < deviceByGroup[i].groups.length; j++) {
              if (deviceByGroup[i].groups[j].id === group.id) {
                groups.push(deviceByGroup[i].groups[j].name);
                $scope.GroupMarkersSelected.push(deviceByGroup[i]);
                markers.push(assemblyMarkers(deviceByGroup[i], groups));
              }
            }
          }
        } else {
          $scope.GroupMarkersSelected = deviceByGroup;
          markers = defaultMarkers;
        }
        $scope.GroupMarkers = markers;
        if ($scope.GroupMarkersSelected.length > 0) {
          var myBounds = new google.maps.LatLngBounds();
          myBounds = extendsBound($scope.GroupMarkersSelected, myBounds);
          $scope.map.bounds = {
            northeast: {
              latitude: myBounds.getNorthEast().lat(),
              longitude: myBounds.getNorthEast().lng()
            },
            southwest: {
              latitude: myBounds.getSouthWest().lat(),
              longitude: myBounds.getSouthWest().lng()
            }
          };
        }
      }

      var extendsBound = function (markers, myBounds) {
        for (var k = 0; k < markers.length; k++) {
          myBounds.extend(new google.maps.LatLng(markers[k].latitude, markers[k].longitude));
        }
        return myBounds;
      }

      var assemblyMarkers = function (data, groups) {
        return {
          latitude: data.latitude,
          longitude: data.longitude,
          id: data.id,
          icon: 'http://maps.google.com/mapfiles/ms/icons/red-dot.png',
          title: {
            name: data.name, status: data.activated ? 'sim' : 'não',
            groups: groups.toString(), scheduleLastUpdate: data.scheduleLastUpdate
          }
        };
      }

      var getDevicesByGroup = function (groups, devices) {
        var index;
        for (var i = 0; i < devices.length; i++) {
          for (var j = 0; j < devices[i].groups.length; j++) {
            for (var k = 0; k < groups.length; k++) {
              if ($scope.groups[k].devices === 'undefined') {
                $scope.groups[k].devices = 0;
              }
              if (groups[k].id === devices[i].groups[j].id) {
                $scope.groups[k].devices += 1;
              }
            }
          }
        }
      }

      var addDeviceMarker = function () {
        DevicesService.getDevicesGroups(enterpriser.id).then(
          function success(data) {
            var markers = [];
            deviceByGroup = data;
            for (var i = 0; i < data.length; i++) {
              var groups = [];
              for (var j = 0; j < data[i].groups.length; j++) {
                groups.push(data[i].groups[j].name);
                console.log(data[i].latitude);
                console.log(data[i].longitude);
              }
              markers.push(assemblyMarkers(data[i], groups));
            }
            getDevicesByGroup($scope.groups, deviceByGroup);
            $scope.GroupMarkers = markers;
            defaultMarkers = markers;
            var myBounds = new google.maps.LatLngBounds();
            myBounds = extendsBound(markers, myBounds);
            $scope.map.bounds = {
              northeast: {
                latitude: myBounds.getNorthEast().lat(),
                longitude: myBounds.getNorthEast().lng()
              },
              southwest: {
                latitude: myBounds.getSouthWest().lat(),
                longitude: myBounds.getSouthWest().lng()
              }
            };
          }
        ).catch(function (message) {
            exception.catcher('Erro no servidor')(message, true);
          });
      }
      init();
    }
  ]);
