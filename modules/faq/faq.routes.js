;(function () {
  'use strict';

  angular
    .module('cloudmidiaApp')
    .config(config);

  config.$inject = [
    '$stateProvider'
  ];

  function config($stateProvider) {
    $stateProvider
      .state('app.faq', {
        url: '/faq',
        templateUrl: 'modules/faq/faq.html',
        controller: 'FaqController',
        data: {
          requireAuthentication: false
        },
        resolve: {
          deps: ['$ocLazyLoad', function ($ocLazyLoad) {
            return $ocLazyLoad
              .load([], {
                insertBefore: '#lazyload_placeholder'
              })
              .then(function () {
                return $ocLazyLoad.load([
                  'modules/faq/faq.controller.js'
                ]);
              });
          }]
        }
      });
  }
}());
