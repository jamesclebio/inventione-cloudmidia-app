;(function () {
  'use strict';

  angular
    .module('cloudmidiaApp')
    .controller('HowItWorksController', HowItWorksController);

  HowItWorksController.$inject = [
    '$state',
    '$stateParams'
  ];

  function HowItWorksController($state, $stateParams) {}
})();
