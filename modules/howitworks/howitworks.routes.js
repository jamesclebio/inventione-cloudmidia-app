;(function () {
  'use strict';

  angular
    .module('cloudmidiaApp')
    .config(config);

  config.$inject = [
    '$stateProvider'
  ];

  function config($stateProvider) {
    $stateProvider
      .state('app.howitworks', {
        url: '/howitworks',
        templateUrl: 'modules/howitworks/howitworks.html',
        controller: 'HowItWorksController',
        data: {
          requireAuthentication: false
        },
        resolve: {
          deps: ['$ocLazyLoad', function ($ocLazyLoad) {
            return $ocLazyLoad
              .load([], {
                insertBefore: '#lazyload_placeholder'
              })
              .then(function () {
                return $ocLazyLoad.load([
                  'modules/howitworks/howitworks.controller.js'
                ]);
              });
          }]
        }
      });
  }
}());
