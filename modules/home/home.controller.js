; (function () {
  'use strict';

  angular
    .module('cloudmidiaApp')
    .controller('HomeController', [
      '$scope',
      '$q',
      '$http',
      '$rootScope',
      'authService',
      '$state',
      '$stateParams',
      'AppSettings',
      '$location',
      'Exception',
      'CloudmidiaApi',
      function ($scope, $q, $http, $rootScope, authService, $state, $stateParams, AppSettings, $location, Exception, CloudmidiaApi) {

        var authentication = authService.authentication();

        if (authentication.isAuth) {
          $state.go('lite.enterprises');
        }

        function init(){
          $state.reload();
        }

        $scope.submitLoading = false;
        $scope.sendMessage = send;
        var url = AppSettings.urlApi + 'contact';

        $scope.user = {}
        $scope.userPublish = {};
        $scope.userAdvertiser = {};
        $scope.token = $stateParams.token ? $stateParams.token: undefined;
        if(!$scope.token){
          $scope.token = $rootScope.invitationToken;
        }

        $scope.user.token = $scope.token;
        $scope.baseUrl = AppSettings.urlOrigin + 'p/';

        var redirect = function (url) {
          $location.path(url);
          $location.url($location.path());
        };

        function validateForm(){
          $scope.formContact.$setSubmitted();
          if($scope.formContact.$invalid){
            return true;
          }
        }

        $scope.registerPublish = function(){
          $scope.registerFormPublish.$setSubmitted();
          if($scope.registerFormPublish.$invalid || $scope.alreadyExist){
            return;
          }else{
            $scope.user = $scope.userPublish;
            $scope.register(true);
          }
        }

        $scope.registerAdvertiser = function(){
          $scope.registerFormAdvertiser.$setSubmitted();
          if($scope.registerFormAdvertiser.$invalid || $scope.alreadyExist){
            return;
          }else{
            $scope.user = $scope.userAdvertiser;
            $scope.register(false);
          }
        }

        $scope.register = function (isPublisher) {
          $scope.submitLoading = true;
          $scope.user.slug = $scope.justSlug;
          $scope.user.isPublisher = isPublisher;
          CloudmidiaApi.RegisterApi.save($scope.user,
            function success(data) {
              $scope.submitLoading = false;
              Exception.catcherSuccess(data).then(
                function success(result){
                  if (result.successes.length > 0 && result.successes[0].type === 'SUCCESS') {
                    redirect('/login');
                  }
                  else
                    return;
                }
              )
            },
            function error(err) {
              $scope.submitLoading = false;
              Exception.catcherError(err);
            });
        };

        //Send message of contact
        function postMessage(){
          var deferred = $q.defer();
          $http.post(url,{'name':$scope.contact.name, 'phone':$scope.contact.phone,
            'message':$scope.contact.message,'email':$scope.contact.email})
            .success(function(data){
              deferred.resolve(data);
            })
            .error(function(error){
              Exception.catcherError(error);
              deferred.reject(error);
            });
          return deferred.promise;
        }

        function send(){

          if(validateForm()){
            return true;
          }
          $scope.submitLoading = true;
          postMessage().then(
            function success(response){
              $scope.submitLoading = false;
              Exception.catcherSuccess(response).then(
                function success(data){

                }
              )
              init();
            },
            function error(err){
              $scope.submitLoading = false;
              Exception.catcherError(err);
            }
          )
        }
      }
    ]);
})();
