;(function () {
  'use strict';

  angular
    .module('cloudmidiaApp')
    .config([
    '$stateProvider',

    function ($stateProvider) {
      $stateProvider.state('app.home', {
        url: '/home',
        templateUrl: 'modules/home/home.html',
        controller: 'HomeController',
        data:{
          requireAuthentication: false
        },
        resolve: {
          deps: [
            '$ocLazyLoad',
            function ($ocLazyLoad) {
              return $ocLazyLoad.load([
                'simpleTextRotator'
              ], {
                insertBefore: '#lazyload_placeholder'
              }).then(function () {
                return $ocLazyLoad.load([
                  'modules/home/home.controller.js',
                  'services/cloudmidia-api.service.js',
                  'services/invitation.service.js'
                ]);
              });
            }
          ]}
      });
    }
  ]);
})();
