;(function () {
  'use strict';

  angular
    .module('cloudmidiaApp')
    .controller('PasswordForgotController', [
      '$scope',
      '$rootScope',
      '$state',
      'authService',
      'CloudmidiaApi',
      'AppSettings',
      '$q',
      '$http',
      'Exception',
      function ($scope, $rootScope, $state, authService,CloudmidaApi,AppSettings,$q,$http, Exception) {

        var accountURI = AppSettings.urlApi + 'account/forgot-password';

        var sendEmail = function(email){
          var deferred = $q.defer();
          $http.post(accountURI, {'email': email})
            .success(function (data) {
              deferred.resolve(data);
            }).error(function (error) {
              deferred.reject(error);
            });
          return deferred.promise;
        };

        $scope.forgotPassword = {
          email:''
        };

        $scope.submitLoading = false;

        $scope.emailSend = false;

        $scope.ok = function() {
          $scope.passwordForm.$setSubmitted();
          if($scope.passwordForm.$invalid){
            return;
          }
          $scope.submitLoading = true;
          if($scope.passwordForm.$invalid){
            return;
          }
          sendEmail($scope.forgotPassword.email).then(
            function success(response){
              Exception.catcherSuccess(response).then(
                function success(data){
                  if (data.errors.length > 0) {
                    $scope.emailSend = false;
                  }else{
                    $scope.emailSend = true;
                  }
                  $scope.submitLoading = false;
                }
              )
            },
            function error(err){
              $scope.emailSend = false;
              $scope.submitLoading = false;
              Exception.catcherError(err);
            }
          );
        };
      }
    ]);
})();
