;(function () {
  'use strict';

  angular
    .module('cloudmidiaApp')
    .config([
      '$stateProvider',

      function ($stateProvider) {
        $stateProvider
          .state('lite.register', {
            url: '/register?token',
            templateUrl: 'modules/register/register.html',
            controller: 'RegisterController',
            data: {
              requireAuthentication: false
            },
            resolve: {
              deps: [
                '$ocLazyLoad',

                function ($ocLazyLoad) {
                  return $ocLazyLoad
                    .load([
                      'underscore'
                    ], {
                      insertBefore: '#lazyload_placeholder'
                    })
                    .then(function () {
                      return $ocLazyLoad.load([
                        'modules/register/register.controller.js',
                        'services/cloudmidia-api.service.js',
                        'services/invitation.service.js',
                        'directives/password-check.js',
                      ]);
                    });
                }
              ]
            }
          })
          .state('lite.login', {
            url: '/login?redirectTo',
            cache:false,
            templateUrl: 'modules/register/login.html',
            controller: 'LoginController',
            data: {
              requireAuthentication: false
            },
            resolve: {
              deps: [
                '$ocLazyLoad',

                function ($ocLazyLoad) {
                  return $ocLazyLoad
                    .load([], {
                      insertBefore: '#lazyload_placeholder'
                    })
                    .then(function () {
                      return $ocLazyLoad.load([
                        'services/cloudmidia-api.service.js',
                        'services/auth.service.js',
                        'services/provider.services.js',
                        'modules/register/login.controller.js'
                      ]);
                    });
                }
              ]
            }
          })
          .state('lite.passwordForgot', {
            url: '/password/forgot',
            templateUrl: 'modules/register/password-forgot.html',
            controller: 'PasswordForgotController',
            data: {
              requireAuthentication: false
            },
            resolve: {
              deps: [
                '$ocLazyLoad',

                function ($ocLazyLoad) {
                  return $ocLazyLoad
                    .load([], {
                      insertBefore: '#lazyload_placeholder'
                    })
                    .then(function () {
                      return $ocLazyLoad.load([
                        'services/cloudmidia-api.service.js',
                        'services/auth.service.js',
                        'services/provider.services.js',
                        'modules/register/password-forgot.controller.js'
                      ]);
                    });
                }
              ]
            }
          })
          .state('lite.passwordReset', {
            url: '/reset-password?user&token',
            templateUrl: 'modules/register/password-reset.html',
            controller: 'PasswordResetController',
            data: {
              requireAuthentication: false
            },
            resolve: {
              deps: [
                '$ocLazyLoad',

                function ($ocLazyLoad) {
                  return $ocLazyLoad
                    .load([], {
                      insertBefore: '#lazyload_placeholder'
                    })
                    .then(function () {
                      return $ocLazyLoad.load([
                        'services/cloudmidia-api.service.js',
                        'services/auth.service.js',
                        'services/provider.services.js',
                        'modules/register/password-reset.controller.js',
                        'directives/password-check.js',
                      ]);
                    });
                }
              ]
            }
          });
      }
    ]);
})();
