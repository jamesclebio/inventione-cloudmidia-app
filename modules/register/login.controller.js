angular.module('cloudmidiaApp')
  .controller('LoginController', ['$scope',
    'authService',
    '$state',
    '$location',
    '$stateParams',
    '$modal',
    'Exception',
    '$rootScope',
    'ProviderService',
    function ($scope, authService, $state, $location, $stateParams, $modal, Exception, $rootScope, ProviderService) {
      'use strict';

      $scope.loginForm = {};
      var urlEnterprise = '/enterprises'

      var urlEnterprise = '/enterprises';

      var redirectTo = $stateParams.redirectTo;

      $scope.enter = function () {
        $scope.personal.$setSubmitted();
        if($scope.personal.$invalid){
          return true;
        }
        $scope.submitLoading = true;
        authService.login($scope.loginForm).then(
          function (response) {
            if (redirectTo !== undefined) {
              authService.profileInfo().then(
                function (data) {
                  $scope.submitLoading = false;
                  if ($rootScope.returnToStateParams) {
                    ProviderService.getEnterprise(data, $rootScope.returnToStateParams.enterpriserSlug).then(
                      function success () {
                        $state.go('lite.enterprises', {redirectTo:redirectTo});
                      }
                    );
                  } else {
                    if($state.href(redirectTo))
                      $state.go(redirectTo);
                    else
                      $location.url(urlEnterprise);
                  }
                });
            } else {
              $scope.submitLoading = false;
              $location.url(urlEnterprise);
            }
          },
          function error(err) {
            $scope.submitLoading = false;
            Exception.catcherError(err);
          }
        )
      };

      $scope.newAccount = function () {
        $location.url('/register');
      };


      $scope.forgotPassword = function (size) {
        var modalInstance = $modal.open({
          templateUrl: 'modules/register/modal-forgot-password.html',
          controller: 'ModalForgotPasswordController',
          windowClass: 'fade fill-in',
          backdrop: 'static',
          keyboard: false,
          size: size,
          resolve: {

          }
        });
        modalInstance.result.then(function (value) {
          if (value) {
            console.log('Email enviado com sucesso');
          }
        }, function () {

        });
      };

      var authentication = authService.authentication();
      if (authentication.isAuth) {
        $state.go('lite.enterprises',{redirectTo:redirectTo});
      }

    }]);
