;(function () {
  'use strict';

  angular
    .module('cloudmidiaApp')
    .controller('PasswordResetController', [
      '$scope',
      '$rootScope',
      '$state',
      'authService',
      'CloudmidiaApi',
      '$stateParams',
      'Exception',
      function ($scope, $rootScope, $state, authService,CloudmidiaApi,$stateParams,Exception) {
        'use strict';

        $scope.user = {
          userId: $stateParams.user,
          token: $stateParams.token
        };

        var redirect = function(url){
          $location.path(url);
          $location.url($location.path());
        };

        $scope.submitLoading = false;

        $scope.emailSend = false;

        function validateForm(){
          $scope.passwordForm.$setSubmitted();
          if($scope.passwordForm.$invalid){
            return true;
          }
        }

        $scope.reset = function(){

          if(validateForm()){
            return true;
          }

          $scope.submitLoading = true;
          CloudmidiaApi.ResetPasswordApi.save($scope.user,
            function success(data) {
              Exception.catcherSuccess(data).then(
                function success(result){
                  $scope.submitLoading = false;
                  if (result.successes[0].type === 'SUCCESS') {
                    $scope.emailSend = true;
                  }
                }
              )
            },
            function error(err){
              $scope.submitLoading = false;
              Exception.catcherError(err);
            });
        };
      }
    ]);
})();
