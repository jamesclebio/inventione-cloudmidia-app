angular.module('cloudmidiaApp')
  .controller('RegisterController',
  ['$scope',
    '$rootScope',
    '$state',
    '$location',
    '$stateParams',
    'CloudmidiaApi',
    'authService',
    'AppSettings',
    'InvitationService',
    'Exception',
    function ($scope, $rootScope,$state, $location, $stateParams, CloudmidiaApi, authService, AppSettings,InvitationService,Exception) {
      'use strict';

      $scope.user = {};
      $scope.alreadyExist = false;

      $scope.token = $stateParams.token;
      if(!$scope.token){
        $scope.token = $rootScope.invitationToken;
      }
      $scope.user.token = $scope.token;

      $scope.baseUrl = AppSettings.urlOrigin + 'p/';

      var redirect = function (url) {
        $location.path(url);
        $location.url($location.path());
      };

      function getInvitationInformation () {
        main.loaderPage.on();
        CloudmidiaApi.InvitationInformationApi.get({'token': $scope.token},
          function success(data) {
            $scope.invitedUser = data;
            if (data.userRegistred) {
              $scope.isUser = true;
              $scope.showInvitation = true;
              var authentication = authService.authentication();
              if (authentication.isAuth) {
                //if (!$rootScope.invitationToken)
                  $scope.acceptInviteUser();
              } else {
                main.loaderPage.off();
                $rootScope.invitationToken = $scope.token;
                $state.go('lite.login', {redirectTo: 'lite.register'});
              }
            }else{
              main.loaderPage.off();
            }
          });
      };

      if($scope.token)
        getInvitationInformation();

      $scope.acceptInviteUser = function () {

        InvitationService.acceptInvitation($scope.invitedUser.accountId,$scope.token).then(
          function success(data){
            main.loaderPage.off();
            Exception.catcherSuccess(data.data).then(function success(){
              $state.go('lite.login',{redirectTo:'app.account.companies'});
            });
          }).catch(function(message){
            main.loaderPage.off();
            Exception.catcherError(message,true);
          });
      };

      $scope.onBlur = function (data) {
        if(data !== undefined){
          var slug = formatData(data);
          $scope.isLoadingAvailabitily = true;
          CloudmidiaApi.EnterpriserApi.get({'slug': slug},
            function success(response) {
              $scope.isLoadingAvailabitily = false;
              if(response.id === undefined){
                $scope.slug = slug;
                $scope.alreadyExist = false;
              }else{
                $scope.slug = slug;
                $scope.alreadyExist = true;
              }
            },
            function error(err){
              Exception.catcherError(err);
            });
        }else{
          $scope.slug = undefined;
          $scope.alreadyExist = false;
        }
      };

      var formatData = function (palavra) {
        if (palavra !== undefined) {
          var comAcento = 'áàãâäéèêëíìîïóòõôöúùûüçÁÀÃÂÄÉÈÊËÍÌÎÏÓÒÕÖÔÚÙÛÜÇ´`^¨~';
          var semAcento = 'aaaaaeeeeiiiiooooouuuucAAAAAEEEEIIIIOOOOOUUUUC     ';
          for (var l = 0; l < palavra.length; l++) {
            for (var p = 0; p < comAcento.length; p++) {
              if (palavra[l] === comAcento[p]) {
                palavra = palavra.replace(palavra[l], semAcento[p]);
              }
            }
          }
          var lowerCase = palavra.toLowerCase();

          $scope.justSlug = lowerCase.replace(' ', '-');

          return lowerCase.replace(' ', '-');
        }
        else {
          return '';
        }
      };

      $scope.registerByInvitation = function(user){
        $scope.registerForm.$setSubmitted();
        if($scope.registerForm.lname.$invalid || $scope.registerForm.username.$invalid
        || $scope.registerForm.password.$invalid){
          return;
        }
        $scope.submitLoading = true;
        $scope.user.accountId = $scope.invitedUser.accountId;
        $scope.dataLogin = {'userName': $scope.invitedUser.email, 'password': $scope.user.password};
        InvitationService.acceptRegisterInvitation($scope.invitedUser.accountId,user).then(
          function success(response){
            if (response.data.messages[0].type === 'SUCCESS') {
              authService.login($scope.dataLogin).then(
                function success (response) {
                  $scope.submitLoading = false;
                  $state.go('lite.login',{redirectTo:'app.account.companies'});
                },
                function error(err){
                  $scope.submitLoading = false;
                  Exception.catcherError(err,true);
                }
              );
            } else {
              return;
            }
          }
        ).catch(function(message){
            $scope.submitLoading = false;
            Exception.catcherError(message,true);
          });
      }

      $scope.register = function (ownerOrAdvertiser) {
        $scope.registerForm.$setSubmitted();
        if($scope.registerForm.$invalid || $scope.alreadyExist){
          return;
        }
        $scope.submitLoading = true;
        $scope.user.slug = $scope.justSlug;
        $scope.user.ownerOrAdvertiser = ownerOrAdvertiser;
        CloudmidiaApi.RegisterApi.save($scope.user,
          function success(data) {
            $scope.submitLoading = false;
            Exception.catcherSuccess(data).then(
              function success(result){
                if (result.successes.length > 0 && result.successes[0].type === 'SUCCESS') {
                  redirect('/login');
                }
                else
                  return;
              }
            )
          },
          function error(err) {
            $scope.submitLoading = false;
            Exception.catcherError(err);
          });
      };

    }]);
