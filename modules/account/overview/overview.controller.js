;(function () {
  'use strict';

  angular
    .module('cloudmidiaApp')
    .controller('AccountOverviewController', AccountOverviewController);

  AccountOverviewController.$inject = [
    '$scope',
    '$state',
    '$stateParams',
    'authService',
    'ProviderService',
    'AccountService',
    'Exception',
  ];

  function AccountOverviewController($scope,$state, $stateParams, authService, ProviderService, AccountService,Exception) {

    var enterprise;
    var init = function(){
      main.loaderPage.on();
      enterprise = ProviderService.getCurrentEnterpriser();
      getUserInfoById();
    };

    var getUserInfoById = function(){
      if (authService.authentication().isAuth) {
        authService.profileInfoById(enterprise.id).then(
          function success(data) {
            $scope.profileInfo = data;
            getAllInformation(enterprise.id);
          }, function error(err) {
            Exception.catcherError(err, true);
          });
      }
    };

    var getAllInformation = function(){
      AccountService.getAllInformation(enterprise.id).then(
        function success(data){
          $scope.allInformation = data;
          main.loaderPage.off();
        },
        function error(err){
          main.loaderPage.off();
          Exception.catcherError(err, true);
        }
      );
    };

    init();
  }
})();
