;(function () {
  'use strict';

  angular
    .module('cloudmidiaApp')
    .controller('AccountUsersController', AccountUsersController);

  AccountUsersController.$inject = [
    '$scope',
    '$state',
    '$stateParams',
    'CloudmidiaApi',
    'authService',
    'InvitationService',
    'ProviderService',
    'Exception'
  ];

  function AccountUsersController($scope,$state, $stateParams, CloudmidiaApi, authService, InvitationService, ProviderService, Exception) {

    var enterprise;
    $scope.role = {owner:'Dono', adm:'Administrador'};

    var init = function(){
      enterprise = ProviderService.getCurrentEnterpriser();
      $scope.name = enterprise.name;
      userHasCompany();
      getUsers();
      getInvitations();
    }

    $scope.pageChanged = function (currentPage) {
      $scope.currentPage = currentPage;
      getUsers();
    };

    var getInvitations = function(){
      CloudmidiaApi.InvitationApi.query({accountId:enterprise.id},
        function success(data){
          $scope.invitations = data;
          $scope.showTableInvitations = true;
        },
        function error(error){
        }
      );
    };

    var userHasCompany =  function (){
      if(enterprise.role === $scope.role.owner || enterprise.role === $scope.role.adm){
        $scope.hasCompany = true;
        return;
      }
      $scope.hasCompany = false;
    };

    $scope.resendInvitation = function(invitationId){
      InvitationService.resendInvitationById(invitationId).then(
        function success(data){
          Exception.catcherSuccess(data.data).then(
            function success(result){
              getInvitations(enterprise.id);
            }
          )
        }
      ).catch(function(message){
          Exception.catcherError(message,false);
        });
    };

    $scope.cancelInvitation = function(invitationId){
      InvitationService.cancelInvitationById(invitationId).then(
        function success(data){
          Exception.catcherSuccess(data.data).then(
            function success(result){
              getInvitations(enterprise.id);
            }
          )
        }).catch(function(message){
          Exception.catcherError(message,false);
        });
    };

    var getUsers = function () {
      $scope.isLoadingAvailability = true;
      main.loaderPage.on();
      CloudmidiaApi.UsersApi.get({'accountId': enterprise.id,
          's':$scope.filterName,'page':$scope.currentPage,'pageSize':$scope.pageSize},
        function success(data) {
          main.loaderPage.off();
          $scope.isLoadingAvailability = false;
          $scope.quantity = data.items.length;
          $scope.pageSize = data.pageSize;
          $scope.showTable = data.totalItems > 0;
          $scope.showEmptyState = !$scope.showTable;
          var owner = _.find(data.items, function(role){return role.roles[0].name === 'Dono';});
          var index = _.indexOf(data.items,owner);
          $scope.data = data.items;
          $scope.data.splice(index,1);
          $scope.data.splice(0,0,owner);
        },
        function error(err) {
          Exception.catcherError(err,true);
        }
      );
    };

    init();
  }
})();
