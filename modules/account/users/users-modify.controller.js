;(function () {
  'use strict';

  angular
    .module('cloudmidiaApp')
    .controller('AccountUsersModifyController', AccountUsersModifyController);

  AccountUsersModifyController.$inject = [
    '$scope',
    '$q',
    '$state',
    'CloudmidiaApi',
    'ProviderService',
    'Exception',
  ];

  function AccountUsersModifyController($scope, $q, $state, CloudmidiaApi, ProviderService, Exception) {

      var enterpriser;
      var init = function () {
        enterpriser = ProviderService.getCurrentEnterpriser();
        $scope.enterpriserName = enterpriser.name;
        main.loaderPage.on();
        getRoles(enterpriser.id).then(function(data){
          main.loaderPage.off();
          $scope.allRoles = data;
        }).catch(function(message){
          exception.catcherError(message,true);
        });
      };

      var getRoles = function (accountId) {
        var deferred = $q.defer();
        CloudmidiaApi.RolesApi.query({accountId: accountId},
          function success(data) {
            deferred.resolve(data);
          }, function failure(error) {
            Exception.catcherError(error,true);
            deferred.reject(error);
          }
        );
        return deferred.promise;
      };

    $scope.checked = function(){
      return _.find($scope.allRoles, function(data){return data.checked === true})
    };

    $scope.select = function(role){
      _.map($scope.allRoles,function(data){ if(data.id === role){return data.checked=true;}data.checked=false;});
    }

    $scope.confirm = function(){
      $scope.invitationForm.$setSubmitted();
      var rolesChecked = _.filter($scope.allRoles, function(data){return data.checked === true;});
      if(rolesChecked.length === 0 || $scope.invitationForm.$invalid){
        return;
      }

      $scope.submitLoading = true;
      var user = {
        'accountId':enterpriser.id,
        'email':$scope.email,
        'message':$scope.message,
        'roles': _.pluck(rolesChecked, 'id')
      };
      CloudmidiaApi.InvitationApi.save(user,function success(data){
          $scope.submitLoading = false;
          Exception.catcherSuccess(data).then(
            function success(result){
              $state.go('app.account.users');
            }
          )
        },
        function error(err){
          Exception.catcherError(err,false);
          $scope.submitLoading = false;
        });

    };

    init();
  }
})();
