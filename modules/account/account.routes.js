;(function () {
  'use strict';

  angular
    .module('cloudmidiaApp')
    .config(config);

  config.$inject = [
    '$stateProvider'
  ];

  function config($stateProvider) {
    $stateProvider
      .state('app.account', {
        abstract: true,
        url: '/p/:enterpriserSlug/account',
        templateUrl: 'modules/account/account.html',
        controller: 'AccountController',
        data: {
          requireAuthentication: true
        },
        resolve: {
          deps: ['$ocLazyLoad', function ($ocLazyLoad) {
            return $ocLazyLoad
              .load([], {
                insertBefore: '#lazyload_placeholder'
              })
              .then(function () {
                return $ocLazyLoad.load([
                  'modules/account/account.controller.js'
                ]);
              });
          }]
        }
      })
      // Overview
      .state('app.account.overview', {
        url: '/overview',
        templateUrl: 'modules/account/overview/overview.html',
        controller: 'AccountOverviewController',
        controllerAs: 'vm',
        resolve: {
          deps: ['$ocLazyLoad', function ($ocLazyLoad) {
            return $ocLazyLoad
              .load([], {
                insertBefore: '#lazyload_placeholder'
              })
              .then(function () {
                return $ocLazyLoad.load([
                  'modules/account/overview/overview.controller.js',
                  'services/auth.service.js',
                  'services/provider.services.js',
                  'services/account.service.js'
                ]);
              });
          }]
        }
      })
      // Profile
      .state('app.account.profile', {
        url: '/profile',
        templateUrl: 'modules/account/profile/profile.html',
        controller: 'AccountProfileController',
        controllerAs: 'vm',
        resolve: {
          deps: ['$ocLazyLoad', function ($ocLazyLoad) {
            return $ocLazyLoad
              .load([], {
                insertBefore: '#lazyload_placeholder'
              })
              .then(function () {
                return $ocLazyLoad.load([
                  'modules/account/profile/profile.controller.js',
                  'services/auth.service.js',
                  'services/provider.services.js',
                  'services/account.service.js',
                  'directives/password-check.js'
                ]);
              });
          }]
        }
      })

      // Companies
      .state('app.account.companies', {
        url: '/companies',
        templateUrl: 'modules/account/companies/companies.html',
        controller: 'AccountCompaniesController',
        controllerAs: 'vm',
        resolve: {
          deps: ['$ocLazyLoad', function ($ocLazyLoad) {
            return $ocLazyLoad
              .load([], {
                insertBefore: '#lazyload_placeholder'
              })
              .then(function () {
                return $ocLazyLoad.load([
                  'modules/account/companies/companies.controller.js',
                  'services/auth.service.js',
                  'services/cloudmidia-api.service.js',
                  'services/provider.services.js',
                  'services/account.service.js'
                ]);
              });
          }]
        }
      })
      .state('app.account.users',{
        url:'/users',
        template:'<ui-view></ui-view>',
        data: {
          requireAuthentication: true
        }
      })

      // Users
      .state('app.account.users.list', {
        url: '/list',
        templateUrl: 'modules/account/users/users.html',
        controller: 'AccountUsersController',
        controllerAs: 'vm',
        resolve: {
          deps: ['$ocLazyLoad', function ($ocLazyLoad) {
            return $ocLazyLoad
              .load([], {
                insertBefore: '#lazyload_placeholder'
              })
              .then(function () {
                return $ocLazyLoad.load([
                  'modules/account/users/users.controller.js',
                  'services/auth.service.js',
                  'services/provider.services.js',
                  'services/invitation.service.js',
                  'services/cloudmidia-api.service.js'
                ]);
              });
          }]
        }
      })

      .state('app.account.users.invite', {
        url: '/invite',
        templateUrl: 'modules/account/users/users-modify.html',
        controller: 'AccountUsersModifyController',
        controllerAs: 'vm',
        resolve: {
          deps: ['$ocLazyLoad', function ($ocLazyLoad) {
            return $ocLazyLoad
              .load([
                'underscore'
              ], {
                insertBefore: '#lazyload_placeholder'
              })
              .then(function () {
                return $ocLazyLoad.load([
                  'modules/account/users/users-modify.controller.js',
                  'services/auth.service.js',
                  'services/provider.services.js',
                  'services/invitation.service.js',
                  'services/cloudmidia-api.service.js'
                ]);
              });
          }]
        }
      })

      .state('app.account.usersUpdate', {
        url: '/users/:id/update',
        templateUrl: 'modules/account/users/users-modify.html',
        controller: 'AccountUsersModifyController',
        controllerAs: 'vm',
        resolve: {
          deps: ['$ocLazyLoad', function ($ocLazyLoad) {
            return $ocLazyLoad
              .load([], {
                insertBefore: '#lazyload_placeholder'
              })
              .then(function () {
                return $ocLazyLoad.load([
                  'modules/account/users/users-modify.controller.js'
                ]);
              });
          }]
        }
      })

      // Billing
      .state('app.account.billing', {
        url: '/billing',
        templateUrl: 'modules/account/billing/billing.html',
        controller: 'AccountBillingController',
        controllerAs: 'vm',
        resolve: {
          deps: ['$ocLazyLoad', function ($ocLazyLoad) {
            return $ocLazyLoad
              .load([], {
                insertBefore: '#lazyload_placeholder'
              })
              .then(function () {
                return $ocLazyLoad.load([
                  'modules/account/billing/billing.controller.js',
                  'services/auth.service.js',
                  'services/campaigns.service.js',
                  'services/provider.services.js',
                  'services/account.service.js'
                ]);
              });
          }]
        }
      });
  }
}());
