;(function () {
  'use strict';

  angular
    .module('cloudmidiaApp')
    .controller('AccountCompaniesController', AccountCompaniesController);

  AccountCompaniesController.$inject = [
    '$scope',
    'AppSettings',
    'authService',
    'CloudmidiaApi',
    'AccountService',
    'ProviderService',
    '$stateParams',
    'Exception'
  ];

  function AccountCompaniesController($scope,AppSettings,authService,CloudmidiaApi,AccountService,ProviderService,Exception) {

    var enterprise;
    $scope.hasCompany = false;
    $scope.role = {adm:'Dono'};
    $scope.url = AppSettings.urlApi;
    var init  = function(){
      enterprise = ProviderService.getCurrentEnterpriser();
      getUserInfo();
    }

    var userHasCompany =  function (){
      if(_.find($scope.enterprises, function(data){return data.role === $scope.role.adm;})){
        $scope.hasCompany = true;
        return;
      }
      $scope.hasCompany = false;
    }

    var getUserInfo = function () {
      if (authService.authentication().isAuth) {
        main.loaderPage.on();
        authService.profileInfo().then(
          function success(data) {
            $scope.profileInfo = data;
            getEnterprises();
          },
          function error(err){
            Exception.catcherError(err);
          })
      }
    };

    var getEnterprises = function () {
      AccountService.getEnterprises($scope.profileInfo.userName).then(
        function success(data) {
          main.loaderPage.off();
          ProviderService.saveAllEnterpriser(data);
          var notOwnerEnterprises = _.sortBy(_.reject(data,function(item){return item.role === 'Dono'}),'name');
          var OwnerEnterprises = _.sortBy(_.reject(data,function(item){return item.role !== 'Dono'}),'name');
          $scope.enterprises =_.union(OwnerEnterprises,notOwnerEnterprises);
          userHasCompany();
        },
        function error(err){
          Exception.catcherError(err);
        }
      )
    };

    init();
  }
})();
