;(function () {
  'use strict';

  angular
    .module('cloudmidiaApp')
    .controller('AccountController', AccountController);

  AccountController.$inject = [
    '$scope',
    '$state',
    '$stateParams',
    'Authorization',
    'localStorageService'
  ];

  function AccountController($scope,$state, $stateParams,Authorization, localStorageService) {
    $scope.authorized = function(){
      var permission = localStorageService.get('permissions');
      if(permission){
        return Authorization.authorized('ListUser','user',permission.data);
      }
    }
  }
})();
