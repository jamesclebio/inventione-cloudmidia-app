;(function () {
  'use strict';

  angular
    .module('cloudmidiaApp')
    .controller('AccountProfileController', AccountProfileController);

  AccountProfileController.$inject = [
    '$scope',
    '$rootScope',
    '$q',
    'AppSettings',
    'Upload',
    'AccountService',
    'authService',
    'ProviderService',
    'Exception'
  ];

  function AccountProfileController($scope,$rootScope,$q,AppSettings,Upload,AccountService,authService,ProviderService,Exception) {

    var enterprise;

    $scope.uploadPattern = 'image/*';

    var init = function(){
      main.loaderPage.on();
      $scope.user = {};
      $scope.user.currentPassword = '';
      $scope.user.newPassword = '';
      $scope.user.confirmPassword = '';
      enterprise = ProviderService.getCurrentEnterpriser();
      getUserInfoById();
    };

    var getUserInfoById = function(){
      if (authService.authentication().isAuth) {
        authService.profileInfoById(enterprise.id).then(
          function success(data) {
            main.loaderPage.off();
            $scope.profile = data;
          }, function error(err) {
            main.loaderPage.off();
            Exception.catcherError(err, true);
          });
      }
    };

    function calcThumbSize(videoWidth,videoHeight) {
      var maxWidth = 140;
      var maxHeight = 105;
      var ratio = 0;
      var width = videoWidth;
      var height = videoHeight;
      if(videoWidth <= maxWidth && videoHeight <= maxHeight){
        return {'width': width, 'height': height};
      }else{
        if (width > height && width > maxWidth) {
          ratio = maxWidth / width;
          height = height * ratio;
          width = width * ratio;
        } else {
          ratio = maxHeight / height;
          width = width * ratio;
          height = height * ratio;
        }
        return {'width': width, 'height': height};
      }
    }

    var resize = function (file) {
      var deferred = $q.defer();
      var URL = window.URL || window.webkitURL;
      var imgUrl = URL.createObjectURL(file);
      var canvas = document.getElementById('canvas');

      var ctx = canvas.getContext('2d');
      var resizedFileUrl;

      var img = new Image();
      img.src = imgUrl;
      img.setAttribute('id',imgUrl);
      img.onload = function () {
        var newSize = calcThumbSize(img.width,img.height);
        canvas.width = newSize.width;
        canvas.height = newSize.height;
        ctx.drawImage(img, 0, 0, newSize.width, newSize.height);
        resizedFileUrl = canvas.toDataURL('image/jpeg');
        console.log(resizedFileUrl);
        ctx.clearRect(0,0,newSize.width,newSize.height);
        deferred.resolve(resizedFileUrl);
      };

      return deferred.promise;
    };

    // upload on file select or drop
    function doneUploadAll() {
      $scope.isLoading = false;
    }

    $scope.accountUploadMedia = function (files) {
      $scope.files = files;
      $scope.isLoading = true;
      var finishUploadCallback = _.after(files.length, doneUploadAll);
      _.each(files, function (file) {
        resize(file).then(function success(thumb) {
          uploadMedia(file, thumb, finishUploadCallback);
        }).catch(function(message){
          Exception.catcherError(message,true);
        });
      });
    };

    var uploadMedia = function (file, thumb, finishUploadCallback) {

      var url = AppSettings.urlApi + 'accounts/' + enterprise.id + '/users/'+ $scope.profile.userName+'/media';
      $scope.isLoadingImage = true;
      file.upload = Upload.upload({
        url: url,
        data: {file: [file, Upload.dataUrltoBlob(thumb)]}
      });
      file.upload
        .success(function (resp) {
          $scope.isLoadingImage = false;
          $scope.selectedMediaId = resp.id;
          $scope.profile.userProfileImageUrl= resp.thumbUrl;
          $rootScope.$broadcast('imageUpdatedEvent');
          file.isSuccess = true;
          finishUploadCallback();
        })
        .error(function (resp) {
          file.isError = true;
          if (resp.status > 0) {
            $scope.errorMsg = true;
          }
        })
        .progress(function (evt) {
          file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
        });
    };

    function validateForm(){
      $scope.profileForm.$setSubmitted();
      if($scope.profileForm.$invalid){
        main.scrollTop.run();
        return true;
      }
      return false;
    }

    $scope.validate = function(password){
      return !/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&'])[^ ]{5,}$/.test(password);
    };

    $scope.passwordUpdate = function(user){
      if($scope.passwordForm.$invalid || validateForm()){
        return;
      }
      $scope.isLoadingAvailabitily = true;
      AccountService.passwordUpdate(user).then(
        function(data){
          $scope.isLoadingAvailabitily = false;
          Exception.catcherSuccess(data).then(
            function success(result){
              init();
              $scope.passwordForm.$setPristine();
            }
          )
        }).catch(function(message){
          Exception.catcherError(message);
          $scope.isLoadingAvailabitily = false;
        });
    };

    $scope.accountUpdate = function(profile){
      if(validateForm()){
        return;
      }
      $scope.isLoadingAvailabitily = true;
      AccountService.userUpdate(profile).then(
        function success(data){
          $scope.isLoadingAvailabitily = false;
          Exception.catcherSuccess(data).then(
            function success(result){
              init();
            }
          )
        },
        function error(err){
          console.log(err);
        }).catch(function(message){
          Exception.catcherError(message);
          $scope.isLoadingAvailabitily = false;
        });
    }

    init();
  }
})();
