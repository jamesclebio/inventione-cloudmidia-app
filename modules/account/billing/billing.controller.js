;(function () {
  'use strict';

  angular
    .module('cloudmidiaApp')
    .controller('AccountBillingController', BillingController);

  BillingController.$inject = [
    '$scope',
    'CampaignService',
    'ProviderService',
    'Exception'
  ];

  function BillingController($scope,CampaignService,ProviderService,Exception) {

    var enterprise= ProviderService.getCurrentEnterpriser();
    var init = function(){
      getBilling();
    };

    var getBilling = function(){
      main.loaderPage.on();
      CampaignService.getBilling(enterprise.id).then(
        function success(data){
          $scope.billings = data;
          main.loaderPage.off();
        },
        function error(err){
          Exception.catcherError(err);
        }
      )
    };

    $scope.calcPrice = function(data){
      var total = 0;
      angular.forEach(data, function(value){
        total += value.boosting * value.price;
      });
      return total;
    };

    init();
  }
})();
