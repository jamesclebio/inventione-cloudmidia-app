// Karma configuration
// Generated on Thu Oct 15 2015 12:11:42 GMT-0300 (E. South America Standard Time)

module.exports = function(config) {
  config.set({

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '',


    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['jasmine'],


    // list of files / patterns to load in the browser
    files: [
        'assets/plugins/angular/angular.js',
        'assets/plugins/angular-mocks/angular-mocks.js',
        'assets/plugins/angular-ui-router/release/angular-ui-router.min.js',
        'assets/plugins/angular-ui-util/ui-utils.js',
        'assets/plugins/angular-ui-bootstrap/ui-bootstrap-0.13.1.js',
        'assets/plugins/angular-oc-lazyload/ocLazyLoad.min.js',
        'assets/plugins/angular-resource/angular-resource.min.js',
        'assets/plugins/ng-file-upload/ng-file-upload.min.js',
        'assets/plugins/angular-messages/angular-messages.js',
        'assets/plugins/ng-table/dist/ng-table.js',
        'assets/plugins/angular-local-storage/dist/angular-local-storage.min.js',
        'assets/plugins/checklist-model/checklist-model.js',
        'assets/plugins/angular-google-maps/dist/angular-google-maps.js',
        'assets/plugins/angular-simple-logger/dist/angular-simple-logger.js',
        'assets/plugins/angular-google-places-autocomplete/dist/autocomplete.min.js',
        'assets/plugins/ladda/dist/ladda.min.js',
        'assets/plugins/angular-ladda/dist/angular-ladda.js',
        'block/logger.module.js',
        'block/logger.js',
        'block/exception.module.js',
        'block/exception.js',
        'block/exception-handler.provider.js',
        'app.js',
        'main.js',
        'services/*.js',
        'modules/**/*.js',
        'test/services/devices.service.test.js',
        'test/campaign/campaign.controller.test.js'
    ],


    // list of files to exclude
    exclude: [
    ],


    // preprocess matching files before serving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {
    },


    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: ['progress','html'],

    htmlReporter: {
      outputDir: 'app/test/report', // where to put the reports
      templatePath: null, // set if you moved jasmine_template.html
      focusOnFailures: true, // reports show failures on start
      namedFiles: false, // name files instead of creating sub-directories
      pageTitle: null, // page title for reports; browser info by default
      urlFriendlyName: false, // simply replaces spaces with _ for files/dirs
      reportName: 'report-summary-filename', // report summary filename; browser info by default


      // experimental
      preserveDescribeNesting: false, // folded suites stay folded
      foldAll: false, // reports start folded (only with preserveDescribeNesting)
    },

    // web server port
    port: 9876,


    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,


    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: ['Chrome'],


    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: false
  })
}
