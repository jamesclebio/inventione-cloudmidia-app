;(function () {
  'use strict';

  // Service base definition
  window.serviceBase = (function () {
    var hosts = {
      production: {
        regex: /cloudmidia\.com/,
        set: {
          urlApi: 'http://api.cloudmidia.com/api/',
          urlOrigin: 'http://cloudmidia.com/',
        }
      },

      development: {
        regex: /cmfront\.azurewebsites\.net/,
        set: {
          urlApi: 'http://cmapi.azurewebsites.net/api/',
          urlOrigin: 'http://cmfront.azurewebsites.net/',
        }
      },

      local: {
        regex: /localhost/,
        port: '3000',
        set: {
          urlApi: 'http://localhost:61110/api/',
          urlOrigin: 'http://localhost:3000/',
        }
      },

      localRemote: {
        regex: /localhost/,
        port: '37374',
        set: {
          urlApi: 'http://cmapi.azurewebsites.net/api/',
          urlOrigin: 'http://localhost:37374/',
        }
      },
    };

    for (var host in hosts) {
      if (hosts[host].regex.test(window.location.hostname)) {

        if (!hosts[host].port) {
          return hosts[host].set;
        }

        if (hosts[host].port === window.location.port) {
          return hosts[host].set;
        }
      }
    }
  })();

  // App
  angular
    .module('cloudmidiaApp')
    .config([
      '$stateProvider',
      '$urlRouterProvider',
      '$ocLazyLoadProvider',
      '$resourceProvider',
      '$httpProvider',
      '$authProvider',
      function ($stateProvider, $urlRouterProvider, $ocLazyLoadProvider, $resourceProvider, $httpProvider, $authProvider) {
        $urlRouterProvider.otherwise('/home');

        //window.serviceBase.urlApi
        $authProvider.instagram({
          clientId: '5ac7901719224b9ca52c7dec43f10f12',
          name: 'instagram',
          url: '',
          authorizationEndpoint: 'https://api.instagram.com/oauth/authorize',
          redirectUri:'http://cloudmidia.com/',
          requiredUrlParams: ['scope'],
          scope: ['public_content'],
          scopeDelimiter: '+',
          type: '2.0'
        });

        $stateProvider
          .state('app', {
            abstract: true,
            templateUrl: 'layouts/default.html'
          })

          .state('lite', {
            abstract: true,
            templateUrl: 'layouts/lite.html'
          });

        $resourceProvider.defaults.cache = false;

        $httpProvider.interceptors.push('MessagesInterceptor');
        $httpProvider.interceptors.push('authInterceptorService');
      }
    ])
    .constant('AppSettings', window.serviceBase);
})();
