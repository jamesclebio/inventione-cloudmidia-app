(function () {
  'use strict';

  angular
    .module('block.exception')
    .factory('Exception', exception);

  exception.$inject = ['logger', '$state','MessagesInterceptor','$q'];

  /* @ngInject */
  function exception(logger, $state, MessagesInterceptor, $q) {
    var service = {
      catcherError: catcherError,
      catcherSuccess: catchSuccess
    };
    return service;

    function catchSuccess(success, showSuccessNotification, showErrorNotification, showInfoNotification, showWarningNotification)
    {
      var deferred = $q.defer();
      showSuccessNotification = typeof showSuccessNotification === 'undefined' ? true : showSuccessNotification;
      showErrorNotification = typeof showErrorNotification === 'undefined' ? true : showErrorNotification;
      showInfoNotification = typeof showInfoNotification === 'undefined' ? true : showInfoNotification;
      showWarningNotification = typeof showWarningNotification === 'undefined' ? true : showWarningNotification;

      var response = {
        successes: [],
        errors: [],
        infos: [],
        warnings: []
      }
      var message = {
        success: "",
        error: "",
        info: "",
        warning: ""
      };

      if (success.messages) {
        for (var i = 0; i < success.messages.length; i++) {
          switch ((success.messages[i].type).toUpperCase()) {
            case 'SUCCESS':
              response.successes.push(success.messages[i]);
              break;
            case 'ERROR':
              response.errors.push(success.messages[i]);
              break;
            case 'INFO':
              response.infos.push(success.messages[i]);
              break;
            case 'WARNING':
              response.warnings.push(success.messages[i]);
              break;
          }
        }
      }

      for (var i = 0; i < response.successes.length; i++) {
        message.success = response.successes[i].description;
      }

      for (var i = 0; i < response.errors.length; i++) {
        message.error = response.errors[i].description;
      }
      for (var i = 0; i < response.infos.length; i++) {
        message.info = response.infos[i].description;
      }
      for (var i = 0; i < response.warnings.length; i++) {
        message.warning = response.warnings[i].description;
      }

      if (showSuccessNotification && message.success) {
        MessagesInterceptor.success(message.success);
      }
      if (showErrorNotification && message.error) {
        MessagesInterceptor.error(message.error);
      }
      if (showInfoNotification && message.info) {
        MessagesInterceptor.info(message.info);
      }
      if (showWarningNotification && message.warning) {
        MessagesInterceptor.warning(message.warning);
      }

      if (response.successes.length || response.errors.length || response.infos.length ) {
        deferred.resolve(response);
      } else {
        deferred.reject(response);
      }

      return deferred.promise;

    }

    function catcherError(error, redirect) {
        if (!redirect) {
          switch (error.status) {
            case 0:
              logger.error('Error no servidor.');
              break;
            case 401:
              logger.error('Recurso não encontrado.');
              break;
            case 403:
              logger.error('Recurso não encontrado.');
              break;
            case 404:
              logger.error('Recurso não encontrado.');
              break;
            case 500:
              logger.error('Erro no servidor.');
              break;
            default:
              if(error.error === "2006")
                logger.error(error.error_description);
              else
                logger.error('Recurso indisponível no momento.');
              break;
          }
        }else{
          switch (error.status) {
            case 0:
              $state.go('app.internal-server');
              break;
            case 401:
              $state.go('app.not-found');
              break;
            case 403:
              $state.go('app.not-found');
              break;
            case 404:
              $state.go('app.not-found');
              break;
            case 500:
              $state.go('app.internal-server');
              break;
            default:
              $state.go('app.internal-server');
              break;
          }
        }

    }
  };
}());
