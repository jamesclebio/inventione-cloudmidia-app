(function() {
  'use strict';

  angular
    .module('block.logger')
    .factory('logger', logger);

  logger.$inject = ['$log'];

  function logger($log) {
    var service = {
      showToasts: true,

      error   : error,
      info    : info,
      success : success,
      warning : warning,

      // straight to console; bypass toastr
      log     : $log.log
    };

    var notificationOption = {
      style: 'flip',
      message: '',
      position: 'top-left',
      timeout: 5000,
      type: 'success'
    };

    function showMessage(option, element) {
      $(element || 'body').pgNotification(option).show();
    }

    var toastr = {
      error: function (message, elementId) {
        notificationOption.type = 'error';
        notificationOption.message = message;
        showMessage(notificationOption, elementId);
      },
      success: function (message, elementId) {
        notificationOption.type = 'success';
        notificationOption.message = message;
        showMessage(notificationOption, elementId);
      },
      warning: function (message, elementId) {
        notificationOption.type = 'warning';
        notificationOption.message = message;
        showMessage(notificationOption, elementId);
      },
      info: function (message, elementId) {
        notificationOption.type = 'info';
        notificationOption.message = message;
        showMessage(notificationOption, elementId);
      }
    };

    return service;
    /////////////////////

    function error(message, data, title) {
      toastr.error(message);
      //$log.error('Error: ' + message, data);
    }

    function info(message, data, title) {
      $log.info('Info: ' + message, data);
    }

    function success(message, data, title) {
      $log.info('Success: ' + message, data);
    }

    function warning(message, data, title) {
      $log.warn('Warning: ' + message, data);
    }
  }
}());
