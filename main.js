angular.module('cloudmidiaApp')
  .controller('AppCtrl', [
    '$scope',
    '$rootScope',
    '$state',

    function ($scope, $rootScope, $state) {
      $scope.app = {
        name: 'Cloudmidia',
        layout: {
          menuPin: false,
          menuBehind: false
        }
      };

      // Checks if the given state is the current state
      $scope.is = function (name) {
        return $state.is(name);
      };

      // Checks if the given state/child states are present
      $scope.includes = function (name) {
        return $state.includes(name);
      };

      // Broadcasts a message to pgSearch directive to toggle search overlay
      $scope.showSearchOverlay = function () {
        $scope.$broadcast('toggleSearchOverlay', {
          show: true
        });
      };

      // History back
      $scope.$back = function() {
        window.history.back();
      };
    }
  ]);

angular.module('cloudmidiaApp')

  // Use this directive together with ng-include to include a template file by replacing the placeholder element
  .directive('includeReplace', function() {
    return {
      require: 'ngInclude',
      restrict: 'A',

      link: function(scope, el, attrs) {
        el.replaceWith(el.children());
      }
    };
  });
