angular.module('cloudmidiaApp').factory('MapService', ['$resource', '$q', 'AppSettings', '$window', '$http',
  function ($resource, $q, AppSettings, $window, $http) {

    return {
      getCoordinates: function (address) {
        var deferred = $q.defer();
        var url = 'http://maps.google.com/maps/api/geocode/json?address={address}&sensor=false'.replace('{address}',address);

        $http({
          url:url,
          method:'GET',
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          }
        }).success(function(data){
          deferred.resolve(data);
        });
        return deferred.promise;
      }
    };
  }]);
