/**
 * Created by Admin on 6/28/2016.
 */
(function(){
  'use strict';

  angular.module('cloudmidiaApp')
    .factory('InstagramService',['$q','$resource','AppSettings','$http',
      function($q,$resource,AppSettings,$http){
      var authorizationResult = false;

      var authenticateInstagram = $resource(AppSettings.urlApi + 'accounts/:accountId/instagram/:code',
        {accountId: '@accountId', code:'@code'}, {
          update: {method: 'PUT'}
        });

      var verifyAuthentication = $resource(AppSettings.urlApi + 'accounts/:accountId/instagram/verifyauthentication',
        {accountId:'@accountId'},{});

      return{
        authenticate: function(accountId,code){
          var deferred = $q.defer();
           authenticateInstagram.save({accountId:accountId,code:code},
            function success(data){
              deferred.resolve(data);
            },
            function error(err){
              deferred.reject(err);
            })
          return deferred.promise;
        },
        getImage: function(){
          var deferred = $q.defer();
          var url = 'https://api.instagram.com/v1/tags/nofilter/media/recent?access_token=3478601288.e1859ca.80e01a6f8f8141ffb07d4f1fe738ac23';
          $http.get(url).then(
            function success(data){
              console.log(data);
              deferred.resolve(data);
            },
            function error(err){
              deferred.reject(err);
            }
          );
          return deferred.promise;
        },
        verifyAuthentication : function(accountId, accessToken){
          var deferred = $q.defer();
          verifyAuthentication.get({accountId:accountId,accessToken:accessToken},
            function success(data){
              deferred.resolve(data);
            },
            function error(err){
              deferred.reject(err);
            })
          return deferred.promise;
        }
      }
    }])
}());
