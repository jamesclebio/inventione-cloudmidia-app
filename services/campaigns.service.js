'use strict';
angular.module('cloudmidiaApp')
  .factory('CampaignService', ['$resource',
    '$q',
    'AppSettings',
    function ($resource, $q, AppSettings) {

      var resourceContents = $resource(
        AppSettings.urlApi + 'accounts/:accountId/campaigns/:campaignId',
        {accountId: '@accountId', campaignId: '@campaignId'},
        {update: {method: 'PUT'}}
      );

      var resourcePaidCampaign = $resource(
        AppSettings.urlApi + 'accounts/:accountId/campaigns/:campaignId/paidcampaign',
        {accountId: '@accountId', campaignId: '@campaignId'},
        {update: {method: 'PUT'}}
      );

      var resourceVerifyCampaign = $resource(
        AppSettings.urlApi + 'accounts/:accountId/campaigns/:campaignId/completedcampaign',
        {accountId: '@accountId', campaignId: '@campaignId'},
        {update: {method: 'PUT'}}
      );

      var resourceExecutePayment = $resource(
        AppSettings.urlApi + 'accounts/:accountId/campaigns/:campaignId/executepayment',
        {accountId:'@accountId', campaignId:'@campaignId'},
        {upadate:{method:'PUT'}}
      );

      var resourceLocalContents = $resource(
        AppSettings.urlApi + 'accounts/:accountId/campaigns/:campaignId/local',
        {accountId: '@accountId', campaignId: '@campaignId'},
        {update: {method: 'PUT'}}
      );

      var resourceBillings = $resource(
        AppSettings.urlApi + 'accounts/:accountId/campaigns/billing',
        {accountId:'@accountId'},
        {update:{method:'PUT'}}
      );

      var resourceBasicContents = $resource(
        AppSettings.urlApi + 'accounts/:accountId/campaigns/:campaignId/basic',
        {accountId: '@accountId', campaignId: '@campaignId'},
        {update: {method: 'PUT'}}
      );
      var resourceMediaContents = $resource(
        AppSettings.urlApi + 'accounts/:accountId/campaigns/:campaignId/content',
        {accountId: '@accountId', campaignId: '@campaignId'},
        {update: {method: 'PUT'}}
      );
      var resourceCreateEmptyCampaign = $resource(
        AppSettings.urlApi + 'accounts/:accountId/campaignempty',
        {accountId: '@accountId'},
        {update: {method: 'PUT'}}
      );
      var resourceInterruptCampaign = $resource(
        AppSettings.urlApi + 'accounts/:accountId/campaigns/:campaignId/interrupt',
        {accountId:'@accountId', campaignId:'@campaignId'},
        {update:{method:'PUT'}}
      );
      var resourcePublishCampaign = $resource(
        AppSettings.urlApi + 'accounts/:accountId/campaigns/:campaignId/publish',
        {accountId:'@accountId', campaignId:'@campaignId'},
        {update:{method:'PUT'}}
      );

      var resourceValidCampaigns = $resource(
        AppSettings.urlApi + 'accounts/:accountId/campaigns/valueofvalidcampaigns',
        {accountId:'@accountId'},
        {update:{method:'PUT'}}
      );

      var resourceOnlyMyLocalsInCampaigns = $resource(
        AppSettings.urlApi + 'accounts/:accountId/campaigns/:campaignId/isonlymylocals',
        {accountId:'@accountId', campaignId:'@campaignId'},
        {update:{method:'PUT'}}
      );

      var resourceCloneCampaigns = $resource(
        AppSettings.urlApi + 'accounts/:accountId/campaigns/:campaignId/clone',
        {accountId:'@accountId', campaignId:'@campaignId'},
        {update:{method:'PUT'}}
      );

      return {
        createEmptyCampaign: function(accountId){
          var deferred = $q.defer();
          resourceCreateEmptyCampaign.save({'accountId':accountId},
            function(data){
              deferred.resolve(data);
            },
            function(error){
              deferred.reject(error);
            });
          return deferred.promise;
        },
        isOnlyMyLocalsInCampaign: function(accountId, campaignId){
          var deferred = $q.defer();
          resourceOnlyMyLocalsInCampaigns.get({'accountId':accountId,'campaignId':campaignId},
            function(data){
              deferred.resolve(data);
            },
            function(error){
              deferred.reject(error);
            });
          return deferred.promise;
        },
        cloneCampaign: function(accountId, campaignId){
          var deferred = $q.defer();
          resourceCloneCampaigns.save({'accountId':accountId,'campaignId':campaignId},
            function(data){
              deferred.resolve(data);
            },
            function(error){
              deferred.reject(error);
            });
          return deferred.promise;
        },
        verifyCampaign: function(accountId, campaignId){
          var deferred = $q.defer();
          resourceVerifyCampaign.get({accountId: accountId, campaignId: campaignId},
            function (data) {
              deferred.resolve(data);
            },
            function (error) {
              deferred.reject(error);
            }
          );
          return deferred.promise;
        },
        getQuantityOfValidCampaigns: function(accountId){
          var deferred = $q.defer();
          resourceValidCampaigns.get({accountId:accountId},
            function(data){
              deferred.resolve(data);
            },
            function (error){
              deferred.reject(error);
            });
          return deferred.promise;
        },
        getPaidCampaign: function(accountId, campaignId){
          var deferred = $q.defer();
          resourcePaidCampaign.get({accountId: accountId, campaignId: campaignId},
            function (data) {
              deferred.resolve(data);
            },
            function (error) {
              deferred.reject(error);
            }
          );
          return deferred.promise;
        },
        getBasicCampaign: function(accountId, campaignId){
          var deferred = $q.defer();

          resourceBasicContents.get({accountId: accountId, campaignId: campaignId},
            function (data) {
              deferred.resolve(data);
            },
            function (error) {
              deferred.reject(error);
            }
          );
          return deferred.promise;
        },
        executePayment: function(accountId,campaignId,data){
          var deferred = $q.defer();
          resourceExecutePayment.get({accountId: accountId, campaignId: campaignId, paymentId:data.paymentId,
            token:data.token, PayerID:data.PayerID},
            function (data) {
              deferred.resolve(data);
            },
            function (error) {
              deferred.reject(error);
            }
          );
          return deferred.promise;
        },
        getContentCampaign: function(accountId,campaignId){
          var deferred = $q.defer();
          resourceMediaContents.get({accountId: accountId, campaignId: campaignId},
            function (data) {
              deferred.resolve(data);
            },
            function (error) {
              deferred.reject(error);
            }
          );
          return deferred.promise;
        },
        getLocalCampaign: function(accountId,campaignId){
          var deferred = $q.defer();
          resourceLocalContents.query({accountId:accountId,campaignId:campaignId},
            function (data){
              deferred.resolve(data);
            },
            function (error){
              deferred.reject(error);
            });
          return deferred.promise;
        },
        getCampaigns: function (accountId, searchedName, page, pageSize, sort, privateCamp) {
          var deferred = $q.defer();

          resourceContents.get({'accountId': accountId, 's': searchedName, 'page': page, 'pageSize': pageSize,'srt':sort,
            'privateCampaigns':privateCamp},
            function (data) {
              deferred.resolve(data);
            },
            function (error) {
              deferred.reject(error);
            });

          return deferred.promise;
        },
        interruptCampaign: function(accountId,campaignId){
          var deferred = $q.defer();
          resourceInterruptCampaign.save({accountId:accountId,campaignId:campaignId},
            function(data){
              deferred.resolve(data);
            },
            function(error){
              deferred.reject(error);
            });
          return deferred.promise;
        },
        publishCampaign: function(accountId,campaignId){
          var deferred = $q.defer();
          resourcePublishCampaign.save({accountId:accountId,campaignId:campaignId},
            function(data){
              deferred.resolve(data);
            },
            function(error){
              deferred.reject(error);
            });
          return deferred.promise;
        },
        getBilling: function(accountId){
          var deferred = $q.defer();

          resourceBillings.query({accountId:accountId},
            function(data){
              deferred.resolve(data);
            },
            function(error){
              deferred.reject(error);
            });
          return deferred.promise;
        },
        getCampaign: function (accountId, campaignId) {
          var deferred = $q.defer();

          resourceContents.get({accountId: accountId, campaignId: campaignId},
            function (data) {
              deferred.resolve(data);
            },
            function (error) {
              deferred.reject(error);
            }
          );
          return deferred.promise;
        },
        excludeCampaign: function (accountId, id) {
          var deferred = $q.defer();
          resourceContents.delete({accountId: accountId, campaignId: id},
            function (data) {
              deferred.resolve(data);
            },
            function (error) {
              deferred.reject(error);
            }
          );
          return deferred.promise;
        },
        updateCampaign: function (campaign) {
          var deferred = $q.defer();
          resourceContents.update(campaign,
            function (data) {
              deferred.resolve(data);
            },
            function (error) {
              deferred.reject(error);
            });
          return deferred.promise;
        }
      };
  }]);
