angular.module('cloudmidiaApp')
  .factory('GroupsService', ['$resource', '$q', 'AppSettings','exception',
    function ($resource, $q, AppSettings, exception) {

      'use strict';

      var resourceGroupDevices = $resource(
        AppSettings.urlApi + 'accounts/:accountId/groups/:groupId',
        {accountId: '@accountId', groupId:'@groupId'},{update:{method:'PUT'}}
      );

      var resourceGroups = $resource(AppSettings.urlApi + 'accounts/:accountId/groupsbypage',
        {accountId:'@accountId'}
      );

      return {

        getGroups: function (accountId) {
          var deferred = $q.defer();

          resourceGroupDevices.query({accountId:accountId},
            function (data) {
              deferred.resolve(data);
            },
            function (err) {
              exception.catcher('')(err,true);
              deferred.reject(error);
            });

          return deferred.promise;
        }, // End of getGroups

        getGroupsByPage: function(accountId, searchedName, page, pageSize){
          var deferred = $q.defer();

          resourceGroups.get({'accountId': accountId, 's': searchedName, 'page': page, 'pageSize': pageSize},
            function (data) {
              deferred.resolve(data);
            },
            function (err) {
              exception.resolve('')(err,true);
              deferred.reject(err);
            });

          return deferred.promise;
        },
        createGroup:function(group){
          var deferred = $q.defer();
          resourceGroupDevices.save(group,
            function (data){
              deferred.resolve(data);
            },
            function (err){
              exception.resolve('')(err,false);
              deferred.reject(error);
            });
          return deferred.promise;
        },
        updateGroup:function(accountId,groupId,groupName){
          var deferred = $q.defer();
          resourceGroupDevices.update({'accountId':accountId,'groupId':groupId,'name':groupName},
            function(data){
              deferred.resolve(data);
            },
            function(err){
              exception.resolve('')(err,false);
              deferred.reject(err);
            }
          );
          return deferred.promise;
        },
        excludeGroup:function(accountId, groupId){
          var deferred = $q.defer();
          resourceGroupDevices.delete({'accountId':accountId, 'groupId':groupId},
          function(data){
            deferred.resolve(data);
          },
            function (err){
              exception.catcher('')(err,false);
              deferred.reject(err);
            }
          );
          return deferred.promise;
        }
      };
    } // End of service function
  ]
);
