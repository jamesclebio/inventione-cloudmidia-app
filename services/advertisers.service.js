angular.module('cloudmidiaApp').factory('AdvertisersService',
  ['$resource',
    '$q',
    'AppSettings',
    '$window',
    '$http',
  function ($resource, $q, AppSettings, $window, $http,exception) {

    var resourceContents = $resource(
      AppSettings.urlApi + 'accounts/:accountId/advertisers/:advertiserId',
      {accountId:'@accountId', 'advertiserId':'@advertiserId'},
      {update:{method:'PUT'}}
    );

    var resourceSpecificContents = $resource(
      AppSettings.urlApi + 'accounts/:accountId/alladvertisers',
      {accountId:'@accountId'},
      {update:{method:'PUT'}}
    );

    return {
      createAdvertiser: function(advertiser){
        var deferred = $q.defer();

        resourceContents.save(advertiser,
          function (data){
            deferred.resolve(data);
          },function(err){
            deferred.reject(err);
          });
        return deferred.promise;
      },
      getAdvertiser: function(accountId,advertiserId){
        var deferred = $q.defer();
        resourceContents.get({'accountId':accountId,'advertiserId':advertiserId},
        function(data){
          deferred.resolve(data);
        },
        function(err){
          deferred.reject(err);
        });
        return deferred.promise;
      },
      getAdvertisers: function(accountId,searchedName,page,pageSize,sort){
        var deferred = $q.defer();

        resourceContents.get({'accountId':accountId, 's':searchedName, 'page':page, 'pageSize':pageSize, 'srt':sort},
          function(data){
            deferred.resolve(data);
          },
          function (err){
            deferred.reject(err);
          });
        return deferred.promise;
      },
      updateAdvertiser: function(advertiser){
        var deferred =  $q.defer();
        resourceContents.update(advertiser,
          function(data){
            deferred.resolve(data);
          },
          function(err){
            deferred.reject(err);
          });
        return deferred.promise;
      },
      excludeAdvertiser: function(accountId,id){
        var deferred = $q.defer();
        resourceContents.delete({accountId:accountId,advertiserId:id},
          function(data){
            deferred.resolve(data);
          },
          function(err){
            deferred.reject(err);
          });
        return deferred.promise;
      },
      getAllAdvertisers: function(accountId){
        var deferred = $q.defer();
        resourceSpecificContents.query({accountId:accountId},
          function(data){
            deferred.resolve(data);
          },
          function(err){
            deferred.reject(err);
          });
        return deferred.promise;
      }
    };
  }]);
