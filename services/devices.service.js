angular.module('cloudmidiaApp')
  .factory('DevicesService', ['$resource',
    '$q',
    'AppSettings',
    '$window',
    '$http',
  function ($resource, $q, AppSettings, $window, $http) {

    var resourceDevices = $resource(AppSettings.urlApi + 'accounts/:accountId/devices/:deviceId',
      {accountId: '@accountId', deviceId: '@deviceId'}, {
        update: {method: 'PUT'}
      });

    var resourceDevicesWithGroups = $resource(AppSettings.urlApi +
      'accounts/:accountId/devices/devicesWithGroup', {accountId: '@accountId'}, {});

    function openSaveAsDialog(filename, content, mediaType) {
      var blob = new Blob([content], {type: mediaType});
      saveAs(blob, filename);
    }

    return {
    getAddress: function (device) {
      var geocode = new google.maps.Geocoder();
      var deferred = $q.defer();
      var latlng = {lat: parseFloat(device.deviceLatitude), lng: parseFloat(device.deviceLongitude)}
      geocode.geocode({'location': latlng}, function (results, status) {
        if (status === google.maps.GeocoderStatus.OK) {
          deferred.resolve(results[0]);
        }
      });
      return deferred.promise;
    },
      getDevices:function (accountId, searchedName, page, pageSize, ord) {
      var deferred = $q.defer();

      resourceDevices.get({'accountId': accountId, 's': searchedName, 'page': page, 'pageSize': pageSize,'srt':ord},
        function (data) {
          deferred.resolve(data);
        },
        function (error) {
          deferred.reject(error);
        });

      return deferred.promise;
    },
    getDevice: function (accountId, deviceId) {
      var deferred = $q.defer();

      resourceDevices.get({accountId: accountId, deviceId: deviceId},
        function (data) {
          deferred.resolve(data);
        },
        function (error) {
          deferred.reject(error);
        });
      return deferred.promise;
    },
    //Return groups of devices
    getDevicesGroups: function (accountId) {
      var deferred = $q.defer();

      resourceDevicesWithGroups.query({accountId: accountId},
        function (data) {
          deferred.resolve(data);
        },
        function (error) {
          deferred.reject(error);
        });
      return deferred.promise;
    },
    addDevice: function (accountId, deviceId, deviceName, groups) {
      var deferred = $q.defer();

      resourceDevices.save({accountId: accountId, deviceId: deviceId, name: deviceName, groups: groups},
        function (data) {
          deferred.resolve(data);
        },
        function (error) {
          deferred.reject(error);
        });
      return deferred.promise;
    },
    updateDevice: function (accountId, device) {
      var deferred = $q.defer();

      resourceDevices.update({accountId: accountId, deviceId: device.id, name: device.name,
        locals: device.locals,
        numOfTvs:device.numOfTvs},

        function (data) {
          deferred.resolve(data);
        },
        function (error) {
          deferred.reject(error);
        });
      return deferred.promise;
    },
    excludeDevice: function (accountId, id) {
      var deferred = $q.defer();

      resourceDevices.delete({accountId: accountId, deviceId: id},
        function (data) {
          deferred.resolve(data);
        },
        function (error) {
          deferred.reject(error);
        });
      return deferred.promise;
    },
    downloadFile: function (accountId, deviceId) {
      var downloadURL = AppSettings.urlApi + 'accounts/{accountId}/devices/{deviceId}/scheduleDownload'
          .replace('{accountId}', accountId)
          .replace('{deviceId}', deviceId);

      $http(
        {
          url: downloadURL,
          method: 'POST',
          responseType: 'arraybuffer',
          cache: false,
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          }
        })
        .success(function (data, status, headers) {
          var header = headers();
          openSaveAsDialog('playlist.zip', data, header['content-type']);
        });
    }
  };
}]);
