/**
 * Created by Jadson on 02/10/2015.
 */
'use strict';

angular.module('cloudmidiaApp')
  .factory('MessagesInterceptor', ['$q', '$location', '$timeout',
    function ($q, $location, $timeout) {

      var notificationOption = {
        style: 'flip',
        message: '',
        position: 'top-left',
        timeout: 5000,
        type: 'success'
      };

      var service ={
        error: error,
        success: success,
        warning:warning,
        info:info
      };

      function showMessage(option, element) {
        $(element || 'body').pgNotification(option).show();
      }

      function error(message, elementId){
        notificationOption.type = 'error';
        notificationOption.message = message;
        showMessage(notificationOption, elementId);
      };

      function success(message, elementId){
        notificationOption.type = 'success';
        notificationOption.message = message;
        showMessage(notificationOption, elementId);
      }

      function warning(message,elementId){
        notificationOption.type = 'warning';
        notificationOption.message = message;
        showMessage(notificationOption, elementId);
      }

      function info(message,elementId){
        notificationOption.type = 'info';
        notificationOption.message = message;
        showMessage(notificationOption, elementId);
      }

    return service;
}]);
