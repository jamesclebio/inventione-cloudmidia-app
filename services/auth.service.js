'use strict';
angular.module('cloudmidiaApp').factory('authService',
  ['$rootScope', '$http', '$q', 'localStorageService', 'AppSettings',
    function ($rootScope, $http, $q, localStorageService, AppSettings) {

    var authServiceFactory = {};

      var _authentication = function () {
        var auth = {
          'isAuth': false,
          'userName': ''
        };

        var authorizationData = localStorageService.get('authorizationData');
        if (authorizationData !== null) {
          auth.isAuth = true;
          auth.userName = authorizationData.userName;
        }

        return auth;
    };

    var _externalAuthData = {
      provider: '',
      userName: '',
      externalAccessToken: ''
    };

    var _saveRegistration = function (registration) {

      _logOut();

      return $http.post(AppSettings.urlApi + 'api/account/register', registration).then(function (response) {
        return response;
      });

    };

    var _token = function (data) {
      var deferred = $q.defer();

      $http.post(
        AppSettings.urlApi + 'token',
        $.param(data),
        {headers: {'Content-Type': 'application/x-www-form-urlencoded'}})
        .success(function (response) {

          localStorageService.set('authorizationData', {
            token: response.access_token,
            userName: data.userName,
            refreshToken: response.refresh_token,
            accessToken: response.access_token
          });


          _authentication.isAuth = true;
          _authentication.userName = data.userName;

          $rootScope.$broadcast('$$userAuthenticated');

          deferred.resolve(response);

        }).error(function (err, status) {
          _logOut();
          deferred.reject(err);
        });

      return deferred.promise;
    };

    var _getUserPermissions = function (enterpriseId){
      var deferred = $q.defer();
       $http.get(
         AppSettings.urlApi + 'profile?pid='+enterpriseId
       ).success(function(response){
           localStorageService.set('permissions',{
             data:response.permissions
           });
           deferred.resolve(response);
         }).error(function(err){
           deferred.reject(err);
         });
      return deferred.promise;
    }

    var _login = function (loginData) {

      var data = {
        grant_type: 'password',
        userName: loginData.userName,
        password: loginData.password,
        client_id: 'cloudmidiaApp-local'
      };

      return _token(data);

    };

    var _logOut = function () {

      localStorageService.remove('authorizationData');
      localStorageService.remove('allEnterpriser');
      localStorageService.remove('currentEnterpriser');
      localStorageService.clearAll();
      _authentication.isAuth = false;
      _authentication.userName = '';
    };

    var _fillAuthData = function () {

      var authData = localStorageService.get('authorizationData');
      if (authData) {
        _authentication.isAuth = true;
        _authentication.userName = authData.userName;
      }

    };

    var _refreshToken = function () {
      var deferred = $q.defer();
      var authData = localStorageService.get('authorizationData');

      if (authData && authData.refreshToken) {

        var data = 'grant_type=refresh_token&refresh_token=' + authData.refreshToken + '&client_id=' + ngAuthSettings.clientId;

        localStorageService.remove('authorizationData');

        $http.post(
          AppSettings.urlApi + 'token',
          data,
          {headers: {'Content-Type': 'application/x-www-form-urlencoded'}})
          .success(function (response) {

            localStorageService.set('authorizationData', {
              token: response.access_token,
              userName: response.userName,
              refreshToken: response.refresh_token
            });

            deferred.resolve();

          }).error(function (err, status) {
            _logOut();
            deferred.reject(err);
          });
      } else {
        deferred.resolve();
      }

      return deferred.promise;
    };

    var _obtainAccessToken = function (externalData) {

      var deferred = $q.defer();

      $http.get(AppSettings.urlApi + 'api/account/ObtainLocalAccessToken', {
        params: {
          provider: externalData.provider,
          externalAccessToken: externalData.externalAccessToken
        }
      }).success(function (response) {

        localStorageService.set('authorizationData', {
          token: response.access_token,
          userName: response.userName,
          provider: response.provider
        });

        _authentication.isAuth = true;
        _authentication.userName = response.userName;

        $rootScope.$broadcast('$$userAuthenticated');
        deferred.resolve(response);

      }).error(function (err, status) {
        _logOut();
        deferred.reject(err);
      });

      return deferred.promise;

    };

    var _getExternalUserInfo = function (registerExternalData) {
      var deferred = $q.defer();

      $http.post(AppSettings.urlApi + 'api/account/externalUserInfo', registerExternalData)
        .success(function (response) {
          deferred.resolve(response);
        })
        .error(function (error) {
          deferred.reject(error);
        });

      return deferred.promise;
    };

    var _registerExternal = function (registerExternalData) {

      var deferred = $q.defer();

      $http.post(AppSettings.urlApi + 'api/account/registerOrUpdateExternalLogin', registerExternalData).success(function (response) {

        localStorageService.set('authorizationData', {
          token: response.access_token,
          userName: response.userName,
          refreshToken: response.refresh_token
        });

        _authentication.isAuth = true;
        _authentication.userName = response.userName;

        $rootScope.$broadcast('$$userAuthenticated');

        deferred.resolve(response);

      }).error(function (err, status) {
        _logOut();
        deferred.reject(err);
      });

      return deferred.promise;

    };

    var _retrieveProfileInfo = function () {
      var authData = localStorageService.get('authorizationData');
      var deferred = $q.defer();

      if (authData) {
        $http.get(AppSettings.urlApi + '/account/userinfo')
          .success(function (response) {
            localStorageService.set('profileInfo',response);
            deferred.resolve(response);
          })
          .error(function (err, status) {
            deferred.reject(err);
          });
      }
      return deferred.promise;
    };

    var _retrieveProfileInfoById = function (accountId) {
      var authData = localStorageService.get('authorizationData');
      var deferred = $q.defer();

      if (authData) {
        $http.get(AppSettings.urlApi + '/account/'+accountId+'/userinfo')
          .success(function (response) {
            localStorageService.set('profileInfo',response);
            deferred.resolve(response);
          })
          .error(function (err, status) {
            deferred.reject(err);
          });
      }
      return deferred.promise;
    };

    var _getSavedProfileInfo = function(){
      return localStorageService.get('profileInfo');
    };



    var _validateEmail = function (data) {
      var deferred = $q.defer();

      if (data) {
        $http.post(AppSettings.urlApi + 'api/account/confirm', data)
          .success(function (response) {
            deferred.resolve(response);
          })
          .error(function (err, status) {
            deferred.reject(err);
          });
      }

      return deferred.promise;
    };

    authServiceFactory.saveRegistration = _saveRegistration;
    authServiceFactory.login = _login;
    authServiceFactory.logOut = _logOut;
    authServiceFactory.fillAuthData = _fillAuthData;
    authServiceFactory.authentication = _authentication;
    authServiceFactory.profileInfo = _retrieveProfileInfo;
    authServiceFactory.refreshToken = _refreshToken;
    authServiceFactory.profileInfoById = _retrieveProfileInfoById;
    authServiceFactory.obtainAccessToken = _obtainAccessToken;
    authServiceFactory.externalAuthData = _externalAuthData;
    authServiceFactory.getExternalUserInfo = _getExternalUserInfo;
    authServiceFactory.registerExternal = _registerExternal;
    authServiceFactory.validateEmail = _validateEmail;

    authServiceFactory.getSavedProfileInfo = _getSavedProfileInfo;
    authServiceFactory.getUserPermissions = _getUserPermissions;
    return authServiceFactory;
  }]);
