angular.module('cloudmidiaApp').factory('LocalsService',
  ['$resource',
    '$q',
    'AppSettings',
    'Exception',
  function ($resource, $q, AppSettings,Exception) {

    var resourceLocals = $resource(AppSettings.urlApi + 'accounts/:accountId/locals/:localId',
      {accountId: '@accountId', localId: '@localId'}, {
        update: {method: 'PUT'}
      });

    var resourceAllLocals = $resource(AppSettings.urlApi + 'accounts/:accountId/alllocals',
      {accountId: '@accountId'}, {
        update: {method: 'PUT'}
      });

    var resourceLocalsDevice = $resource(AppSettings.urlApi + 'accounts/:accountId/alllocaldevice',
      {accountId: '@accountId'}, {
        update: {method: 'PUT'}
      });

    var resourceTypeOfBusiness = $resource(AppSettings.urlApi + 'accounts/:accountId/typesOfBusiness',
      {accountId: '@accountId'}, {
        update: {method: 'PUT'}
      });

    var resourceValidLocals = $resource(AppSettings.urlApi + 'accounts/:accountId/validlocals',
      {accountId: '@accountId'},{});

    var resourceLocalsStatusUpdate = $resource(AppSettings.urlApi + 'accounts/:accountId/locals/:localId/updatestatus',
      {accountId: '@accountId', localId:'@localId'},{
        update:{method: 'PUT'}
      });

    var resourceConfig = $resource(AppSettings.urlApi +'Account/config-values');

    var geocode = new google.maps.Geocoder();

    function openSaveAsDialog(filename, content, mediaType) {
      var blob = new Blob([content], {type: mediaType});
      saveAs(blob, filename);
    }

    return {
      getAddress: function (local) {
        var deferred = $q.defer();
        var latlng = {lat: parseFloat(local.latitude), lng: parseFloat(local.longitude)}
        geocode.geocode({'location': latlng},
          function (results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
              deferred.resolve(results[1]);
            }
          },
          function(err){
        });
        return deferred.promise;
      },
      // Used to get locals with empty slots
      getValidLocals:function(accountId,searchedName, page, pageSize, privateLocals){
        var deferred = $q.defer();
        resourceValidLocals.get({accountId:accountId,'s': searchedName, 'page': page, 'pageSize': pageSize},
          function success(data){
            deferred.resolve(data);
          },
          function error(err){
            deferred.reject(err);
          });
        return deferred.promise;
      },
      updateLocalStatus: function(accountId, localId, status){
        var deferred = $q.defer();
        resourceLocalsStatusUpdate.update({accountId:accountId,localId:localId,status:status},
          function(data){
            deferred.resolve(data);
          },
          function(error){
            deferred.reject(data);
          });
        return deferred.promise;
      },
      updateLocal:function(local){
        var deferred = $q.defer();
        resourceLocals.update(local,
          function(data){
            deferred.resolve(data);
          },
          function (err){
            deferred.reject(err);
          });
        return deferred.promise;
      },
      addLocal:function(local){
        var deferred = $q.defer();
        resourceLocals.save(local,
          function (data){
            deferred.resolve(data);
          },
          function (err){
            deferred.reject(err);
          }
        );
        return deferred.promise;
      },
      getLocalsByPage:function (accountId, searchedName, page, pageSize, sort) {
        var deferred = $q.defer();

        resourceLocals.get({'accountId': accountId,'s': searchedName, 'page': page, 'pageSize': pageSize,'srt':sort},
          function (data) {
            deferred.resolve(data);
          },
          function (err) {
            deferred.reject(err);
          });

        return deferred.promise;
      },
      getLocalsDeviceByPage:function(accountId, searchedName,page,pageSize){
        var deferred = $q.defer();

        resourceLocalsDevice.get({'accountId': accountId,'s': searchedName, 'page': page, 'pageSize': pageSize},
          function (data) {
            deferred.resolve(data);
          },
          function (err) {
            deferred.reject(err);
          });

        return deferred.promise;
      },
      getLocal:function(accountId,localId){
        var deferred = $q.defer();
        resourceLocals.get({accountId:accountId,localId:localId},
          function(data){
            deferred.resolve(data);
          },
          function(err){
            deferred.reject(err);
          });
        return deferred.promise;
      },
      getLocals:function(accountId){
        var deferred = $q.defer();
        resourceAllLocals.query({accountId:accountId},
          function(data){
            deferred.resolve(data);
          },
          function(err){
            deferred.reject(err);
          });
        return deferred.promise;
      },
      excludeLocal:function(accountId,localId){
        var deferred = $q.defer();
        resourceLocals.delete({accountId:accountId,localId:localId},
          function(data){
            deferred.resolve(data);
          },
          function(err){
            deferred.reject(err);
          });
        return deferred.promise;
      },
      getTypesOfBusiness: function(accountId){
        var deferred = $q.defer();

        resourceTypeOfBusiness.query({'accountId':accountId},
          function(data){
            deferred.resolve(data);
          },
          function(error){
            deferred.reject(error);
          }
        );
        return deferred.promise;
      },
      getConfigValues: function() {
        var deferred = $q.defer();
        resourceConfig.get({},
          function (data) {
            deferred.resolve(data);
          },
          function error(err) {
            deferred.reject(err);
          });
        return deferred.promise;
      }
    };
 }]);
