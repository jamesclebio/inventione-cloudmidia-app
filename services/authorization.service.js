/**
 * Created by Admin on 8/15/2016.
 */
(function(){
  'strict use';

  angular.module('cloudmidiaApp')
    .service('Authorization',function(){

      this.authorized = function(action,context,listPermission){
        var contextPermission = _.map(_.pick(listPermission,context),function(num,key){return num});
        var permission;
        for(var i=0; i <= contextPermission.length;i++){
            permission = _.find(contextPermission[i], function(item){
            return item === action;
          });
          if(permission){
            break;
          }
        }
        return permission !== null && permission !== undefined ? true : false;
      }
    })
}());
