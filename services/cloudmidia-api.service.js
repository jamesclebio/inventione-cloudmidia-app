/**
 * Created by Jadson on 30/09/2015.
 */
angular.module('cloudmidiaApp')
  .factory('CloudmidiaApi', ['$resource', 'AppSettings', CloudmidiaApi]);

function CloudmidiaApi($resource, AppSettings){

  var CampaignApi = $resource(AppSettings.urlApi + 'accounts/:accountId/campaigns/:campaignId',
    {accountId: '@accountId', campaignId: '@campaignId'}, {
      update: {method: 'PUT'}
    });

  var CampaignBasicApi = $resource(AppSettings.urlApi +'accounts/:accountId/campaigns/:campaignId/basic',
    {accountId:'@accountId', campaignId:'@campaignId'},{
      update:{method:'PUT'}
    });

  var CampaignLocalApi = $resource(AppSettings.urlApi +'accounts/:accountId/campaigns/:campaignId/locals',
    {accountId:'@accountId', campaignId:'@campaignId'},{
      update:{method:'PUT'}
    });

  var CampaignContentApi = $resource(AppSettings.urlApi +'accounts/:accountId/campaigns/:campaignId/content',
    {accountId:'@accountId', campaignId:'@campaignId'},{
      update:{method:'PUT'}
    });

  var UserApi = $resource(AppSettings.urlApi + 'accounts/:accountId/users/:userId/roles/:roleId',
    {accountId: '@accountId', userId:'@userId', roleId:'@roleId'},{
      update:{method:'PUT'}
    });

  var EnterpriserApi = $resource(AppSettings.urlApi + 'Account/:slug',
    {slug:'@slug'});

  var UploadApi = function(accountId){
    return $resource(AppSettings.urlApi + 'accounts/'+accountId+'/medias');
  };

  var UsersApi = $resource(AppSettings.urlApi + 'accounts/:accountId/users/:userId',
    {accountId: '@accountId',userId:'@userId'},{});

  var InvitationApi = $resource(AppSettings.urlApi + 'accounts/:accountId/invitations',
    {accountId: '@accountId'},{});

  var InvitationInformationApi = $resource(AppSettings.urlApi + 'invitations/',
    {},{});

  var SocialMediaApi = $resource(AppSettings.urlApi + 'accounts/:accountId/mediasocial',
    {accountId:'@accountId', accessToken:'@accessToken'},{});

  var RolesApi = $resource(AppSettings.urlApi + 'Account/:accountId/Roles',
    {},{});

  var RegisterApi = $resource(AppSettings.urlApi + 'Account/register/',{},{});

  var RegisterCompanyApi = $resource(AppSettings.urlApi + 'Account/:accountId/register',
    {accountId:'@accountId'},{});

  var CampaignSubscriptionApi = $resource(AppSettings.urlApi + 'accounts/:accountId/user/:userId/instagram/subscription/campaign',
    {accountId:'@accountId', userId:'@userId'},{});

  var ResetPasswordApi = $resource(AppSettings.urlApi + 'Account/reset-password/',{},{});

  var DeviceApi = $resource(AppSettings.urlApi + 'accounts/:accountId/devices/:deviceId',
    {accountId: '@accountId', deviceId: '@deviceId'}, {
      update: {method: 'PUT'}
    });

  var MediaApi = $resource(AppSettings.urlApi + 'accounts/:accountId/medias/:mediaId',
    {accountId: '@accountId', mediaId: '@mediaId'}, {
      update: {method: 'PUT'}
    });

  return {
    EnterpriserApi: EnterpriserApi,
    RegisterCompanyApi: RegisterCompanyApi,
    InvitationInformationApi :InvitationInformationApi,
    InvitationApi:InvitationApi,
    UploadApi:UploadApi,
    UsersApi:UsersApi,
    RolesApi:RolesApi,
    UserApi: UserApi,
    RegisterApi:RegisterApi,
    ResetPasswordApi: ResetPasswordApi,
    CampaignApi: CampaignApi,
    DeviceApi: DeviceApi,
    MediaApi: MediaApi,
    SocialMediaApi: SocialMediaApi,
    CampaignSubscriptionApi:CampaignSubscriptionApi,
    CampaignBasicApi: CampaignBasicApi,
    CampaignContentApi: CampaignContentApi,
    CampaignLocalApi: CampaignLocalApi
  };
}
