'use strict';
angular.module('cloudmidiaApp')
  .factory('authInterceptorService', ['$q', '$injector', '$location', 'localStorageService',
    function ($q, $injector, $location, localStorageService,$state) {

      var authInterceptorServiceFactory = {};

      var _request = function (config) {

        config.headers = config.headers || {};

        var authData = localStorageService.get('authorizationData');
        if (authData) {
          config.headers.Authorization = 'Bearer ' + authData.token;
        }

        return config;
      };

      var _responseError = function (rejection) {
        if (rejection.status === 401 && $location.path() !== '/app/login') {
          $injector.get('authService').logOut();
          var search = {'redirectTo': $location.url()};
          $location.url('/login').search(search);
        }
        return $q.reject(rejection);
      };

      authInterceptorServiceFactory.request = _request;
      authInterceptorServiceFactory.responseError = _responseError;

      return authInterceptorServiceFactory;
    }]);
