angular.module('cloudmidiaApp').factory('LocationService', ['$resource', '$q', 'AppSettings', '$window', '$http',
  function ($resource, $q, AppSettings, $window, $http) {

     return {

      getCurrentPosition: function () {
        var deferred = $q.defer();

        if (!$window.navigator.geolocation) {
          deferred.reject('Geolocation not support.');
        }

        $window.navigator.geolocation.getCurrentPosition(
          function (position) {
            deferred.resolve(position);
          },
          function (err) {
            deferred.reject(err);
          }
        );
        return deferred.promise;
      }
    };
  }]);
