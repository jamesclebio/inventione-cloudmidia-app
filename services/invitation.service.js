angular.module('cloudmidiaApp').factory('InvitationService', ['$resource', '$q', 'AppSettings', '$window', '$http',
  function ($resource, $q, AppSettings, $window, $http) {


    return {

      acceptInvitation: function (accountId, token) {
        var deferred = $q.defer();
        var url = AppSettings.urlApi + 'accounts/{accountId}/invitations/accept'
            .replace('{accountId}', accountId);
        $http.post(url, {'token': token}).then(function success(response) {
            deferred.resolve(response);
          },
          function error(response) {
            deferred.reject(response);
          });
        return deferred.promise;
      },
      cancelInvitationById: function(invitationId){
        var deferred = $q.defer();
        var url = AppSettings.urlApi + 'invitations/invitationId/cancel'.replace('invitationId',invitationId);
        $http.post(url).then(function success(response){
          deferred.resolve(response);
        },function error(error){
          deferred.reject(error);
        });
        return deferred.promise;
      },
      resendInvitationById:function(invitationId){
        var deferred = $q.defer();
        var url = AppSettings.urlApi + 'invitations/invitationId/resend'.replace('invitationId',invitationId);
        $http.post(url).then(function success(response){
          deferred.resolve(response);
        },function error(error){
          deferred.reject(error);
        });
        return deferred.promise;
      },
      acceptRegisterInvitation: function (accountId, user) {
        var deferred = $q.defer();
        var url = AppSettings.urlApi + '/accounts/accountId/invitations/accept-and-register'
            .replace('accountId', accountId);
        $http.post(url, user).then(function success(response) {
            deferred.resolve(response);
          },
          function error(response) {
            deferred.reject(response);
          });
        return deferred.promise;
      }
    };
  }]);
