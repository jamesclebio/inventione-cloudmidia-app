angular.module('cloudmidiaApp').factory('AccountService', ['$resource', '$q', 'AppSettings', '$window', '$http',
  function ($resource, $q, AppSettings, $window, $http) {

    var resourceUserAccount = $resource(AppSettings.urlApi + 'accounts/:accountId/user',
      {accountId: '@accountId'}, {
        update: {method: 'PUT'}
      });

    var resourceAccount = $resource(AppSettings.urlApi + 'Account/:accountId/account',
      {accountId: '@accountId'}, {
        update: {method: 'PUT'}
      });

    var resourceAllInformation = $resource(AppSettings.urlApi + 'accounts/:accountId/allinformation')

    var resourcePassword = $resource(AppSettings.urlApi + 'Account/ChangePassword',
      {}, {});

    var resourceEnterprise = $resource(AppSettings.urlApi + 'Account/:email/Enterprises',
      {email:'@email'},{});

    return {
      userUpdate:function(user){
        var deferred = $q.defer();

        resourceUserAccount.update({accountId:user.accountId,firstName:user.firstName,
            lastName:user.lastName,email:user.userName},
        function(data){
          deferred.resolve(data);
        },
        function (error){
          deferred.reject(error);
        });
        return deferred.promise;
      },
      accountUpdate:function(company){
        var deferred = $q.defer();
        resourceAccount.update({accountId:company.accountId,name:company.accountName,slug:company.slug, thumbUrl: company.thumbUrl},
        function(data){
          deferred.resolve(data);
        },
        function(error){
          deferred.reject(error);
        });
        return deferred.promise;
      },
      passwordUpdate:function(user){
        var deferred = $q.defer();
        resourcePassword.save(user,
        function(data){
          deferred.resolve(data);
        },
        function(error){
          deferred.reject(error);
        });
        return deferred.promise;
      },
      getEnterprises: function(email){
        var deferred = $q.defer();
        resourceEnterprise.query({email:email},
          function(data){
            deferred.resolve(data);
          },
          function(error){
            deferred.reject(error);
          });
        return deferred.promise;
      },
      getAllInformation: function(enterpriseId){
        var deferred = $q.defer();
        resourceAllInformation.get({accountId:enterpriseId},
          function(data){
            deferred.resolve(data);
          },
          function(error){
            deferred.reject(error);
          });
        return deferred.promise;
      }
    };
  }]);
