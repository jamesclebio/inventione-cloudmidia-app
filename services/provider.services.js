angular.module('cloudmidiaApp').factory('ProviderService',['$resource','$http','$q','AppSettings','$rootScope',
  '$location','localStorageService','authService','Exception','$state',
  function($resource,$http,$q,AppSettings,$rootScope,$location,
              localStorageService,authService,Exception,$state){

    var resourceProviders = $resource(AppSettings.urlApi +'Account/:email/Enterprises/:slug',
      {email:'@email',slug:'@slug'},{});

    var currentEnterpriser;

    var allEnterpriser;

    var compareEnterpriserBySlug = function(slug){
      allEnterpriser = localStorageService.get('allEnterpriser');
      if(allEnterpriser !== null){
        for(var i=0; i<allEnterpriser.length;i++){
          if(allEnterpriser[i].slug === slug){
            return true;
          }
        }
      }
      return false;
    };

    var getUserInfo = function (){
      var deferred = $q.defer();
      if (authService.authentication().isAuth) {
        authService.profileInfo().then(
          function success(data) {
             deferred.resolve(data);
          }, function error() {
          });
        return deferred.promise;
      }
    };

    var getEnterpriserBySlug = function(slug){
      allEnterpriser = localStorageService.get('allEnterpriser');
      if(allEnterpriser !== null){
        for(var i=0; i<allEnterpriser.length;i++){
          if(allEnterpriser[i].slug === slug){
            return allEnterpriser[i];
          }
        }
      }
    };

    var setEnterpriser = function(enterpriser){
      var current  = localStorageService.get('currentEnterpriser');
      if (current !== undefined){
        localStorageService.remove('currentEnterpriser');
      }
      localStorageService.set('currentEnterpriser',enterpriser);
    };

    var updateEnterprises = function(slug){
      var deferred = $q.defer();
      getUserInfo()
        .then(function(data){
          getEnterprise(slug,data)
            .then(
              function success (response){
               if(response.id !== undefined){
                  var allEnterprises = localStorageService.get('allEnterpriser');
                  localStorageService.remove('allEnterpriser');
                  //add new enterprise
                  allEnterprises.push(response);
                  localStorageService.set('allEnterpriser',oldata);
                  localStorageService.set('currentEnterpriser',response);
                  deferred.resolve();
                }
            }, function error(err){
              Exception.catcherError(err, true);
            });
        });
      return deferred.promise;
    };

    var getEnterprise = function(slug,data){
      var deferred = $q.defer();
      resourceProviders.get({email:data.userName,slug:slug},
        function(data){
          if(data.id !== undefined){
            localStorageService.set('currentEnterpriser',data);
            deferred.resolve(data);
          }else{
            deferred.reject(data);
          }
        },
        function(err){
          deferred.reject(err);
        });
      return deferred.promise;
    };

    var hasCurrentEnterpriser = function(slug){
      var current  = localStorageService.get('currentEnterpriser');
      return ( current !== null ? true : false);
    };

    var isCurrentEnterpriser = function(slug){
      var current  = localStorageService.get('currentEnterpriser');
      return (slug === current.slug);
    };

    return{
      verifyEnterpriser:function(slug){
        if(hasCurrentEnterpriser()){
          if(!isCurrentEnterpriser(slug)){
            if(compareEnterpriserBySlug(slug)){
                setEnterpriser(getEnterpriserBySlug(slug));
                $rootScope.$broadcast('changedEnterpriser');
            }else{
              //called when someone type new slug on url
              updateEnterprises(slug).then(function success(){
                setEnterpriser(getEnterpriserBySlug(slug));
                $rootScope.$broadcast('changedEnterpriser');
              },function error(err){
                Exception.catcherError(err);
              });
            }
          }else{
            //called when slug is equal to currentEnterprise.
            return '';
          }
        }else{
          //called when there isn't Enterprise into Url
          if(compareEnterpriserBySlug(slug)){
            setEnterpriser(getEnterpriserBySlug(slug));
            $rootScope.$broadcast('changedEnterpriser');
          }else{
            //called when there isn't Enterprise into URL and neither localstorage.
            var data = localStorageService.get('profileInfo');
            getEnterprise(slug,data).then(
              function success(){
              $rootScope.$broadcast('changedEnterpriser');
              setEnterpriser(getEnterpriserBySlug(slug));
            },
              function error(err){
                Exception.catcherError(err,true);
              })
          }
        }
      },
      getCurrentEnterpriser: function(){
        return localStorageService.get('currentEnterpriser');
      },
      getAllEnterprise: function(){
        return localStorageService.get('allEnterpriser');
      },
      getEnterprise:function(data,slug){
        var deferred = $q.defer();
        return getEnterprise(slug,data).then(
          function success(data){
            deferred.resolve(data);
            return deferred.promise;
          }
        );
      },
      updateCurrentEnterprise: function(data){
        var deferred = $q.defer();
        var current = this.getCurrentEnterpriser();
        if(current){
          var newData = _.find(data, function(enterprise){return enterprise.id == current.id});
          localStorageService.set('currentEnterpriser',newData);
          deferred.resolve(newData);
        }
        return deferred.promise;
      },
      saveAllEnterpriser: function(data){
        localStorageService.set('allEnterpriser',data);
      }
    };

  }]);
